-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 30, 2020 at 07:48 PM
-- Server version: 5.6.45
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jithvarc_career`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `source` varchar(255) NOT NULL,
  `source_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `user_id`, `source`, `source_id`) VALUES
(2, 59, 'facebook', '2137040789942110');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_assignment`
--

CREATE TABLE `tbl_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_assignment`
--

INSERT INTO `tbl_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', 1, 1521359762),
('admin', 11, NULL),
('user', 3, 1523382545),
('user', 7, NULL),
('user', 8, NULL),
('user', 9, NULL),
('user', 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_item`
--

CREATE TABLE `tbl_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_item`
--

INSERT INTO `tbl_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, 'Administrator of this application', NULL, NULL, 1521056030, 1521056030),
('adminArticle', 2, 'Allows admin+ roles to manage articles', NULL, NULL, 1521056029, 1521056029),
('createArticle', 2, 'Allows editor+ roles to create articles', NULL, NULL, 1521056029, 1521056029),
('deleteArticle', 2, 'Allows admin+ roles to delete articles', NULL, NULL, 1521056029, 1521056029),
('editor', 2, 'Editor of this application', NULL, NULL, 1521056029, 1521056029),
('manageUsers', 2, 'Allows admin+ roles to manage users', NULL, NULL, 1521056029, 1521056029),
('member', 2, 'Registered users, members of this site', NULL, NULL, 1521056029, 1521056029),
('premium', 2, 'Premium members. They have more permissions than normal members', NULL, NULL, 1521056029, 1521056029),
('support', 2, 'Support staff', NULL, NULL, 1521056029, 1521056029),
('theCreator', 1, 'You!', NULL, NULL, 1521056030, 1521056030),
('updateArticle', 2, 'Allows editor+ roles to update articles', NULL, NULL, 1521056029, 1521056029),
('updateOwnArticle', 2, 'Update own article', 'isAuthor', NULL, 1521056029, 1521056029),
('usePremiumContent', 2, 'Allows premium+ roles to use premium content', NULL, NULL, 1521056029, 1521056029),
('user', 1, 'User', NULL, NULL, 1521056029, 1521056029);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_item_child`
--

CREATE TABLE `tbl_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_item_child`
--

INSERT INTO `tbl_auth_item_child` (`parent`, `child`) VALUES
('theCreator', 'admin'),
('editor', 'adminArticle'),
('editor', 'createArticle'),
('admin', 'deleteArticle'),
('admin', 'editor'),
('admin', 'manageUsers'),
('support', 'member'),
('support', 'premium'),
('editor', 'support'),
('admin', 'updateArticle'),
('updateOwnArticle', 'updateArticle'),
('editor', 'updateOwnArticle'),
('premium', 'usePremiumContent');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_auth_rule`
--

CREATE TABLE `tbl_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_auth_rule`
--

INSERT INTO `tbl_auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('isAuthor', 'O:28:\"common\\rbac\\rules\\AuthorRule\":3:{s:4:\"name\";s:8:\"isAuthor\";s:9:\"createdAt\";i:1521056028;s:9:\"updatedAt\";i:1521056028;}', 1521056028, 1521056028);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_commission`
--

CREATE TABLE `tbl_commission` (
  `id` int(11) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `plan_amount` decimal(10,2) NOT NULL,
  `discount_percentage` decimal(10,2) NOT NULL,
  `commission_amount` decimal(10,2) NOT NULL,
  `commision_type` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `to_sponser_id` int(11) DEFAULT NULL,
  `from_sponser_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_commission`
--

INSERT INTO `tbl_commission` (`id`, `discount`, `plan_amount`, `discount_percentage`, `commission_amount`, `commision_type`, `plan_id`, `to_sponser_id`, `from_sponser_id`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(5, 20.00, 25000000.00, 0.00, 20000000.00, 1, 1, 29, 35, 3, '2020-03-23 18:32:11', NULL, NULL),
(6, 20.00, 50000.00, 0.00, 40000.00, 1, 1, 35, 36, 3, '2020-03-24 12:08:31', NULL, NULL),
(7, 20.00, 25000000.00, 0.00, 20000000.00, 1, 1, 36, 37, 3, '2020-03-30 09:45:29', NULL, NULL),
(8, 20.00, 25000000.00, 0.00, 20000000.00, 1, 1, 37, 38, 3, '2020-03-30 09:48:41', NULL, NULL),
(9, 20.00, 25000000.00, 0.00, 20000000.00, 1, 1, 38, 40, 3, '2020-03-30 10:09:49', NULL, NULL),
(10, 20.00, 25000000.00, 0.00, 20000000.00, 1, 1, 1, 41, 3, '2020-03-30 13:46:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country`
--

CREATE TABLE `tbl_country` (
  `c_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(50) NOT NULL,
  `tel` varchar(12) NOT NULL,
  `flag_icon` varchar(255) NOT NULL,
  `flag_path` varchar(255) NOT NULL,
  `language` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `created_ip` int(11) NOT NULL,
  `update_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_by` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_ip` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country`
--

INSERT INTO `tbl_country` (`c_id`, `name`, `code`, `tel`, `flag_icon`, `flag_path`, `language`, `created_on`, `created_by`, `created_ip`, `update_on`, `update_by`, `update_ip`, `status`, `is_delete`) VALUES
(1, 'India', 'INR', '+91', 'Jellyfish.jpg', '', 'Hindi', '2019-02-16 12:53:39', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image`
--

CREATE TABLE `tbl_image` (
  `img_id` int(11) NOT NULL,
  `image` int(255) NOT NULL,
  `ques_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_image`
--

INSERT INTO `tbl_image` (`img_id`, `image`, `ques_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 1),
(4, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_master`
--

CREATE TABLE `tbl_master` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `code` varchar(100) NOT NULL,
  `abbr` varchar(150) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_master`
--

INSERT INTO `tbl_master` (`id`, `name`, `code`, `abbr`, `parent_id`, `status`, `created_on`, `created_by`, `updated_on`, `updated_by`) VALUES
(1, 'jhyujytu', '', 'tyutyu', 2, 23, '2019-02-16 11:35:17', 0, '0000-00-00 00:00:00', 0),
(2, 'Madhya pradesh', '123', 'State', 1, 1, '2019-02-16 13:10:02', 0, '0000-00-00 00:00:00', 0),
(6, 'Bhopal', '12', 'City', 1, 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 1),
(7, 'Up', '123', 'State', 1, 1, '2019-02-16 13:34:09', 31, '0000-00-00 00:00:00', 0),
(8, 'sd', 'rr', 'Area', 4, 4, '2019-02-16 13:42:13', 31, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE `tbl_member` (
  `id` int(11) NOT NULL,
  `sponser_id` varchar(50) NOT NULL,
  `unique_id` varchar(150) DEFAULT NULL,
  `upline_id` varchar(50) NOT NULL,
  `left_right_leg` enum('left','right','no') NOT NULL,
  `applicant_name` varchar(255) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `name_type` varchar(11) DEFAULT NULL,
  `father_spouse_name` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `address` text,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pan_no` varchar(50) DEFAULT NULL,
  `aadhaar_no` varchar(50) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `IFSC_code` varchar(100) DEFAULT NULL,
  `account_holder_name` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `nomine_name` varchar(255) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `upload_photo` varchar(255) DEFAULT NULL,
  `id_proof_photo` varchar(255) DEFAULT NULL,
  `pan_card_photo` varchar(255) DEFAULT NULL,
  `passbook_photo` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `plan_amount` decimal(10,2) DEFAULT NULL,
  `leg_count` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` date DEFAULT NULL,
  `status` enum('1','0','','') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`id`, `sponser_id`, `unique_id`, `upline_id`, `left_right_leg`, `applicant_name`, `gender`, `name_type`, `father_spouse_name`, `dob`, `address`, `city`, `state`, `mobile`, `email`, `pan_no`, `aadhaar_no`, `bank_name`, `account_no`, `IFSC_code`, `account_holder_name`, `date`, `nomine_name`, `qualification`, `upload_photo`, `id_proof_photo`, `pan_card_photo`, `passbook_photo`, `password`, `plan_id`, `plan_amount`, `leg_count`, `created_by`, `created_on`, `updated_by`, `updated_on`, `status`) VALUES
(1, '1', 'DAS22', '1', 'no', 'Admin', 'male', '1', 'test', '1994-10-16', NULL, '2', '9', '644845654545', 'admin@gmail.com', '5655', '15548588888548', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '1'),
(41, '1', 'deqpr41', 'DAS22/Admin', 'left', 'Jony', 'male', 'S/O', 'Herry', '2020-03-30', 'test', '6', '2', '8878845488', 'jony@gmail.com', '122448884848', '02154888878', 'test', '215648484848', '6656456456456', 'jony', '2020-03-30', 'test', 'BCA', NULL, NULL, NULL, NULL, '123456', 1, 25000000.00, 0, 3, '2020-03-30', NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_image`
--

CREATE TABLE `tbl_member_image` (
  `id` int(11) NOT NULL,
  `upload_photo` varchar(255) DEFAULT NULL,
  `id_proof_photo` varchar(255) DEFAULT NULL,
  `pan_card_photo` varchar(255) DEFAULT NULL,
  `passbook_photo` varchar(255) DEFAULT NULL,
  `member_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_member_image`
--

INSERT INTO `tbl_member_image` (`id`, `upload_photo`, `id_proof_photo`, `pan_card_photo`, `passbook_photo`, `member_id`) VALUES
(1, 'ee', 'rewee', 'ERE', 'EERWE1', 1),
(2, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Large.jpg.jpg', 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', 'AlbumArtSmall.jpg.jpg', 'AlbumArtSmall.jpg.jpg', 7),
(3, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Large.jpg.jpg', 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', 'AlbumArtSmall.jpg.jpg', 'Folder.jpg.jpg', 8),
(4, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Large.jpg.jpg', 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', 'AlbumArtSmall.jpg.jpg', 'Folder.jpg.jpg', 9),
(5, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 10),
(6, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 11),
(7, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 12),
(8, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 13),
(9, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 14),
(10, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Large.jpg.jpg', NULL, NULL, NULL, 15),
(11, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 16),
(12, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 17),
(13, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 18),
(14, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 19),
(15, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 20),
(16, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 21),
(17, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 22),
(18, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 23),
(19, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 24),
(20, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 25),
(21, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 26),
(22, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 27),
(23, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Small.jpg.jpg', NULL, NULL, NULL, 28),
(24, 'AlbumArt_{5FA05D35-A682-4AF6-96F7-0773E42D4D16}_Large.jpg.jpg', NULL, NULL, NULL, 29),
(25, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 30),
(26, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 31),
(27, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 32),
(28, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 33),
(29, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 34),
(30, 'AlbumArtSmall.jpg.jpg', NULL, NULL, NULL, 35),
(31, 'kindpng_2524695.png', NULL, NULL, NULL, 36),
(32, 'kindpng_2524695.png', NULL, NULL, NULL, 37),
(33, 'kindpng_2524695.png', NULL, NULL, NULL, 38),
(35, 'kindpng_2524695.png', NULL, NULL, NULL, 40),
(36, 'kindpng_2524695.png', NULL, NULL, NULL, 41);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link_type` varchar(255) NOT NULL,
  `link_value` varchar(255) NOT NULL,
  `position_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` smallint(2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migration`
--

CREATE TABLE `tbl_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_migration`
--

INSERT INTO `tbl_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1521056015),
('m141022_115823_create_user_table', 1521056017),
('m141022_115912_create_rbac_tables', 1521056021),
('m141022_115922_create_session_table', 1521056022),
('m150104_153617_create_article_table', 1521056022);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_openings`
--

CREATE TABLE `tbl_openings` (
  `id` int(11) NOT NULL,
  `job_code` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `designation` int(11) NOT NULL,
  `min_experience` int(11) NOT NULL,
  `max_experience` int(11) NOT NULL,
  `min_salary` decimal(10,2) NOT NULL,
  `max_salary` decimal(10,2) NOT NULL,
  `salary_frequence` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `walkin` tinyint(1) NOT NULL,
  `walkin_from` date DEFAULT NULL,
  `walkin_to` date DEFAULT NULL,
  `openings` int(11) NOT NULL,
  `opening_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_ip` varchar(20) NOT NULL,
  `updated_on` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_ip` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_openings`
--

INSERT INTO `tbl_openings` (`id`, `job_code`, `title`, `description`, `designation`, `min_experience`, `max_experience`, `min_salary`, `max_salary`, `salary_frequence`, `location`, `walkin`, `walkin_from`, `walkin_to`, `openings`, `opening_date`, `created_by`, `created_on`, `created_ip`, `updated_on`, `updated_by`, `updated_ip`) VALUES
(1, 'dfgg', 'grreret', '<p>uytyutyu</p>', 5656, 6, 45, 1000.00, 10000.00, 14424, 665, 127, '0000-00-00', '0000-00-00', 11456, '0000-00-00', 31, '2019-02-16 12:00:54', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_page`
--

CREATE TABLE `tbl_page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `sub_name` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_desc` text NOT NULL,
  `og_type` varchar(255) NOT NULL,
  `og_description` text NOT NULL,
  `og_url` varchar(255) NOT NULL,
  `og_site` varchar(255) NOT NULL,
  `og_image` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` smallint(2) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_page`
--

INSERT INTO `tbl_page` (`id`, `name`, `sub_id`, `sub_name`, `template_id`, `content`, `meta_keywords`, `meta_desc`, `og_type`, `og_description`, `og_url`, `og_site`, `og_image`, `image`, `status`, `slug`, `created_by`, `created_on`) VALUES
(3, 'Service', 0, 0, 6, '<p>hello world</p>', 'hhg', 'ghg', 'ghgh', 'dfg', 'ghgh', 'ggh', '/uploads/page/service-og-1550211518.jpg', '/uploads/page/service-1550211518.jpg', 1, 'service', 31, '2019-02-15 06:18:38'),
(4, 'About', 0, 0, 6, '<section style=\"float: left; position: relative; width: 1263.2px; color: rgb(51, 51, 51); font-family: lato; letter-spacing: 0.3px;\"><div class=\"block whitish high-opacity\" style=\"float: left; padding: 100px 0px; position: relative; width: 1263.2px;\"><div class=\"container\" style=\"padding: 0px; width: 1170px;\"><div class=\"row\"><div class=\"col-md-12\" style=\"float: left; width: 1200px;\"><div class=\"why-chooseus style2 gray-bg\"><div class=\"row\"><div class=\"col-xl-6 col-lg-6 col-md-12 col-sm-12\" style=\"float: left; width: 600px;\"><div class=\"row\"><div class=\"col-sm-6\" style=\"float: left; width: 300px;\"><div class=\"chooseus-opt\" style=\"float: left; margin-bottom: 0px; padding: 60px 30px 40px; position: relative; text-align: center; width: 270px; background: none 0px 0px repeat scroll rgb(250, 250, 250); border-radius: 3px;\"><i style=\"border-radius: 50%; background: none 0px 0px repeat scroll transparent; color: rgb(192, 31, 31); display: inline-block; font-size: 36px; height: initial; line-height: initial; width: initial;\"><img src=\"file:///D:/project_report/jithvar/jithvar/images/resource/why-choos-icon1.png\" alt=\"\" style=\"border: 0px; margin-top: -15px;\"></i><span style=\"float: left; font-family: arimo; font-size: 12px; margin-top: 20px; width: 210px; color: rgb(192, 31, 31);\"><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span></span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(69, 69, 69); margin: 8px 0px 15px; font-size: 14px; float: left; width: 210px; text-transform: uppercase;\">WE DELIVER TOP RANKING ON WEB MARKETING.</h2></div></div><div class=\"col-sm-6\" style=\"float: left; width: 300px;\"><div class=\"chooseus-opt\" style=\"float: left; margin-bottom: 0px; padding: 60px 30px 40px; position: relative; text-align: center; width: 270px; background: none 0px 0px repeat scroll rgb(250, 250, 250); border-radius: 3px;\"><i style=\"border-radius: 50%; background: none 0px 0px repeat scroll transparent; color: rgb(192, 31, 31); display: inline-block; font-size: 36px; height: initial; line-height: initial; width: initial;\"><img src=\"file:///D:/project_report/jithvar/jithvar/images/resource/why-choos-icon2.png\" alt=\"\" style=\"border: 0px; margin-top: -15px;\"></i><span style=\"float: left; font-family: arimo; font-size: 12px; margin-top: 20px; width: 210px; color: rgb(192, 31, 31);\"><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span></span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(69, 69, 69); margin: 8px 0px 15px; font-size: 14px; float: left; width: 210px; text-transform: uppercase;\">PROPER AND HIGH CUSTOMER ENGAGEMENT.</h2></div></div></div><div class=\"row\"><div class=\"col-sm-6\" style=\"float: left; width: 300px;\"><div class=\"chooseus-opt\" style=\"float: left; margin-bottom: 0px; padding: 60px 30px 40px; position: relative; text-align: center; width: 270px; background: none 0px 0px repeat scroll rgb(250, 250, 250); border-radius: 3px;\"><i style=\"border-radius: 50%; background: none 0px 0px repeat scroll transparent; color: rgb(192, 31, 31); display: inline-block; font-size: 36px; height: initial; line-height: initial; width: initial;\"><img src=\"file:///D:/project_report/jithvar/jithvar/images/resource/why-choos-icon2.png\" alt=\"\" style=\"border: 0px; margin-top: -15px;\"></i><span style=\"float: left; font-family: arimo; font-size: 12px; margin-top: 20px; width: 210px; color: rgb(192, 31, 31);\"><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span></span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(69, 69, 69); margin: 8px 0px 15px; font-size: 14px; float: left; width: 210px; text-transform: uppercase;\">PROPER AND HIGH CUSTOMER ENGAGEMENT.</h2></div></div><div class=\"col-sm-6\" style=\"float: left; width: 300px;\"><div class=\"chooseus-opt\" style=\"float: left; margin-bottom: 0px; padding: 60px 30px 40px; position: relative; text-align: center; width: 270px; background: none 0px 0px repeat scroll rgb(250, 250, 250); border-radius: 3px;\"><i style=\"border-radius: 50%; background: none 0px 0px repeat scroll transparent; color: rgb(192, 31, 31); display: inline-block; font-size: 36px; height: initial; line-height: initial; width: initial;\"><img src=\"file:///D:/project_report/jithvar/jithvar/images/resource/why-choos-icon3.png\" alt=\"\" style=\"border: 0px; margin-top: -15px;\"></i><span style=\"float: left; font-family: arimo; font-size: 12px; margin-top: 20px; width: 210px; color: rgb(192, 31, 31);\"><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span><span class=\"fa fa-star\"></span></span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(69, 69, 69); margin: 8px 0px 15px; font-size: 14px; float: left; width: 210px; text-transform: uppercase;\">NOT EXPENSIVE JUST IN YOUR BUDGET.</h2></div></div></div></div><div class=\"col-xl-6 col-lg-6 col-md-12 col-sm-12\" style=\"float: left; width: 600px;\"><div class=\"whoweare\" style=\"float: left; margin-right: -40px; padding-left: 40px; width: calc(100% + 40px);\"><div class=\"title1 style1 leftalign\" style=\"float: left; width: 570px;\"><span style=\"float: left; font-family: montserrat; width: 570px; color: rgb(192, 31, 31); margin-top: 10px;\">Best Product For Your Bussiness</span><h2 style=\"font-family: montserrat; font-weight: 800; line-height: 40px; color: rgb(64, 64, 64); margin: 15px 0px 0px; font-size: 28px; float: left; text-transform: uppercase; width: 570px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/about.html#\" title=\"\" style=\"background: rgb(226, 99, 99); color: rgb(255, 255, 255); transition: all 0.4s linear 0s; border-radius: 5px; float: left; font-size: 13px; line-height: initial; margin-right: 20px; padding: 10px 25px;\">ABOUT US</a>WHO WE ARE?</h2><p style=\"margin: 23px 0px 0px; color: rgb(85, 85, 85); font-size: 15px; letter-spacing: 0.3px; line-height: 28px; float: left; max-width: 100%;\">Jithvar is an IT Start up having its head office in Lucknow. We are specialized in Website Development, ERP and CRM Development, Android Application Development and Search Engine Optimization. Our primary objective is to provide high quality service to our valuable clients.</p></div><div class=\"fun-facts style2\" style=\"float: left; width: 570px; margin-top: 40px;\"><ul class=\"funfacts-list\" style=\"margin-bottom: 0px; background: none 0px 0px repeat scroll rgba(0, 0, 0, 0); display: inline-block; list-style: none outside none; padding: 0px; width: 570px; text-align: center;\"><li style=\"color: rgb(102, 102, 102); display: inline-block; margin: 0px -2px; width: 190px; padding: 0px; position: relative; text-align: left;\"><div class=\"funfact\" style=\"float: left; width: 190px;\"><h4 style=\"font-family: montserrat; font-weight: 700; line-height: 1.1; color: rgb(45, 45, 45); margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-size: 48px; float: left; width: 190px;\">10<small style=\"font-size: 24px; font-weight: 700; line-height: 1;\">+</small></h4><span style=\"color: rgb(192, 31, 31); float: left; font-weight: 700; font-family: &quot;open sans&quot;; font-size: 11px; text-transform: uppercase; width: 190px;\">10YEARS OF EXPERIENCE</span></div></li>&nbsp;<li style=\"color: rgb(102, 102, 102); display: inline-block; margin: 0px -2px; width: 190px; padding: 0px; position: relative; text-align: left;\"><div class=\"funfact\" style=\"float: left; width: 190px;\"><h4 style=\"font-family: montserrat; font-weight: 700; line-height: 1.1; color: rgb(45, 45, 45); margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-size: 48px; float: left; width: 190px;\">85<small style=\"font-size: 24px; font-weight: 700; line-height: 1;\">K</small></h4><span style=\"color: rgb(192, 31, 31); float: left; font-weight: 700; font-family: &quot;open sans&quot;; font-size: 11px; text-transform: uppercase; width: 190px;\">HAPPY CUSTOMERS</span></div></li>&nbsp;<li style=\"color: rgb(102, 102, 102); display: inline-block; margin: 0px -2px; width: 190px; padding: 0px; position: relative; text-align: left;\"><div class=\"funfact\" style=\"float: left; width: 190px;\"><h4 style=\"font-family: montserrat; font-weight: 700; line-height: 1.1; color: rgb(45, 45, 45); margin-right: 0px; margin-bottom: 6px; margin-left: 0px; font-size: 48px; float: left; width: 190px;\">100<small style=\"font-size: 24px; font-weight: 700; line-height: 1;\">%</small></h4><span style=\"color: rgb(192, 31, 31); float: left; font-weight: 700; font-family: &quot;open sans&quot;; font-size: 11px; text-transform: uppercase; width: 190px;\">SATISFACTION</span></div></li></ul></div></div></div></div></div></div></div></div></div></section><section style=\"float: left; position: relative; width: 1263.2px; color: rgb(51, 51, 51); font-family: lato; letter-spacing: 0.3px;\"><div class=\"block\" style=\"float: left; padding: 100px 0px; position: relative; width: 1263.2px;\"><div class=\"container\" style=\"padding: 0px; width: 1170px;\"><div class=\"row\"><div class=\"col-md-12\" style=\"float: left; width: 1200px;\"><div class=\"title3 centeralign\" style=\"text-align: center; float: left; margin-bottom: 55px; width: 1170px;\"><span style=\"font-family: montserrat; font-size: 15px; letter-spacing: 0.4px; color: rgb(192, 31, 31);\">Digital Specialist Business</span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 800; line-height: 1.1; color: rgb(68, 68, 68); margin: 6px 0px 0px; font-size: 36px; text-transform: uppercase;\">PROVIDING SERVICES SINCE<span style=\"display: block; font-family: montserrat; font-size: 18px; margin-top: 9px;\"><span style=\"color: rgb(192, 31, 31);\">16 YEARS</span>&nbsp;WITH TRUST</span></h2></div><div class=\"our-services\"><div class=\"services style3\" style=\"float: left; width: 1170px;\"><div class=\"row\"><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 1px solid rgb(230, 230, 230);\"><div class=\"service-box2\" style=\"border-bottom: 1px solid rgb(230, 230, 230); float: left; padding: 20px 40px 45px; position: relative; width: 369px;\"><span class=\"flaticon-business-1\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">t.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 289px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 289px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">TAXES PLANNING</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 309px; color: rgb(192, 31, 31);\"></span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 309px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\"></a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 309px;\"></p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 1px solid rgb(230, 230, 230);\"><div class=\"service-box2\" style=\"border-bottom: 1px solid rgb(230, 230, 230); float: left; padding: 20px 40px 45px; position: relative; width: 369px;\"><span class=\"flaticon-business\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">i.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 289px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 289px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">INVESTMENT MANAGEMENT</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 309px; color: rgb(192, 31, 31);\"></span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 309px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\"></a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 309px;\"></p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 0px none;\"><div class=\"service-box2\" style=\"border-bottom: 1px solid rgb(230, 230, 230); float: left; padding: 20px 40px 45px; position: relative; width: 370px;\"><span class=\"flaticon-graphic-1\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">f.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 290px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 290px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">FINANCIAL PLANNING</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 310px; color: rgb(192, 31, 31);\"></span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 310px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\"></a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 310px;\"></p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 1px solid rgb(230, 230, 230);\"><div class=\"service-box2\" style=\"border-bottom: 0px none; float: left; padding: 20px 40px 45px; position: relative; width: 369px;\"><span class=\"flaticon-graphic\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">b.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 289px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 289px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">BUSINESS LOAN</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 309px; color: rgb(192, 31, 31);\"></span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 309px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\"></a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 309px;\"></p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 1px solid rgb(230, 230, 230);\"><div class=\"service-box2\" style=\"border-bottom: 0px none; float: left; padding: 20px 40px 45px; position: relative; width: 369px;\"><span class=\"flaticon-contacts\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">i.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 289px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 289px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">INSURANCE CONSULTING</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 309px; color: rgb(192, 31, 31);\"></span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 309px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\"></a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 309px;\"></p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 400px; border-right: 0px none;\"><div class=\"service-box2\" style=\"border-bottom: 0px none; float: left; padding: 20px 40px 45px; position: relative; width: 370px;\"><span class=\"flaticon-people-1\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 100px; -webkit-font-smoothing: antialiased; transition: all 0.4s linear 0s; border-radius: 50%; background: url(&quot;../images/serviceicon-bg.jpg&quot;) center center / cover no-repeat scroll rgba(0, 0, 0, 0); color: rgb(62, 62, 62); float: left; font-size: 35px; height: 100px; margin-left: 20px; position: relative; text-align: center; width: 100px; z-index: 1; font-family: flaticon !important;\"></span><span style=\"font-weight: 700; transition: all 0.4s linear 0s; color: rgb(235, 235, 235); font-family: poppins; font-size: 50px; position: absolute; right: 55px;\">r.</span><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 290px; padding-left: 20px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 290px; padding-left: 20px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">RETIREMENT PLANNING</a></h2><div class=\"service-front\" style=\"transition: all 0.4s linear 0s; background: none 0px 0px repeat scroll rgba(255, 255, 255, 0.9); bottom: 0px; left: 0px; opacity: 0; padding: 0px 30px; position: absolute; right: 0px; text-align: center; visibility: hidden; z-index: 1;\"><span style=\"float: left; font-family: arimo; font-size: 13.16px; margin-top: 25px; width: 310px; color: rgb(192, 31, 31);\">Grow Your Business</span><h2 style=\"font-family: montserrat; line-height: 1.1; color: rgb(64, 64, 64); margin: 7px 0px 0px; font-size: 14.17px; float: left; text-transform: uppercase; width: 310px;\"><a href=\"file:///D:/project_report/jithvar/jithvar/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">RETIREMENT PLANNING</a></h2><p style=\"margin: 15px 0px 0px; color: rgb(102, 102, 102); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; float: left; width: 310px;\">Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an known printer took a gallery of type and scrm bled it to make specimen.</p></div></div></div></div></div></div></div></div></div></div></section>', 'meta keywords', 'sdf', 'ghgh', 'dssdff', 'http://localhost/jithvar/site/about', 'http://localhost/jithvar/site/about', '/uploads/page/about-og-1550216049.jpg', '/uploads/page/about-1550216049.jpg', 1, 'about', 31, '2019-02-15 07:34:09'),
(5, 'Website Development', 0, 3, 6, '<div class=\"title1 style2 centeralign\" style=\"float: left; width: 1170px; text-align: center; margin-bottom: 60px; color: rgb(51, 51, 51); font-family: lato; letter-spacing: 0.3px;\"><span style=\"float: left; font-family: montserrat; width: 1170px; color: rgb(192, 31, 31);\">Results Driven, Digital Specialist Business</span><h2 style=\"font-family: montserrat; font-weight: 800; line-height: 1.1; color: rgb(64, 64, 64); margin: 8px 0px 25px; font-size: 26px; float: left; text-transform: uppercase; width: 1170px;\">WE MAKE EFFECTIVE&nbsp;<span style=\"color: rgb(192, 31, 31);\">FINANCIAL</span>&nbsp;PLANS</h2><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(85, 85, 85); font-size: 15px; letter-spacing: 0.3px; line-height: 26px; float: none; display: inline-block; max-width: 500px; padding-left: 170px; position: relative; text-align: left;\"><span class=\"flaticon-clock-1\" style=\"speak: none; font-variant-numeric: normal; font-variant-east-asian: normal; line-height: 1; -webkit-font-smoothing: antialiased; color: rgb(83, 83, 83); font-size: 60px; left: 0px; padding-right: 40px; position: absolute; font-family: flaticon !important;\"></span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id lectus quis dui euismod consectet ur. Pellentesque ac ante sollicitudin.</p></div><div class=\"services style2 style3\" style=\"float: left; width: 1170px; padding: 0px 105px; color: rgb(51, 51, 51); font-family: lato; letter-spacing: 0.3px;\"><div class=\"remove-ext\" style=\"float: left; width: 960px; margin-bottom: -50px;\"><div class=\"row\"><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 330px;\"><div class=\"service\" style=\"float: left; padding: 0px; text-align: center; width: 300px; margin-bottom: 50px;\"><div class=\"service-thumb\" style=\"float: left; overflow: hidden; position: relative; width: 300px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service1.jpg\" alt=\"\" style=\"border: 0px; display: block; width: 300px;\"></div><div class=\"service-info\" style=\"float: left; position: relative; width: 300px;\"><i style=\"display: inline-block; font-size: 52px; height: 98px; color: rgb(192, 31, 31); border-radius: 50%; background-image: none; background-position: 0px 0px; background-size: initial; background-repeat: repeat; background-attachment: scroll; background-origin: initial; background-clip: initial; margin-top: -46.5px; padding-top: 6px; position: relative; width: 98px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service-icon1.png\" alt=\"\" style=\"border: 0px; display: inline-block; border-radius: 50%; width: auto;\"></i><span style=\"float: left; font-family: montserrat; font-size: 12px; width: 300px; color: rgb(192, 31, 31); letter-spacing: 0.3px; margin-bottom: 0px; margin-top: 10px;\">Unbelievable Options</span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(64, 64, 64); margin: 10px 0px 0px; font-size: 15px; float: left; width: 300px;\"><a href=\"http://localhost/all_projects/jithvar/frontend/web/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">Financial Planning</a></h2><p style=\"margin: 15px 0px 0px; color: rgb(85, 85, 85); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; display: inline-block; max-width: 80%; width: 240px;\">Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 330px;\"><div class=\"service\" style=\"float: left; padding: 0px; text-align: center; width: 300px; margin-bottom: 50px;\"><div class=\"service-thumb\" style=\"float: left; overflow: hidden; position: relative; width: 300px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service2.jpg\" alt=\"\" style=\"border: 0px; display: block; width: 300px;\"></div><div class=\"service-info\" style=\"float: left; position: relative; width: 300px;\"><i style=\"display: inline-block; font-size: 52px; height: 98px; color: rgb(192, 31, 31); border-radius: 50%; background-image: none; background-position: 0px 0px; background-size: initial; background-repeat: repeat; background-attachment: scroll; background-origin: initial; background-clip: initial; margin-top: -46.5px; padding-top: 6px; position: relative; width: 98px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service-icon2.png\" alt=\"\" style=\"border: 0px; display: inline-block; border-radius: 50%; width: auto;\"></i><span style=\"float: left; font-family: montserrat; font-size: 12px; width: 300px; color: rgb(192, 31, 31); letter-spacing: 0.3px; margin-bottom: 0px; margin-top: 10px;\">Unbelievable Options</span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(64, 64, 64); margin: 10px 0px 0px; font-size: 15px; float: left; width: 300px;\"><a href=\"http://localhost/all_projects/jithvar/frontend/web/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">Estate Planning</a></h2><p style=\"margin: 15px 0px 0px; color: rgb(85, 85, 85); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; display: inline-block; max-width: 80%; width: 240px;\">Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div></div></div><div class=\"col-lg-4 col-md-6 col-sm-12\" style=\"float: left; width: 330px;\"><div class=\"service\" style=\"float: left; padding: 0px; text-align: center; width: 300px; margin-bottom: 50px;\"><div class=\"service-thumb\" style=\"float: left; overflow: hidden; position: relative; width: 300px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service3.jpg\" alt=\"\" style=\"border: 0px; display: block; width: 300px;\"></div><div class=\"service-info\" style=\"float: left; position: relative; width: 300px;\"><i style=\"display: inline-block; font-size: 52px; height: 98px; color: rgb(192, 31, 31); border-radius: 50%; background-image: none; background-position: 0px 0px; background-size: initial; background-repeat: repeat; background-attachment: scroll; background-origin: initial; background-clip: initial; margin-top: -46.5px; padding-top: 6px; position: relative; width: 98px;\"><img src=\"http://localhost/all_projects/jithvar/frontend/web/images/resource/service-icon3.png\" alt=\"\" style=\"border: 0px; display: inline-block; border-radius: 50%; width: auto;\"></i><span style=\"float: left; font-family: montserrat; font-size: 12px; width: 300px; color: rgb(192, 31, 31); letter-spacing: 0.3px; margin-bottom: 0px; margin-top: 10px;\">Unbelievable Options</span><h2 style=\"font-family: &quot;open sans&quot;; font-weight: 700; line-height: 1.1; color: rgb(64, 64, 64); margin: 10px 0px 0px; font-size: 15px; float: left; width: 300px;\"><a href=\"http://localhost/all_projects/jithvar/frontend/web/service-detail.html\" title=\"\" style=\"color: inherit; transition: all 0.4s linear 0s;\">Fiduciary Services</a></h2><p style=\"margin: 15px 0px 0px; color: rgb(85, 85, 85); font-size: 15px; letter-spacing: 0.3px; line-height: 24px; display: inline-block; max-width: 80%; width: 240px;\">Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p></div></div></div></div></div></div>', 'Website Development', 'Website Development', 'Website Development', 'Website Development', 'site/page/service/1', 'http://localhost/jithvar/site/page/service/1', '/uploads/page/website-development-og-1550224382.jpg', '/uploads/page/website-development-1550224382.jpg', 1, 'website-development', 31, '2019-02-15 09:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission`
--

CREATE TABLE `tbl_permission` (
  `id` int(11) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `controller_name` varchar(50) NOT NULL,
  `action_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission`
--

INSERT INTO `tbl_permission` (`id`, `group_id`, `name`, `controller_name`, `action_name`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1, 1, 'List', 'insurance-type', 'index', 3, '2018-10-30 22:32:06', 1, '2018-10-31 12:50:28'),
(2, 1, 'Create', 'insurance-type', 'create', 3, '2018-10-30 22:32:52', 1, '2018-10-31 12:50:38'),
(3, 1, 'View', 'insurance-type', 'view', 3, '2018-10-30 22:33:03', 1, '2018-10-31 12:49:22'),
(4, 1, 'Update', 'insurance-type', 'update', 3, '2018-10-30 22:33:14', 1, '2018-10-31 12:50:14'),
(5, 2, 'Create', 'insurance-plan', 'create', 1, '2018-10-31 07:21:08', NULL, NULL),
(6, 2, 'Update', 'insurance-plan', 'update', 1, '2018-10-31 07:21:18', NULL, NULL),
(7, 2, 'List', 'insurance-plan', 'index', 1, '2018-10-31 07:21:31', NULL, NULL),
(8, 2, 'View', 'insurance-plan', 'view', 1, '2018-10-31 07:21:39', NULL, NULL),
(9, 7, 'Pending Customer', 'customer', 'index', 3, '2018-10-31 21:18:39', NULL, NULL),
(10, 7, 'View', 'customer', 'view', 3, '2018-10-31 21:18:47', NULL, NULL),
(11, 7, 'update', 'customer', 'update', 3, '2018-10-31 21:19:00', NULL, NULL),
(12, 9, 'List', 'order', 'index', 3, '2018-10-31 21:19:31', NULL, NULL),
(13, 9, 'Create', 'order', 'create', 3, '2018-10-31 21:19:40', NULL, NULL),
(14, 9, 'Update', 'order', 'update', 3, '2018-10-31 21:19:56', NULL, NULL),
(15, 9, 'View', 'order', 'view', 3, '2018-10-31 21:20:22', NULL, NULL),
(16, 10, 'Shipped', 'tracking', 'index', 3, '2018-11-01 01:31:36', NULL, NULL),
(17, 10, 'Delivered', 'tracking', 'delivered', 3, '2018-11-01 01:31:57', NULL, NULL),
(18, 10, 'Cancelled', 'tracking', 'cancelled', 3, '2018-11-01 01:32:10', NULL, NULL),
(19, 11, 'Ready', 'claim', 'ready', 3, '2018-11-01 01:33:48', NULL, NULL),
(20, 11, 'Submitted', 'claim', 'submitted', 3, '2018-11-01 01:34:06', NULL, NULL),
(21, 11, 'Denied', 'claim', 'denied', 3, '2018-11-01 01:34:19', NULL, NULL),
(22, 11, 'Partial', 'claim', 'partial', 3, '2018-11-01 01:34:27', NULL, NULL),
(23, 12, 'List', 'short-url', 'index', 3, '2018-11-01 01:34:57', NULL, NULL),
(24, 5, 'List', 'product', 'index', 3, '2018-11-01 01:53:20', NULL, NULL),
(25, 3, 'List', 'billing-code', 'index', 3, '2018-11-01 01:55:25', NULL, NULL),
(26, 4, 'List', 'category', 'index', 3, '2018-11-01 01:55:36', NULL, NULL),
(27, 3, 'Create', 'billing-code', 'create', 3, '2018-11-01 01:57:20', NULL, NULL),
(28, 3, 'View', 'billing-code', 'view', 3, '2018-11-01 01:57:29', NULL, NULL),
(29, 3, 'Update', 'billing-code', 'update', 3, '2018-11-01 01:57:38', NULL, NULL),
(30, 4, 'Create', 'category', 'create', 3, '2018-11-01 01:57:52', NULL, NULL),
(31, 4, 'View', 'category', 'view', 3, '2018-11-01 01:58:01', NULL, NULL),
(32, 4, 'Update', 'category', 'update', 3, '2018-11-01 01:58:09', NULL, NULL),
(33, 6, 'List', 'physician', 'index', 3, '2018-11-01 04:53:56', NULL, NULL),
(34, 7, 'Create', 'customer', 'create', 3, '2018-11-01 09:59:06', NULL, NULL),
(35, 10, 'Cancel', 'order', 'cancel', 3, '2018-11-01 10:18:52', NULL, NULL),
(36, 10, 'Deliver', 'order', 'deliver', 3, '2018-11-01 10:19:09', NULL, NULL),
(37, 13, 'Create', 'permission-group', 'create', 3, '2018-11-29 19:22:38', NULL, NULL),
(38, 13, 'Update', 'permission-group', 'update', 3, '2018-11-29 19:23:16', NULL, NULL),
(39, 13, 'List', 'permission-group', 'index', 3, '2018-11-29 19:23:34', NULL, NULL),
(40, 14, 'List', 'permission', 'index', 3, '2018-11-29 19:24:09', NULL, NULL),
(41, 14, 'Create', 'permission', 'create', 3, '2018-11-29 19:24:18', NULL, NULL),
(42, 14, 'Update', 'permission', 'update', 3, '2018-11-29 19:24:26', NULL, NULL),
(43, 15, 'Manage', 'role-permission', 'create', 3, '2018-11-29 19:25:15', NULL, NULL),
(44, 13, 'View', 'permission-group', 'view', 3, '2018-11-29 19:33:53', NULL, NULL),
(45, 16, 'Create', 'user', 'create', 3, '2018-11-29 19:34:31', NULL, NULL),
(46, 16, 'Update', 'user', 'update', 3, '2018-11-29 19:34:39', NULL, NULL),
(47, 16, 'List', 'user', 'index', 3, '2018-11-29 19:34:50', NULL, NULL),
(48, 16, 'View', 'user', 'view', 3, '2018-11-29 19:34:58', NULL, NULL),
(49, 17, 'Create', 'roles', 'create', 3, '2018-11-29 19:35:57', 3, '2018-11-29 19:37:15'),
(50, 17, 'Update', 'roles', 'update', 3, '2018-11-29 19:36:05', 3, '2018-11-29 19:37:06'),
(51, 17, 'List', 'roles', 'index', 3, '2018-11-29 19:36:12', 3, '2018-11-29 19:37:09'),
(52, 17, 'View', 'roles', 'view', 3, '2018-11-29 19:36:19', 3, '2018-11-29 19:37:12'),
(53, 6, 'Create', 'physician', 'create', 3, '2018-11-29 19:51:07', NULL, NULL),
(54, 6, 'Update', 'physician', 'update', 3, '2018-11-29 19:51:20', NULL, NULL),
(55, 6, 'View', 'physician', 'view', 3, '2018-11-29 19:51:28', NULL, NULL),
(56, 5, 'Create', 'product', 'create', 3, '2018-11-29 19:52:32', NULL, NULL),
(57, 5, 'Update', 'product', 'update', 3, '2018-11-29 19:52:40', NULL, NULL),
(58, 5, 'View', 'product', 'view', 3, '2018-11-29 19:52:49', NULL, NULL),
(59, 12, 'Create', 'short-url', 'create', 3, '2018-11-29 19:55:28', NULL, NULL),
(60, 12, 'Import', 'short-url', 'import', 3, '2018-11-29 19:55:58', NULL, NULL),
(61, 9, 'Pending', 'order', 'pending', 3, '2018-11-30 17:37:58', NULL, NULL),
(62, 9, 'Shipped', 'order', 'shipped-orders', 3, '2018-11-30 17:38:20', NULL, NULL),
(63, 9, 'Completed', 'order', 'completed', 3, '2018-11-30 17:38:30', NULL, NULL),
(64, 11, 'Cleared', 'claim', 'cleared', 3, '2018-11-30 18:50:24', NULL, NULL),
(65, 12, 'View', 'short-url', 'view', 3, '2018-11-30 19:05:11', NULL, NULL),
(66, 12, 'Detail', 'short-url', 'detail', 3, '2018-11-30 19:07:58', NULL, NULL),
(67, 11, 'Detail', 'claim', 'detail', 3, '2018-11-30 19:09:27', NULL, NULL),
(68, 16, 'Profile', 'user', 'profile', 3, '2018-12-15 05:59:13', NULL, NULL),
(69, 11, 'Issue Claim', 'claim', 'issue', 3, '2019-01-22 15:04:32', NULL, NULL),
(70, 18, 'List', 'remit', 'index', 3, '2019-01-22 16:12:29', NULL, NULL),
(71, 18, 'Create', 'remit', 'create', 3, '2019-01-22 16:13:05', NULL, NULL),
(72, 18, 'Update', 'remit', 'update', 3, '2019-01-22 17:27:14', NULL, NULL),
(73, 11, 'Write Off', 'claim', 'write-off', 3, '2019-01-25 12:45:04', NULL, NULL),
(74, 11, 'Short Pay', 'claim', 'short-pay', 3, '2019-02-03 20:02:03', 3, '2019-02-06 22:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_group`
--

CREATE TABLE `tbl_permission_group` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permission_group`
--

INSERT INTO `tbl_permission_group` (`id`, `name`, `status`) VALUES
(1, 'Insurance Type', 1),
(2, 'Insurance Plan', 1),
(3, 'Billing Code', 1),
(4, 'Category', 1),
(5, 'Product', 1),
(6, 'Physician', 1),
(7, 'Customer', 1),
(8, 'Pending Customer', 1),
(9, 'Orders', 1),
(10, 'Tracking', 1),
(11, 'Claim', 1),
(12, 'Short URL', 1),
(13, 'Permission Group', 1),
(14, 'Permission', 1),
(15, 'Role Permission', 1),
(16, 'User', 1),
(17, 'User Role', 1),
(18, 'Remit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_plan`
--

CREATE TABLE `tbl_plan` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `discount_type` decimal(10,2) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_plan`
--

INSERT INTO `tbl_plan` (`id`, `title`, `description`, `discount_type`, `created_on`, `created_by`, `updated_by`, `updated_on`) VALUES
(1, 'Flat', 'Flat', 20.00, '2020-03-20 18:30:00', 1, NULL, NULL),
(2, 'Commericial', 'Commericial', 10.00, '2020-03-20 18:30:00', 1, NULL, NULL),
(3, 'Plot', 'plot', 5.00, '2020-03-20 18:30:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_question`
--

CREATE TABLE `tbl_question` (
  `q_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `opt_a` varchar(255) NOT NULL,
  `opt_b` varchar(255) NOT NULL,
  `opt_c` varchar(255) NOT NULL,
  `opt_d` varchar(255) NOT NULL,
  `opt_e` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `created_id` varchar(100) DEFAULT NULL,
  `update_on` timestamp NULL DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `update_ip` varchar(100) DEFAULT NULL,
  `view` int(255) DEFAULT NULL,
  `user_like` int(11) DEFAULT NULL,
  `slug` text,
  `is_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_question`
--

INSERT INTO `tbl_question` (`q_id`, `question`, `opt_a`, `opt_b`, `opt_c`, `opt_d`, `opt_e`, `answer`, `created_by`, `created_on`, `created_id`, `update_on`, `update_by`, `update_ip`, `view`, `user_like`, `slug`, `is_active`) VALUES
(1, 'Which of the following is not an event in ancient Indian history in BC era? [A] Foundation of the Indo-Greek empire [B] Beginning of Vikram samvat Er', 'Foundation of the Indo-Greek empire', 'Beginning of Vikram samvat Era', 'Fourth Buddhist Council', 'Hathigumpha inscription by Kharvela', '', 'D', 31, '2019-02-27 08:03:26', NULL, NULL, NULL, NULL, 110, NULL, 'which-of-the-following-is-not-an-event-in-ancient-indian-history-in-bc-era-a-foundation-of-the-indo-greek-empire-b-beginning-of-vikram-samvat-er', 1),
(5, 'Grand Central Terminal, Park Avenue, New York is the world\'s.?', 'largest railway station', 'highest railway station', 'longest railway station', 'None of the above', '', 'A', 42, '2019-03-06 05:43:52', NULL, NULL, NULL, NULL, 11, NULL, 'grand-central-terminal-park-avenue-new-york-is-the-world-s', 1),
(6, 'Entomology is the science that studies\r\n\r\n', 'Behavior of human beings', 'Insects', 'The origin and history of technical and scientific terms', 'The formation of rocks', '', 'B', 52, '2019-03-08 07:37:29', NULL, NULL, NULL, NULL, 1, NULL, 'entomology-is-the-science-that-studies-', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`id`, `name`, `status`) VALUES
(1, 'Administrator', 1),
(2, 'Customer Service', 1),
(3, 'Customer Service Manger', 1),
(4, 'Accounting Manager', 1),
(5, 'Accounting', 1),
(6, 'Tracking', 1),
(7, 'Tracking Manager', 1),
(8, 'Claim Specialist', 1),
(9, 'Claim Specialist Manger', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role_permission`
--

CREATE TABLE `tbl_role_permission` (
  `id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role_permission`
--

INSERT INTO `tbl_role_permission` (`id`, `permission_id`, `role_id`, `status`, `created_by`, `created_on`, `updated_by`, `updated_on`) VALUES
(1723, 33, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1724, 53, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1725, 54, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1726, 55, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1727, 9, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1728, 10, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1729, 11, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1730, 34, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1731, 12, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1732, 13, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1733, 14, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1734, 15, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1735, 61, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1736, 62, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1737, 63, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1738, 16, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1739, 17, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1740, 18, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1741, 35, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1742, 36, 3, 1, 1, '2018-12-03 21:55:52', NULL, NULL),
(1794, 33, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1795, 55, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1796, 9, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1797, 10, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1798, 11, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1799, 34, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1800, 12, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1801, 13, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1802, 15, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1803, 61, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1804, 62, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1805, 63, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1806, 16, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1807, 17, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1808, 18, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1809, 35, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1810, 36, 7, 1, 1, '2018-12-03 21:56:46', NULL, NULL),
(1828, 33, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1829, 53, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1830, 54, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1831, 55, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1832, 9, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1833, 10, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1834, 11, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1835, 34, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1836, 12, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1837, 13, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1838, 14, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1839, 15, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1840, 61, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1841, 62, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1842, 63, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1843, 16, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1844, 17, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1845, 18, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1846, 35, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1847, 36, 2, 1, 1, '2018-12-07 15:22:00', NULL, NULL),
(1866, 33, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1867, 53, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1868, 54, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1869, 55, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1870, 9, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1871, 10, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1872, 11, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1873, 34, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1874, 12, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1875, 13, 9, 1, 1, '2018-12-10 15:00:04', NULL, NULL),
(1876, 14, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1877, 15, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1878, 16, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1879, 17, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1880, 18, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1881, 35, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1882, 36, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1883, 19, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1884, 20, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1885, 21, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1886, 22, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1887, 64, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1888, 67, 9, 1, 1, '2018-12-10 15:00:05', NULL, NULL),
(1889, 33, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1890, 55, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1891, 9, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1892, 10, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1893, 11, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1894, 34, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1895, 12, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1896, 13, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1897, 14, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1898, 15, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1899, 61, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1900, 62, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1901, 63, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1902, 16, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1903, 17, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1904, 18, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1905, 35, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1906, 36, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1907, 19, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1908, 20, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1909, 21, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1910, 22, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1911, 67, 5, 1, 1, '2018-12-10 15:00:55', NULL, NULL),
(1980, 10, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(1981, 16, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(1982, 17, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(1983, 18, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(1984, 35, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(1985, 36, 6, 1, 1, '2018-12-17 16:08:30', NULL, NULL),
(2197, 33, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2198, 53, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2199, 54, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2200, 55, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2201, 9, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2202, 10, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2203, 11, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2204, 34, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2205, 12, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2206, 13, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2207, 14, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2208, 15, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2209, 61, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2210, 62, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2211, 63, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2212, 16, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2213, 17, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2214, 18, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2215, 35, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2216, 36, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2217, 19, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2218, 20, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2219, 21, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2220, 22, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2221, 64, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2222, 67, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2223, 70, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2224, 71, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2225, 72, 4, 1, 3, '2019-01-22 17:35:56', NULL, NULL),
(2299, 33, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2300, 55, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2301, 9, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2302, 10, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2303, 12, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2304, 13, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2305, 14, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2306, 15, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2307, 61, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2308, 62, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2309, 63, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2310, 16, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2311, 17, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2312, 18, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2313, 35, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2314, 36, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2315, 19, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2316, 20, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2317, 67, 8, 1, 1, '2019-01-28 21:31:00', NULL, NULL),
(2318, 1, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2319, 2, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2320, 3, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2321, 4, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2322, 5, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2323, 6, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2324, 7, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2325, 8, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2326, 25, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2327, 27, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2328, 28, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2329, 29, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2330, 26, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2331, 30, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2332, 31, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2333, 32, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2334, 24, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2335, 56, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2336, 57, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2337, 58, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2338, 33, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2339, 53, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2340, 54, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2341, 55, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2342, 9, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2343, 10, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2344, 11, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2345, 34, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2346, 12, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2347, 13, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2348, 14, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2349, 15, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2350, 61, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2351, 62, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2352, 63, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2353, 16, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2354, 17, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2355, 18, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2356, 35, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2357, 36, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2358, 19, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2359, 20, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2360, 21, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2361, 22, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2362, 64, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2363, 67, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2364, 69, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2365, 73, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2366, 74, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2367, 23, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2368, 59, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2369, 60, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2370, 65, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2371, 66, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2372, 37, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2373, 38, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2374, 39, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2375, 44, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2376, 40, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2377, 41, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2378, 42, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2379, 43, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2380, 45, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2381, 46, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2382, 47, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2383, 48, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2384, 68, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2385, 49, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2386, 50, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2387, 51, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2388, 52, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2389, 70, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2390, 71, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL),
(2391, 72, 1, 1, 3, '2019-02-03 20:08:08', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service`
--

CREATE TABLE `tbl_service` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_service`
--

INSERT INTO `tbl_service` (`id`, `name`, `description`, `image`) VALUES
(1, 'Website Development', 'Website Development', 'website-development.jpg'),
(2, 'Software Development', 'Software Development', 'website-development.jpg'),
(3, 'ERP Development', 'ERP Development', 'website-development.jpg'),
(4, 'IOS Development', 'IOS Development', 'Penguins.jpg'),
(5, 'Android Development', 'Android Development', 'Hydrangeas.jpg'),
(6, 'Brand Promotion', 'Brand Promotion', 'Jellyfish.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `expire` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`id`, `expire`, `data`) VALUES
('0pb1flsm5lh0n1o7nnqsj0qbl3', 1524480597, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b),
('1iki6enb39705tdgfp75ekca64', 1527163434, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b),
('1u76c4966tvj5ht8b6kse67pp4', 1528479773, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b),
('39i37ad7n0h323qevr93445e56', 1528455557, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b),
('a56rpsutrde1u565qpfiebmse4', 1524422088, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b),
('hof4g13f5o6kb21e232ch41b37', 1527163339, 0x5f5f666c6173687c613a303a7b7d),
('ulcc762h7kdartlmptth9ckv35', 1524421878, 0x5f5f666c6173687c613a303a7b7d5f5f69647c693a313b);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey`
--

CREATE TABLE `tbl_survey` (
  `s_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `selected_ans` varchar(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(150) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `created_ip` int(11) DEFAULT NULL,
  `update_id` int(11) DEFAULT NULL,
  `update_on` timestamp NULL DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL,
  `correct_answer` varchar(150) NOT NULL,
  `user_email` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_survey`
--

INSERT INTO `tbl_survey` (`s_id`, `question_id`, `selected_ans`, `user_id`, `user_name`, `created_by`, `created_on`, `created_ip`, `update_id`, `update_on`, `update_by`, `correct_answer`, `user_email`, `status`) VALUES
(1, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(2, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(3, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(4, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(5, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(6, 5, 'A', 52, NULL, 52, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(7, 1, 'C', 58, NULL, 58, NULL, NULL, NULL, NULL, NULL, 'D', NULL, NULL),
(8, 5, 'B', 58, NULL, 58, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL),
(9, 6, 'A', 58, NULL, 58, NULL, NULL, NULL, NULL, NULL, 'B', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_template`
--

CREATE TABLE `tbl_template` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_template`
--

INSERT INTO `tbl_template` (`id`, `name`, `status`, `created_by`, `created_on`) VALUES
(6, 'Temp1', 1, 3, '2018-12-29 09:19:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `status` smallint(6) NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_activation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `first_name`, `last_name`, `employee_id`, `username`, `email`, `password_hash`, `role_id`, `status`, `auth_key`, `password_reset_token`, `account_activation_token`, `created_at`, `updated_at`, `company_id`) VALUES
(1, NULL, NULL, '', 'IMomin', 'imomin@babypavilion.com', '$2y$13$duAodbBS8hSlAoFKPhuQTeE4/s1R.7SILHPMg.lr6CTEiUIgqAK5y', 1, 10, 'bF8q-4nXvNUGo0h-EVdmAjTB2SNa1BbH', NULL, NULL, 1538494924, 1538494924, NULL),
(3, 'Sachin', 'Jaiswal', '', 'admin@admin.com', 'sachin@ptindia.org', '$2y$13$G30yoL8mjlutpSGkdLphH.juf0INmGJeiX7Qusc/JnEchrizohMcW', 1, 10, '87uifHXJN17aFDgdA0TVAIyT4WcKC_9e', NULL, NULL, 1523382544, 1544411732, 1),
(6, NULL, NULL, '', 'jaiswal', 'dasdsad@fsd.fd', '$2y$13$f9i8KlXzyOjfspHaTcyzn.T6VHhuln/5DpT2RVCqovPub4T0w6Hxa', 1, 10, 'gDNsOB2l5LO6qI-QPIa6nlqX4Lp5mfJc', NULL, NULL, 1524379719, 1524379719, NULL),
(7, 'Tabassum', 'Mithani', '92001', 'TMithani', 'tmithani@babypavilion.com', '$2y$13$NUu1G6mxvg/C76l0WBo/6OGcWPedk6nFA9wBVo3L84jhF/5ptl8ji', 4, 10, 'mf1i806EwQ9d0-i_6cIcgKen7PR2iCv3', NULL, NULL, 1538494152, 1547660955, NULL),
(8, 'Alexandra', 'Redmon', '92002', 'ARedmon', 'aredmon@babypavilion.com', '$2y$13$heJcRReRWB5hRiUSNQWE8OfcishqqZOdOqDtpixbofrkElVcrAKOa', 3, 10, 'Ql_sbn5kZqPvDgTXWFtFVJfK3QSkcC-m', NULL, NULL, 1538494186, 1544479075, NULL),
(9, 'Safiya', 'Hafizi', '92009', 'SHafizi', 'SHafizi@babypavilion.com', '$2y$13$emtkMgKm3kFCu9/sNORBruQ..kY0pR6smhVaLGtanm/pgUQEU3fHe', 9, 10, 'Qs3butfCO0jRnXzgDb1Ag_GmbuFNvzAY', NULL, NULL, 1538494689, 1544479099, NULL),
(10, 'Mohammed', 'Mithani', '92010', 'MMithani', 'mmithani@babypavilion.com', '$2y$13$GW/19XgQcd2gVYNnmJJ4YuPj6rQW8dwqNJKmubvOtANYn/a/sLuxe', 4, 10, 'fmRdbtc_JamcbgpnCDRqh78dr06OxQX0', NULL, NULL, 1538494885, 1544479122, NULL),
(12, 'Mustafa', 'Ahsan', '92016', 'MAhsan', 'MAhsan@Babypavilion.com', '$2y$13$euk3xDbLdoaLguD3YWHOaOZjjx7u.6b49eb6GnenIhAyGFDyE6bnG', 2, 10, 'Vo9h--MJcYcOj-ivIk9lIgQyYaqxjiMj', NULL, NULL, 1542658889, 1544479139, NULL),
(18, 'Soniya', 'Varughese', '92021', 'SVarughese', 'SVarughese@BabyPavilion.com', '$2y$13$pIJSz20klSMA853M2s9V..pPQqc4USPp0hJAcQVR3rGH3snWNl79i', 2, 10, 'SZ7BFTSXFFGBW_8ixPpJk3o0ffXNMr3C', NULL, NULL, 1543007334, 1544479324, NULL),
(19, 'Fatima', 'Shariff', '92012', 'fshariff', 'fshariff@babypavilion.com', '$2y$13$fw2115pboepdKwlL6BiGnu.sDGAGNoRdwuDyqd6O1PIYRQMeAsHpi', 8, 10, 'baNTjZokPluZlEWOqCoiK6AlX9BVUNid', NULL, NULL, 1543007368, 1544479186, NULL),
(20, 'Mehdiyah', 'Rajani', '92008', 'mrajani', 'mrajani@babypavilion.com', '$2y$13$9T07XmjvOGDMKP1f7iq2CuV9to61QHMBmMPHJENAm0xGYVuhvc5Fa', 2, 10, 'w77P8yxOIKR1iSftK76jjce92JvlUJNR', NULL, NULL, 1543007439, 1544479220, NULL),
(21, 'Sakina', 'Shariff', '92013', 'SShariff', 'sshariff@babypavilion.com', '$2y$13$yR0QAwdAU9hu.rz/jzFzuOP0.OtXXIsro9EGkbbpJCj5Zv0AHVlqm', 3, 10, 'wFL8oI-Svq4PwEW4cIvFsaH5gnOZ2N0v', NULL, NULL, 1543007483, 1544479252, NULL),
(22, 'Crystal', 'Arellano', '92005', 'CArellanoDeleted', 'carellano@babypavilion.com', '$2y$13$CcRlaI/MQceabDYWj0i8BO2DjNKLqMAo9XSCasYtn8qS0n3lxKHSu', 2, 1, '3PDY1CgCzzEoqlG8jStRahc9HRLTyhBg', NULL, NULL, 1543007519, 1546641660, NULL),
(23, 'Shabnam', 'Badarpura', '92020', 'SBadarpura', 'sbadarpura@babypavilion.com', '$2y$13$MXrbdhF4LhoGEkRdUrKMguH1ZfA5h/0y/41ne1GFbRx6.gkhIMVsa', 8, 10, 'YfH7G9j6Cb88IlOfYVP0duCVtzh6SDRh', NULL, NULL, 1543007557, 1544479294, NULL),
(25, 'Hector', 'Zarate', '92011', 'hzarate', 'hzarate@babypavilion.com', '$2y$13$jF.IeBR43tLQXSO9t260MuXZbd0rFroYsKNsHodFuu6m7.Ns5SF7W', 6, 1, 'iBfShQZ_caLraz09_GLFiny8n896h-il', NULL, NULL, 1545062555, 1547494426, NULL),
(26, 'Muhammad', 'Ali', '92022', 'MAli', 'therealmammad221@gmail.com', '$2y$13$lB1C8Ail7ut4xsQs74y6Te3W2/sUUJEICnZ4Jw/C7FQGcsvY6RJ7u', 6, 10, 'lrzPd8NmOxiw3Gyltsr8i7SAFjtWdfH3', NULL, NULL, 1545062711, 1545062711, NULL),
(27, 'Anis', 'Balsaniya', '', 'abalsaniya', 'balsaniyaanis4255@gmail.com', '$2y$13$hbNAWpabApgYH.RN7CDlVuclV92wXIqpkhdxpOQ2AHkZ6zeBvujMG', 2, 10, '5PD9DU-YDcVLUmsWCeEdx6_XCqY86PQZ', NULL, NULL, 1546873561, 1546873561, NULL),
(28, 'Amanda', 'Juarez', '92025', 'AJuarez', 'AJuarez@Babypavilion.com', '$2y$13$xD6XOZ4pUGH6olkVU2iDDeeoKDk8gx144hRRBIQw9CpVWyfnbwUO6', 8, 10, 'qGOjuZwbznYQiPt9p4g3JW7vN-6E09yM', NULL, NULL, 1546877606, 1546877606, NULL),
(29, 'Amina', 'Bhatti ', '92024', 'ABhatti', 'ABhatti@babypavilion.com', '$2y$13$G9f49WdUBIsBETFEPvtmB.FPZXlIA89PHMGrIEHUekVkw685NCC52', 2, 10, 'mZfS1nbzyEK3qBEA2TdqSzeuPTJi6Z4g', NULL, NULL, 1546878482, 1546878482, NULL),
(30, 'Alizay', 'Karani', '92027', 'AKarani', 'AKarani@babypavilion.com', '$2y$13$4C9zXcebVsCE0aLSGeNFaOFS32Y6DQRQ4VDQ9RJC.F0uWhFpOsTle', 2, 10, 'jj3b7DLy9adEDjNW3lCP20JaH1CQ4MdE', NULL, NULL, 1548695760, 1548711090, NULL),
(31, 'Rashmi ', 'Pathak', '92028', 'jithvar', 'admin@jithvar.com', '$2y$13$BEOwwLaUH/0n4anqRXKkLe1vw/29J70uyuQ7kTJX.SetV5k9mMiy2', 2, 10, 'xXi_HWwPwg_GDijqbmj1McsSigLGriIP', NULL, NULL, 1549007333, 1549007333, NULL),
(47, NULL, NULL, NULL, 'hello4', 'hello4@gmail.com', '$2y$13$tS7NuhdSAoUtWi3ulxNgTuTeasDlbIZN/Ld9.tNFc/U6TKUuLVE4u', NULL, 10, 'gOLv7G_97wn890zCXV3GSYEO5sSMabS7', NULL, NULL, 1551870059, 1551870059, NULL),
(48, NULL, NULL, NULL, 'test', 'test@gmail.com', '$2y$13$hYwL/IXCOXpzcE3BI4YUEOvZfpdKE6M551qlM4QEbDDuq1Q4460EK', NULL, 10, 'aa_ekkobFE8IaRNdVMvRUZXefDMEn4Aa', NULL, NULL, 1551873352, 1551873352, NULL),
(49, NULL, NULL, NULL, 'test1', 'test1@gmail.com', '$2y$13$bHYzz8yPp7rs1YHafQxlsO.0dpJzfUVe7XhT8aDps/Lf69BygpSg2', NULL, 10, 'ybzlml0dsCtZps7aLr2Kh8X1HHe71yvJ', NULL, NULL, 1551873380, 1551873380, NULL),
(52, NULL, NULL, NULL, 'hello', 'hello@gmail.com', '$2y$13$duAodbBS8hSlAoFKPhuQTeE4/s1R.7SILHPMg.lr6CTEiUIgqAK5y', NULL, 10, 'mlZ7OTTSp1sKSUw2svXKtmQY6j8TLsQ9', NULL, NULL, 1551875624, 1551875624, NULL),
(53, NULL, NULL, NULL, 'testa', 'testa@gmail.com', '$2y$13$.PY/gq0LQXGGKxFfeu4rOOz0t3mK/dmrGuk5K3vReJghweW5hGIn.', NULL, 10, 'EzIxzGuKRHx0n6YZLwvpt7lvJOJ0d2Pk', NULL, NULL, 1551875907, 1551875907, NULL),
(57, NULL, NULL, NULL, 'hello5', 'hello5@gmail.com1', '$2y$13$TK4SJMK8xPW..5XTD0dOsO3OL9BgLt.h2h78bkHfG9lsoUqtVn4YS', NULL, 10, 'ID908ZKIYn9D-EKxTnVaS-JaYO73rkci', NULL, NULL, 1551877334, 1551877334, NULL),
(59, NULL, NULL, NULL, 'ranumalviya0202@gmail.com', 'ranumalviya0202@gmail.com', '$2y$13$uCCCjICAxRp.fe52TnbW5eUkywJ0igciwbssfw2BpeiokcgOgnr1K', NULL, 10, 'CkIDBhUuYyCmVfTVMmHjIRa3tVT6yecS', NULL, NULL, 1555931147, 1555931147, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'gw6j2s7SbbhHG3V6H_O9UBq78jDEf_kb', '$2y$13$stFxOGfhf3un3cjTyk0POOOl8tXSA3j2If4IQOZ03FfhgJu9VvKTS', NULL, 'admin@gmail.com', 10, 1551170583, 1551170583);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_auth_assignment`
--
ALTER TABLE `tbl_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`);

--
-- Indexes for table `tbl_auth_item`
--
ALTER TABLE `tbl_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Indexes for table `tbl_auth_item_child`
--
ALTER TABLE `tbl_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `tbl_auth_rule`
--
ALTER TABLE `tbl_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `tbl_commission`
--
ALTER TABLE `tbl_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_country`
--
ALTER TABLE `tbl_country`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `tbl_master`
--
ALTER TABLE `tbl_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_member`
--
ALTER TABLE `tbl_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_member_image`
--
ALTER TABLE `tbl_member_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `link_type` (`link_type`),
  ADD KEY `position_id` (`position_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status` (`status`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `tbl_openings`
--
ALTER TABLE `tbl_openings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `job_code` (`job_code`),
  ADD KEY `designation` (`designation`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `template_id` (`template_id`),
  ADD KEY `status` (`status`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `parent_id` (`group_id`);

--
-- Indexes for table `tbl_permission_group`
--
ALTER TABLE `tbl_permission_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_plan`
--
ALTER TABLE `tbl_plan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_question`
--
ALTER TABLE `tbl_question`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `tbl_service`
--
ALTER TABLE `tbl_service`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey`
--
ALTER TABLE `tbl_survey`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `tbl_template`
--
ALTER TABLE `tbl_template`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `status` (`status`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_commission`
--
ALTER TABLE `tbl_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_country`
--
ALTER TABLE `tbl_country`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_master`
--
ALTER TABLE `tbl_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_member`
--
ALTER TABLE `tbl_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tbl_member_image`
--
ALTER TABLE `tbl_member_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_openings`
--
ALTER TABLE `tbl_openings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_permission`
--
ALTER TABLE `tbl_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `tbl_permission_group`
--
ALTER TABLE `tbl_permission_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_plan`
--
ALTER TABLE `tbl_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_question`
--
ALTER TABLE `tbl_question`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_role_permission`
--
ALTER TABLE `tbl_role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2392;

--
-- AUTO_INCREMENT for table `tbl_service`
--
ALTER TABLE `tbl_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_survey`
--
ALTER TABLE `tbl_survey`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_template`
--
ALTER TABLE `tbl_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
