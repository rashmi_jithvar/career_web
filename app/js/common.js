// function to bind question group bu questionnaire id
var bindItem = function(field_id, suffix, selected_val){
    $.post('../item/bind-item'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
        $(".select2-container--bootstrap").css("width", "100%");
    });
}

// function to bind question group bu questionnaire id
$(".bind-warehouse").change(function (){
    var field_id = $(this).attr('data-child');
    $.post('../warehouse/bind-warehouse.jtvr', function (data){
        populateCombo(field_id,data,"id","name");
    });
});

// function to load a ddl
var bindWarehouse = function(field_id, suffix, selected_val){
    $.post('../warehouse/bind-warehouse'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}

// function to load a ddl
var bindItemCategory = function(field_id, suffix, selected_val){
    $.post('../item-category/bind-category'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}

// function to load a ddl
var bindTaxes = function(field_id, suffix, selected_val){
    $.post('../taxes/bind-taxes'+suffix, function (data){
        populateCombo(field_id,data,"id","tax_name", selected_val);
        $(".select2-container--bootstrap").css("width", "100%");
    });
}

// function to load a ddl
var bindState = function(field_id, suffix, selected_val, country_id, url){
    $.post(url+'state/bind-state'+suffix+'?country='+country_id, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}

// function to load a ddl
var bindFinancialYear = function(field_id, suffix, selected_val){
    $.post('financial-year/bind-financial-year'+suffix, function (data){
        populateCombo(field_id,data,"id","financial_year", selected_val);
    });
}

// function to load a ddl
var bindDateFormat = function(field_id, suffix, selected_val){
    $.post('date-format/bind-date-format'+suffix, function (data){
        populateCombo(field_id,data,"value","date_format_label", selected_val);
    });
}

// function to load a ddl
var bindTimeZone = function(field_id, suffix, selected_val){
    $.post('time-zone/bind-time-zone'+suffix, function (data){
        populateCombo(field_id,data,"id","time_zone", selected_val);
    });
}

// function to load a ddl
var bindCountry = function(field_id, suffix, selected_val){
    $.post('country/bind-country'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}

// function to load a ddl
var bindIndustry = function(field_id, suffix, selected_val){
    $.post('industry/bind-industry'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}


// function to load a ddl
var bindCategory = function(field_id, suffix = '', selected_val = '', parent_id = ''){
    $.post('../category/bind-category'+suffix, function (data){
        populateCombo(field_id,data,"id","name", selected_val);
    });
}

// function to bind question group bu questionnaire id
$(".bind-category").change(function (){
    var field_id = $(this).attr('data-child');
    $.post('../category/bind-category.jtvr?parent='+this.value, function (data){
        populateCombo(field_id,data,"id","name");
    });
});



// function to bind products by category id
$(".bind-prodouct").change(function (){
    var field_id = $(this).attr('data-child');
    $.post('../product/bind-product?category='+this.value, function (data){
        populateCombo(field_id,data,"id","title");
    });
});


// function to bind question group bu questionnaire id
$(".bind-state").change(function (){
    var field_id = $(this).attr('data-child');
    $.post('state/bind-state.jtvr?country='+this.value, function (data){
        populateCombo(field_id,data,"id","name");
    });
});

// function to bind questions by questionnaire id

$(".bind-question").change(function (z){
	var field_id = $(this).attr('data-child');
	z.preventDefault();
        z.stopPropagation();
        $("#loaderr").show();
    $.post("index.php?r=questions/bind-questions.jtvr&group_id="+this.value, function (data)
    {
    	populateComboForIdKey(field_id,data,'text');
    }).always(function(){
    	$("#loaderr").hide();
    });
});

// function to bind answers based on question id

$(".bind-answers").change(function (z){
	var field_id = $(this).attr('data-child');
	z.preventDefault();
        z.stopPropagation();
        $("#loaderr").show();
    $.post("index.php?r=questions/bind-answers.jtvr&question_id="+this.value, function (data)
    {
    	populateComboForIdKey(field_id,data,"text");
    }).always(function(){
    	$("#loaderr").hide();
    });
});


var createJSONANDSORT  = function(sortable, desc){
	jsonObj = [];
    $("#sortable li").each(function() {
        var id = $(this).attr("id");
        var text = $(this).text();
        item = {}
        item ["id"] = id;
        item ["text"] = text;
        jsonObj.push(item);
    });
	jsonObj.sort(function (a, b) {
		if(desc){
			return b.text.localeCompare( a.text);
		}else{
			return a.text.localeCompare( b.text);
		}
		
	});
	populateDragable(jsonObj, 'id', 'text');
}


$(".industry_status").click(function (){
     //alert ('hvb');
    var field_id = $(this).attr('data-child');
    $.post("index.php?r=services/bind-services.jtvr&industry="+this.value, function (data)
    {
        populateComboForIdKey(field_id,data,'text');
    });
});

$(".get-service").click(function (){ 
    var field_id = $(this).attr('data-child');
    var industry = $('#answerservicesmappingsearch-industry_status').val();
    var service_name = $('#answerservicesmappingsearch-servicelike').val(); 
    $.post("index.php?r=services/bind-services.jtvr&industry="+industry+"&service_name="+service_name, function (data)
    {
        populateComboForIdKey(field_id,data,'text');
    });
});

var populateComboForIdKey = function(field_id,data, value_name){
	populateCombo(field_id,data, "id", value_name);
}


var populateCombo = function(field_id,data, key_name, value_name, selected_val){
	var field = $("#"+field_id);
    	$(field).find("option").remove();
        $(field).append("<option value=''>Please Select</option>");
        data.forEach(function(obj) {
            if(obj[key_name] == selected_val)
            {
                var selected = 'selected'
            }
            else
            {
                var selected = '';
            }

        	$(field).append("<option "+selected+" value="+ obj[key_name] +">"+ obj[value_name] +"</option>");
        	// sortableUL.append("<li class=\'ui-state-default\' id=\'"+obj.id+"\'><span class=\'ui-icon ui-icon-arrowthick-2-n-s\'></span>"+obj.text+"</li>");
        });
}

$(".drag-questions").change(function (z){
		z.preventDefault();
        z.stopPropagation();
        $("#loaderr").show();
    $.post("index.php?r=questions/bind-questions.jtvr&group_id="+this.value, function (data)
    {
    }).done(function(data){
    	populateDragable(data, 'id', 'text');
    })
    .fail(function(){
    	$("#sortable").find("li").remove();
    }).always(function(){
    	$("#loaderr").hide();
    });
});


$(".drag-answers").change(function (z){
		z.preventDefault();
        z.stopPropagation();
        $("#loaderr").show();
    $.post("index.php?r=questions/bind-answers.jtvr&question_id="+this.value, function (data)
    {
    }).done(function(data){
    	populateDragable(data, 'id', 'text');
    })
    .fail(function(){
    	$("#sortable").find("li").remove();
    }).always(function(){
    	$("#loaderr").hide();
    });
});


var dragMenus = function (parent){
    $.post("index.php?r=menu-list/menu-json.jtvr&parent="+parent, function (data)
    {
    }).done(function(data){
    	populateDragable(data, 'MenuId', 'MenuName');
    })
    .fail(function(){
    	$("#sortable").find("li").remove();
    }).always(function(){
    });
}


var populateDragable = function(data, field_id, value_name){
	var sortableUL = $("#sortable");
    sortableUL.find("li").remove();
    $( "#sortable" ).sortable({
		stop: function( event, ui ) {
	        setDisplayOrder();
		}
	});
	$( "#sortable" ).disableSelection();
    
    data.forEach(function(obj) {
        sortableUL.append("<li class=\'ui-state-default\' id=\'"+obj[field_id]+"\'><span class=\'ui-icon ui-icon-arrowthick-2-n-s\'></span>"+obj[value_name]+"</li>");
    });
}
var desc = false;
$(".dictionary-short").on('click', function(){
	sortUnorderedList('sortable', desc);
	setDisplayOrder();
	if(desc){
		desc = false;
	}else{
		desc = true;
	}
});

var setDisplayOrder = function(){
	var orderlist ="";
	$("#sortable").find("li").each(function(){
	            orderlist +=$(this).attr("id")+":"; 
    });       
	$("#questions-question_order").val(orderlist);
}

var sortUnorderedList = function(ul, sortDescending) {        
	createJSONANDSORT(ul,sortDescending);
	setDisplayOrder();
}



// code to clone staging table
$('form')
    .on('click', '.addButton', function() {
    var incIndex = parseInt($(this).closest('table').attr('data-value'));
    incIndex++;
    $srNo = 1;
    
    var $temp = $(this).closest('tr');
        $clonedele = $temp.clone();
        $c    = $clonedele.insertAfter($temp);
        var count = -1;
        
            $clonedele.find('div.help-block').html('');


            $clonedele.find('select, input').each(function()
            {
                var $id = $(this).attr('id');
                var $valid_attr = $(this).attr('data-validate');
                var $rep_id = $id.replace((parseInt(incIndex)-1), parseInt(incIndex));
                $(this).removeAttr('id').attr('id', $rep_id);

                var $name = $(this).attr('name');

                var $rep_name = $name.replace('['+(parseInt(incIndex)-1)+']', '['+parseInt(incIndex)+']');
                $(this).removeAttr('name').attr('name', $rep_name);
                $(this).val('');

                $pDiv = $(this).closest('div').removeAttr('class').addClass("form-group "+"field-"+$rep_id+" required");

                // code for validation starts
                $(this).closest('form').yiiActiveForm("add",{
                    "id": $rep_id,
                    "name": $rep_name,
                    "container": '.field-'+$rep_id,
                    "input": '#'+$rep_id,
                    "validateOnChange": true,
                    "validate": function(attribute, value, messages, deferred, $form) {
                        yii.validation.required(value, messages, {
                            "message": "This cannot be blank."
                        });

                        if($valid_attr == 'number')
                        {
                            yii.validation.number(value, messages, {
                                "pattern": /^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/,
                                "min" : 1,
                                "tooSmall":"Quantity can't be '0'.",
                                "message": "This must be a number.",
                                "skipOnEmpty": 1
                            });
                        }

                        if($valid_attr == 'email')
                        {
                            yii.validation.email(value, messages, {
                                "pattern":/^[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/,"fullPattern":/^[^@]*<[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?>$/,
                                "allowName":false,
                                "message": "This must be an email.",
                                "skipOnEmpty": 1
                            });
                        }

                        if($valid_attr == 'string')
                        {
                            yii.validation.string(value, messages, {
                                "message": "This must be a string.",
                                "max": 255,
                                "tooLong": "This should contain at most 255 characters.",
                                "skipOnEmpty": 1
                            });
                        }
                    }
                });
                // code for validation ends
            });
            


            // code to add select 2 on drop down list starts
            $clonedele.find('select.select2').each(function(){
                $select_id = $(this).attr('id');
                if($('#'+$select_id).select2())
                {
                    $('#'+$select_id).select2('destroy');
                    $(this).closest('div').find('.select2-container--bootstrap').remove();
                    $('#'+$select_id).select2({theme: "bootstrap"});
                    $(".select2-container--bootstrap").css("width", "100%");
                }
            });
            // code to add select 2 on drop down list starts


                // code to delete row starts
                $(".fa-times").on("click",function(){
                    var formId   = $(this).closest('form').attr('id');
                    var tableId  = $(this).closest('table').attr('id');
                    var prntTbl  = $(this).closest('table');
                    if($(this).closest('tbody').find("tr").length  > 1){
                        $(this).closest('tr').find('select, input').each(function(){
                            $('#'+$(this).closest('form').attr('id')).yiiActiveForm('remove', $(this).attr('id'));
                        });
                        if($(this).closest("td").find('.addButton').length > 0 ){
                            $(this).closest('tr').prev('tr').find(".fa-times").before($(this).closest("td").find('.addButton'));
                        }

                        var amount = parseFloat($(this).closest('tr').find(':input[placeholder="Sub Total"]').val());
                        var ttlAmount = 0;
                        if(amount && !isNaN(amount))
                        {
                            var ttlField = $(this).closest('form').find(':input[placeholder="Total Amount"]');
                            ttlAmount = parseFloat(ttlField.val());
                            ttlField.val(ttlAmount - amount);

                            // ttlAmount = parseFloat($(this).closest('form').find(':input[placeholder="Total Amount"]').val());
                            // $('#purchase-total_amount').val(ttlAmount - amount);
                        }
                     
                        $(this).closest('tr').remove();
                        $srNo = 1;
                        $(prntTbl).find('.sr-no').each(function(){
                            $(this).html($srNo);
                            $srNo++;
                        });
                    }


                    var rows     = $('#'+tableId).find('tbody').find('tr');
                    var rowCount = rows.length;
                    rows.find('select, input').each(function(){
                        $(this).closest('div').removeClass('has-error');
                        $(this).closest('td').find('div.help-block').html('');
                    });
                    if(rowCount == 1)
                    {
                        // code to remove validation
                        rows.find('select, input').each(function(){
                            $('#'+formId).yiiActiveForm('remove', $(this).attr('id'));
                        });
                    }
                });
                // code to delete row ends

            // code to set serial numner starts
            $(this).closest('tbody').find('.sr-no').each(function(){
                $(this).html($srNo);
                $srNo++;
            });
            // code to set serial numner ends
        ;
    $('.dform').attr('data-value', incIndex)
    $(this).hide();
});




var deleteRow = function (deleteLink, addLink){
    $("."+deleteLink).on("click",function(){
        var formId   = $(this).closest('form').attr('id');
        var tableId  = $(this).closest('table').attr('id');
        $srNo = 1;

        if($(this).closest('tbody').find("tr").length  > 1){
            $(this).closest('tr').find('select, input').each(function(){
                $('#'+formId).yiiActiveForm('remove', $(this).attr('id'));
            });

            if($(this).closest("td").find('.'+addLink).length > 0 ){
                $(this).closest('tr').prev('tr').find("."+deleteLink).before($(this).closest("td").find('.'+addLink));
            }
    alert("Sachin");
         
            $(this).closest('tr').remove();    
    alert("Sachin");
            $(this).closest('tbody').find('.sr-no').each(function(){
                $(this).html($srNo);
                $srNo++;
            });

        }


        var rows     = $('#'+tableId).find('tbody').find('tr');
        var rowCount = rows.length;
        rows.find('select, input').each(function(){
            $(this).closest('div').removeClass('has-error');
            $(this).closest('td').find('div.help-block').html('');
        });
    });
}


 $(document).ready(function (){
        $("form").on("beforeSubmit",(function(z){
            $("table.dform").each(function(){
                var count = -1;
                $(this).find("tr").each(function(){
                            //alert();
                            $(this).find('select, input').each(function(){
                                //alert(count);
                                var elseName = $(this).attr('name');
                                var nameStart = elseName.substr(0,elseName.indexOf("[")+1);
                                var nameEnd = elseName.substr(elseName.indexOf("]"), elseName.length);
                                //alert(elseName);
                                var finalName = nameStart+count+nameEnd;
                                $(this).attr('name',finalName);
                                //alert(finalName);
                            });
                            count++;
                        });   
            });
            
        }));
    });


// code for adding dynamic rows

$(".add_link").click(function (){
    $item_grid   =   $(this).closest("div.item_grid");
    $incIndex    =   parseInt($($item_grid).attr('data-rows'));
    $incIndex++;
    $temp_row  = $(this).closest("div.row");
    $clone_row = $temp_row.clone();

    $clone = $clone_row.insertAfter($temp_row);

    $clone_row.find('select, input, textarea').each(function(){
        $id         =   $(this).attr('id');
        $split_id   =   $id.split('-');
        $oldIndex   =   $split_id[1];
        $new_id     =   $id.replace(parseInt($oldIndex), parseInt($incIndex));
        $(this).removeAttr('id');
        $(this).attr('id', $new_id);

        // code to set new name
        $newName    =   $(this).attr('name').replace('['+parseInt($oldIndex)+']', '['+parseInt($incIndex))+']';
        $(this).removeAttr('name');
        $(this).attr('name', $newName);

        // set values to null
        $(this).val('');
    });
});