<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%url_history}}".
 *
 * @property int $id
 * @property int $url_id
 * @property int $referral_id
 * @property string $created_on
 */
class UrlHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%url_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url_id', 'referral_id', 'created_on'], 'required'],
            [['url_id', 'referral_id'], 'integer'],
            [['created_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url_id' => Yii::t('app', 'Url ID'),
            'referral_id' => Yii::t('app', 'Referral ID'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }
}
