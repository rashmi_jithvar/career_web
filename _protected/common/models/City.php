<?php

namespace common\models;
use yii\helpers\ArrayHelper;
use common\models\Master;
use Yii;

/**
 * This is the model class for table "tbl_master".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $abbr
 * @property int $parent_id
 * @property int $status
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'parent_id', 'status'], 'required'],
            [['parent_id', 'status', 'created_by'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'abbr' => 'Abbr',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'created_by' => 'Created By',
        ];
    }

    /* get ddl*/ 
    public function getCity($id)
    {
        $getGroup   =   Master::find()
                                ->where(['parent_id' => $id])
                                ->where(['abbr'=> 'City'])  
                                ->orderBy('name ASC')
                                ->asArray()
                                ->all();
        return ArrayHelper::map($getGroup, 'id', 'name');
    } 


}
