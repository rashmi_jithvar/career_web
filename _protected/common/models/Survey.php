<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_survey".
 *
 * @property int $s_id
 * @property int $question_id
 * @property int $selected_ans
 * @property int $user_id
 * @property string $user_name
 * @property int $created_by
 * @property string $created_on
 * @property int $created_ip
 * @property int $update_id
 * @property string $update_on
 * @property int $update_by
 * @property int $correct_answer
 * @property string $user_email
 * @property int $status
 */
class Survey extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_survey';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question_id', 'selected_ans', 'user_id'], 'required'],
            [['question_id', 'user_id', 'created_ip'], 'integer'],
            //[['created_on', 'update_on'], 'safe'],
            [['user_name', 'user_email'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            's_id' => 'S ID',
            'question_id' => 'Question ID',
            'selected_ans' => 'Selected Ans',
            'user_id' => 'User ID',
            'user_name' => 'User Name',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'created_ip' => 'Created Ip',
            'update_id' => 'Update ID',
            'update_on' => 'Update On',
            'update_by' => 'Update By',
            'correct_answer' => 'Correct Answer',
            'user_email' => 'User Email',
            'status' => 'Status',
        ];
    }
}
