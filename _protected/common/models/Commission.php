<?php

namespace common\models;
use common\models\Plan;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_commission".
 *
 * @property int $id
 * @property string $discount
 * @property string $plan_amount
 * @property string $discount_percentage
 * @property string $commission_amount
 * @property int $commision_type
 * @property int $plan_id
 * @property int $to_sponser_id
 * @property int $from_sponser_id
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 */
class Commission extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_commission';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount', 'plan_amount', 'commission_amount', 'commision_type', 'plan_id', 'from_sponser_id', 'created_by'], 'required'],
            [['discount', 'plan_amount', 'discount_percentage', 'commission_amount'], 'number'],
            [['commision_type', 'plan_id', 'to_sponser_id', 'from_sponser_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount' => 'Discount',
            'plan_amount' => 'Plan Amount',
            'discount_percentage' => 'Discount Percentage',
            'commission_amount' => 'Commission Amount',
            'commision_type' => 'Commision Type',
            'plan_id' => 'Plan ID',
            'to_sponser_id' => 'To Sponser ID',
            'from_sponser_id' => 'From Sponser ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }

    /* get Direct Commission*/ 
    public function getDirectCommission($to_sponser,$from_sponser,$plan_amount,$plan_id)
    {
        $get_plan = Plan::findOne($plan_id);
        $discount = $get_plan->discount_type;
        $commision_amount = $plan_amount - ($plan_amount * ($discount / 100));
        $model = new Commission();
        $model->discount = $discount;
        $model->plan_amount =  $plan_amount;
        $model->commission_amount = $commision_amount;
        $model->commision_type = '1';
        $model->plan_id = $plan_id;
        $model->to_sponser_id = $to_sponser;
        $model->from_sponser_id = $from_sponser;
        $model->created_by = Yii::$app->user->id;
        $model->created_on = new Expression("now()");
        $model->save();
        if($model->save()){
            return true;
        }
        else{
            return False;
        }
        
    }    
}
