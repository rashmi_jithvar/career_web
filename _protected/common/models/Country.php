<?php

namespace common\models;

use Yii;
use common\models\User;
use common\models\Template;
/**
 * This is the model class for table "tbl_country".
 *
 * @property int $c_id
 * @property string $name
 * @property string $code
 * @property string $tel
 * @property string $flag_icon
 * @property string $flag_path
 * @property string $language
 * @property string $created_on
 * @property string $created_by
 * @property int $created_ip
 * @property string $update_on
 * @property string $update_by
 * @property int $update_ip
 * @property int $status
 * @property int $is_delete
 */
class Country extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_PENDING = 2;
    const STATUS_DELETE = 3;

    public $createdUser;
    public $templateM;
    public $image_file;
    public $og_file;
    public static function tableName()
    {
        return 'tbl_country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'tel', 'flag_icon', 'language','status'], 'required'],
            [['created_on', 'created_by'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['code'], 'string', 'max' => 50],
            [['tel'], 'string', 'max' => 12],
            [['flag_icon'] ,'file', 'extensions' => ['png', 'jpg', 'jpeg']],
            [['language'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'c_id' => 'C ID',
            'name' => 'Name',
            'code' => 'Code',
            'tel' => 'Tel',
            'flag_icon' => 'Flag Icon',
            'language' => 'Language',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'status' => 'Status',
        ];
    }

  public function getStatus() {
        if ($this->status == self::STATUS_ACTIVE) {
            return "Active";
        }
        if ($this->status == self::STATUS_INACTIVE) {
            return "Inactive";
        }
        if ($this->status == self::STATUS_PENDING) {
            return "Pending";
        }
        if ($this->status == self::STATUS_DELETE) {
            return "Delete";
        }
    }

    public function getCreatedBy() {
        return $this->hasOne(User::class, ["id" => "created_by"]);
    }

    public function getTemplate() {
        return $this->hasOne(Template::class, ["id" => "template_id"]);
    }
}
