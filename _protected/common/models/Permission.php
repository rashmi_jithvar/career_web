<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "{{%permission}}".
 *
 * @property int $id
 * @property string $name
 * @property string $controller_name
 * @property string $action_name
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 */
class Permission extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'controller_name', 'action_name'], 'required'],
            [['created_by', 'updated_by', 'group_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name', 'controller_name', 'action_name'], 'string', 'max' => 50],
        ];
    }


    // function to run before Save
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!$insert) { // only on insert
                $this->updated_on = new Expression('now()');
                $this->updated_by = Yii::$app->user->id;
            }
            else{
                $this->created_on   = new Expression('now()');
                $this->created_by   = Yii::$app->user->id;
            }
            return true;
        }
        return false;
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'group_id' => Yii::t('app', 'Group Name'),
            'controller_name' => Yii::t('app', 'Controller Name'),
            'action_name' => Yii::t('app', 'Action Name'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_on' => Yii::t('app', 'Created On'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_on' => Yii::t('app', 'Updated On'),
        ];
    }

    /**
    * @ hasOne relation with parent group table
    * @return mixed
    */
    public function getGroup()
    {
        return $this->hasOne(PermissionGroup::className(), ['id' => 'group_id']);
    }
}
