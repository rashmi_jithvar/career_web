<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_member_image".
 *
 * @property int $id
 * @property string $upload_photo
 * @property string $id_proof_photo
 * @property string $pan_card_photo
 * @property string $passbook_photo
 * @property int $member_id
 */
class MemberImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_member_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
           // [['upload_photo', 'id_proof_photo', 'pan_card_photo', 'passbook_photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'upload_photo' => 'Upload Photo',
            'id_proof_photo' => 'Id Proof Photo',
            'pan_card_photo' => 'Pan Card Photo',
            'passbook_photo' => 'Passbook Photo',
            'member_id' => 'Member ID',
        ];
    }
}
