<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_menu".
 *
 * @property int $id
 * @property string $name
 * @property string $link_type
 * @property string $link_value
 * @property string $position_id
 * @property int $parent_id
 * @property string $slug
 * @property int $status
 * @property int $created_by
 * @property string $created_on
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'link_type', 'link_value', 'parent_id', 'slug', 'status', 'created_by'], 'required'],
            [['parent_id', 'status', 'created_by'], 'integer'],
            [['created_on'], 'safe'],
            [['name', 'link_type', 'link_value', 'position_id', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'link_type' => Yii::t('app', 'Link Type'),
            'link_value' => Yii::t('app', 'Link Value'),
            'position_id' => Yii::t('app', 'Position ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'slug' => Yii::t('app', 'Slug'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }
}
