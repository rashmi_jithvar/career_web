<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_openings".
 *
 * @property int $id
 * @property string $job_code
 * @property string $title
 * @property string $description
 * @property int $designation
 * @property int $min_experience
 * @property int $max_experience
 * @property string $min_salary
 * @property string $max_salary
 * @property int $salary_frequence
 * @property int $location
 * @property int $walkin
 * @property string $walkin_from
 * @property string $walkin_to
 * @property int $openings
 * @property string $opening_date
 * @property int $created_by
 * @property string $created_on
 * @property string $created_ip
 * @property string $updated_on
 * @property int $updated_by
 * @property string $updated_ip
 */
class Openings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_openings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_code', 'title', 'description', 'designation', 'min_experience', 'max_experience', 'min_salary', 'max_salary', 'salary_frequence', 'location', 'walkin', 'openings', 'opening_date', 'created_by'], 'required'],
            [['description'], 'string'],
            [['designation', 'min_experience', 'max_experience', 'salary_frequence', 'location', 'walkin', 'openings', 'created_by', 'updated_by'], 'integer'],
            [['min_salary', 'max_salary'], 'number'],
            [['walkin_from', 'walkin_to', 'opening_date'], 'safe'],
            [['job_code'], 'string', 'max' => 100],
            [['title'], 'string', 'max' => 255],
            [['created_ip', 'updated_ip'], 'string', 'max' => 20],
            [['job_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'job_code' => 'Job Code',
            'title' => 'Title',
            'description' => 'Description',
            'designation' => 'Designation',
            'min_experience' => 'Min Experience',
            'max_experience' => 'Max Experience',
            'min_salary' => 'Min Salary',
            'max_salary' => 'Max Salary',
            'salary_frequence' => 'Salary Frequence',
            'location' => 'Location',
            'walkin' => 'Walkin',
            'walkin_from' => 'Walkin From',
            'walkin_to' => 'Walkin To',
            'openings' => 'Openings',
            'opening_date' => 'Opening Date',
            'created_by' => 'Created By',
        ];
    }
}
