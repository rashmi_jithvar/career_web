<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%permission_group}}".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class PermissionGroup extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%permission_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getPermission()
    {
        return $this->hasMany(Permission::className(), ['group_id' => 'id']);
    }
}
