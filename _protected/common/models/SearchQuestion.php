<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Question;

/**
 * SearchQuestion represents the model behind the search form of `backend\models\Question`.
 */
class SearchQuestion extends Question
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['q_id', 'created_by', 'update_by', 'view', 'user_like', 'slug', 'is_active'], 'integer'],
            [['question', 'opt_a', 'opt_b', 'opt_c', 'opt_d', 'opt_e', 'answer', 'created_on', 'created_id', 'update_on', 'update_ip'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Question::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'q_id' => $this->q_id,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'update_on' => $this->update_on,
            'update_by' => $this->update_by,
            'view' => $this->view,
            'user_like' => $this->user_like,
            'slug' => $this->slug,
            'is_active' => $this->is_active,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'opt_a', $this->opt_a])
            ->andFilterWhere(['like', 'opt_b', $this->opt_b])
            ->andFilterWhere(['like', 'opt_c', $this->opt_c])
            ->andFilterWhere(['like', 'opt_d', $this->opt_d])
            ->andFilterWhere(['like', 'opt_e', $this->opt_e])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->andFilterWhere(['like', 'created_id', $this->created_id])
            ->andFilterWhere(['like', 'update_ip', $this->update_ip]);

        return $dataProvider;
    }
}
