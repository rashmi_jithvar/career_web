<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "tbl_template".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property int $created_by
 * @property string $created_on
 */
class Template extends \yii\db\ActiveRecord {

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_PENDING = 2;
    const STATUS_DELETE = 3;

    public $createdUser;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'tbl_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'status', 'created_by'], 'required'],
            [['status', 'created_by'], 'integer'],
            [['created_on'], 'safe'],
            [['name', 'createdUser'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_on' => Yii::t('app', 'Created On'),
            'createdUser' => Yii::t('app', 'Created By'),
        ];
    }

    public function getStatus() {
        if ($this->status == self::STATUS_ACTIVE) {
            return "Active";
        }
        if ($this->status == self::STATUS_INACTIVE) {
            return "Inactive";
        }
        if ($this->status == self::STATUS_PENDING) {
            return "Pending";
        }
        if ($this->status == self::STATUS_DELETE) {
            return "Delete";
        }
    }

    public function getCreatedBy() {
        return $this->hasOne(User::class, ["id" => "created_by"]);
    }

}
