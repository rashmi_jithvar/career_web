<?php

namespace common\models;

use Yii;
use common\models\User;
use common\models\Template;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property int $id
 * @property string $name
 * @property string $sub_name
 * @property int $template_id
 * @property string $content
 * @property string $meta_keywords 
 * @property string $meta_desc 
 * @property string $og_type 
 * @property string $og_description 
 * @property string $og_url 
 * @property string $og_site 
 * @property string $og_image 
 * @property string $image
 * @property int $status
 * @property string $slug
 * @property int $created_by
 * @property string $created_on
 */
class Page extends \yii\db\ActiveRecord {

    const SCENARIO_CREATE = 'create';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_PENDING = 2;
    const STATUS_DELETE = 3;

    public $createdUser;
    public $templateM;
    public $image_file;
    public $og_file;

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%page}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'template_id','content', 'meta_keywords', 'meta_desc', 'og_type', 'og_description', 'og_url', 'og_site', 'status', 'slug', 'created_by'], 'required'], 
            [['og_file', 'image_file', 'og_image', 'image',], 'required', 'on' => self::SCENARIO_CREATE],
            [['template_id', 'status', 'created_by'], 'integer'],
            [['content', 'meta_desc', 'og_description'], 'string'],
            [['created_on', 'createdUser', 'templateM'], 'safe'],
            [['name', 'meta_keywords', 'og_type', 'og_url', 'og_site', 'og_image', 'image', 'slug'], 'string', 'max' => 255],
            [['image_file', 'og_file'], 'file', 'extensions' => ['png', 'jpg', 'jpeg']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sub_name' => Yii::t('app', 'Sub Name'),
            'template_id' => Yii::t('app', 'Template'),
            'templateM' => Yii::t('app', 'Template'),
            'content' => Yii::t('app', 'Content'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'meta_desc' => Yii::t('app', 'Meta Desc'),
            'og_type' => Yii::t('app', 'Og Type'),
            'og_description' => Yii::t('app', 'Og Description'),
            'og_url' => Yii::t('app', 'Og Url'),
            'og_site' => Yii::t('app', 'Og Site'),
            'og_image' => Yii::t('app', 'Og Image'),
            'image_file' => Yii::t('app', 'Image'),
            'og_file' => Yii::t('app', 'Og Image'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'slug' => Yii::t('app', 'Slug'),
            'created_by' => Yii::t('app', 'Created By'),
            'createdUser' => Yii::t('app', 'Created By'),
            'created_on' => Yii::t('app', 'Created On'),
        ];
    }

    public function getStatus() {
        if ($this->status == self::STATUS_ACTIVE) {
            return "Active";
        }
        if ($this->status == self::STATUS_INACTIVE) {
            return "Inactive";
        }
        if ($this->status == self::STATUS_PENDING) {
            return "Pending";
        }
        if ($this->status == self::STATUS_DELETE) {
            return "Delete";
        }
    }

    public function getCreatedBy() {
        return $this->hasOne(User::class, ["id" => "created_by"]);
    }

    public function getTemplate() {
        return $this->hasOne(Template::class, ["id" => "template_id"]);
    }

}
