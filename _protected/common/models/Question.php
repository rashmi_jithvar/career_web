<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_question".
 *
 * @property int $q_id
 * @property string $question
 * @property string $opt_a
 * @property string $opt_b
 * @property string $opt_c
 * @property string $opt_d
 * @property string $opt_e
 * @property string $answer
 * @property int $created_by
 * @property string $created_on
 * @property string $created_id
 * @property string $update_on
 * @property int $update_by
 * @property string $update_ip
 * @property int $view
 * @property int $user_like
 * @property int $slug
 * @property int $is_active
 */
class Question extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    const SCENARIO_CREATE = 'create';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_PENDING = 2;
    const STATUS_DELETE = 3;
    public static function tableName()
    {
        return 'tbl_question';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'opt_a', 'opt_b', 'answer','is_active'], 'required'],
            [['question'], 'string'],
          //  [['created_on', 'update_on'], 'safe'],
            [['opt_a', 'opt_b', 'opt_c', 'opt_d', 'opt_e', 'answer'], 'string', 'max' => 255],
           // [['created_id', 'update_ip'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'q_id' => 'Q ID',
            'question' => 'Question',
            'opt_a' => 'Opt A',
            'opt_b' => 'Opt B',
            'opt_c' => 'Opt C',
            'opt_d' => 'Opt D',
            'opt_e' => 'Opt E',
            'answer' => 'Answer',
           // 'created_by' => 'Created By',
            //'created_on' => 'Created On',
            //'created_id' => 'Created ID',
            //'update_on' => 'Update On',
            //'update_by' => 'Update By',
            //'update_ip' => 'Update Ip',
            //'view' => 'View',
           // 'user_like' => 'User Like',
           // 'slug' => 'Slug',
            'is_active' => 'Is Active',
        ];
    }

    public function getStatus() {
        if ($this->status == self::STATUS_ACTIVE) {
            return "Active";
        }
        if ($this->status == self::STATUS_INACTIVE) {
            return "Inactive";
        }
        if ($this->status == self::STATUS_PENDING) {
            return "Pending";
        }
        if ($this->status == self::STATUS_DELETE) {
            return "Delete";
        }
    }
      public static function findBySlug($slug) {
        return question::find()->where(['slug' => $slug])->andWhere(['is_active'=>'0']);
    }
}
