<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Openings;

/**
 * OpeningsSearch represents the model behind the search form of `common\models\Openings`.
 */
class OpeningsSearch extends Openings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'designation', 'min_experience', 'max_experience', 'salary_frequence', 'location', 'walkin', 'openings', 'created_by', 'updated_by'], 'integer'],
            [['job_code', 'title', 'description', 'walkin_from', 'walkin_to', 'opening_date', 'created_on', 'created_ip', 'updated_on', 'updated_ip'], 'safe'],
            [['min_salary', 'max_salary'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Openings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'designation' => $this->designation,
            'min_experience' => $this->min_experience,
            'max_experience' => $this->max_experience,
            'min_salary' => $this->min_salary,
            'max_salary' => $this->max_salary,
            'salary_frequence' => $this->salary_frequence,
            'location' => $this->location,
            'walkin' => $this->walkin,
            'walkin_from' => $this->walkin_from,
            'walkin_to' => $this->walkin_to,
            'openings' => $this->openings,
            'opening_date' => $this->opening_date,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'job_code', $this->job_code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_ip', $this->created_ip])
            ->andFilterWhere(['like', 'updated_ip', $this->updated_ip]);

        return $dataProvider;
    }
}
