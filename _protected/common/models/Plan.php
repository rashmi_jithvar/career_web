<?php

namespace common\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "tbl_plan".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $discount_type
 * @property string $created_on
 * @property int $created_by
 * @property int $updated_by
 * @property string $updated_on
 */
class Plan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'discount_type'], 'required'],
            [['description'], 'string'],
            [['discount_type'], 'number'],
            [['created_on', 'updated_on'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'discount_type' => 'Discount Type',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }

    /* get ddl*/ 
    public function getPlan()
    {
        $getGroup   =   Plan::find()->select(['title', 'id'])
                                ->asArray()
                                ->all();
        return ArrayHelper::map($getGroup, 'id', 'title');
    }

}
