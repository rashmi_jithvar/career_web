<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Country;

/**
 * CountrySearch represents the model behind the search form of `common\models\Country`.
 */
class CountrySearch extends Country
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_id', 'created_ip', 'update_ip', 'status', 'is_delete'], 'integer'],
            [['name', 'code', 'tel', 'flag_icon', 'flag_path', 'language', 'created_on', 'created_by', 'update_on', 'update_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'c_id' => $this->c_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'created_ip' => $this->created_ip,
            'update_on' => $this->update_on,
            'update_by' => $this->update_by,
            'update_ip' => $this->update_ip,
            'status' => $this->status,
            'is_delete' => $this->is_delete,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'tel', $this->tel])
            ->andFilterWhere(['like', 'flag_icon', $this->flag_icon])
            ->andFilterWhere(['like', 'flag_path', $this->flag_path])
            ->andFilterWhere(['like', 'language', $this->language]);

        return $dataProvider;
    }
}
