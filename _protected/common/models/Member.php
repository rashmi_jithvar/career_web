<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Plan;
/**
 * This is the model class for table "tbl_member".
 *
 * @property int $id
 * @property string $sponser_id
 * @property string $upline_id
 * @property string $left_right_leg
 * @property string $applicant_name
 * @property string $gender
 * @property int $name_type
 * @property string $father_spouse_name
 * @property string $dob
 * @property string $address
 * @property int $city
 * @property int $state
 * @property string $mobile
 * @property string $email
 * @property string $pan_no
 * @property string $aadhaar_no
 * @property string $bank_name
 * @property string $account_no
 * @property string $IFSC_code
 * @property string $account_holder_name
 * @property string $date
 * @property string $nomine_name
 * @property int $qualification
 * @property string $password
 * @property int $plan_id
 * @property string $plan_amount
 * @property int $leg_count
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 * @property string $unique_id
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sponser_id', 'upline_id', 'left_right_leg', 'applicant_name', 'gender', 'father_spouse_name', 'state', 'mobile'], 'required'],
            [['name_type','left_right_leg', 'gender', 'address','city', 'state','qualification','unique_id'], 'string'],
            [['plan_id', 'leg_count', 'created_by', 'updated_by'], 'integer'],
            [['dob', 'date', 'created_on', 'updated_on'], 'safe'],
            [['plan_amount'], 'number'],
            [['sponser_id', 'upline_id', 'pan_no', 'aadhaar_no'], 'string', 'max' => 50],
            [['applicant_name', 'father_spouse_name', 'email', 'bank_name', 'account_holder_name', 'nomine_name', 'password'], 'string', 'max' => 255],
            [['mobile'], 'string', 'max' => 12],
            [['account_no', 'IFSC_code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sponser_id' => 'Sponser ID',
            'upline_id' => 'Upline ID',
            'left_right_leg' => 'Left Right Leg',
            'applicant_name' => 'Applicant Name',
            'gender' => 'Gender',
            'name_type' => 'Name Type',
            'father_spouse_name' => 'Father Spouse Name',
            'dob' => 'Dob',
            'address' => 'Address',
            'city' => 'City',
            'state' => 'State',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'pan_no' => 'Pan No',
            'aadhaar_no' => 'Aadhaar No',
            'bank_name' => 'Bank Name',
            'account_no' => 'Account No',
            'IFSC_code' => 'I F S C Code',
            'account_holder_name' => 'Account Holder Name',
            'date' => 'Date',
            'nomine_name' => 'Nomine Name',
            'qualification' => 'Qualification',
            'password' => 'Password',
            'plan_id' => 'Plan ID',
            'plan_amount' => 'Plan Amount',
            'leg_count' => 'Leg Count',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'unique_id' => 'Unique Id'
        ];
    }

    /* get ddl*/ 
    public function getMember()
    {
        $getGroup   =   Member::find()->select(['concat(unique_id,"/",applicant_name) as name', 'id'])
                                ->asArray()
                                ->all();
        return ArrayHelper::map($getGroup, 'id', 'name');
    }

    public function getPlan(){
        return $this->hasOne(Plan::class, ["id" => "plan_id"]);
    }

    public function getImage(){
        return $this->hasOne(MemberImage::class, ["member_id" => "id"]);
    }
          
    public function getStatename(){
        return $this->hasOne(State::class, ["id" => "state"]);
    }

    public function getCityname(){
        return $this->hasOne(City::class, ["id" => "city"]);
    }

    public function getFindteam($id)
    {
        $team = Member::find()->where(['id'=>$id])->one();
        if(!empty($team)){
            $team_left = Member::find()->where(['sponser_id'=>$team->id])->where(['left_right_leg'=>'left'])->one();
            $team_right = Member::find()->where(['sponser_id'=>$team->id])->where(['left_right_leg'=>'right'])->one();

            if(empty($team_left)){
                    $team_left = 0;
            }
            if(empty($team_right)){
                $team_right = 0;
            }
            return $team = array('left' =>$team_left ,'right'=>$team_right );
        }

    }

     public function getFindLeft($id)
    {
        $left = Member::find()
                        ->where(['sponser_id'=>$id])
                        ->andWhere(['left_right_leg'=>'left'])
                        ->one();
        if(!empty($left)){
            return $left;
        }  
        else{
            return false;
        }              

    }   

     public function getFindRight($id)
    {
        $right = Member::find()
                        ->where(['sponser_id'=>$id])
                        ->andWhere(['left_right_leg'=>'right'])
                        ->one();
        if(!empty($right)){
            return $right;
        }  
        else{
            return false;
        }              

    }  


    public function getFindteamthird($id)
    { echo $id;exit;
        $team = Member::find()->where(['id'=>$id])->one();
        if(!empty($team)){
            $team_left = Member::find()->where(['sponser_id'=>$team->id])->andWhere(['left_right_leg'=>'left'])->one();
            $team_right = Member::find()->where(['sponser_id'=>$team->id])->andWhere(['left_right_leg'=>'right'])->one();

            if(empty($team_left)){
                    $team_left = 0;
            }
            if(empty($team_right)){
                $team_right = 0;
            }
            return $team = array('left' =>$team_left ,'right'=>$team_right );
        }

    }

}
