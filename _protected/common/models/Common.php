<?php

namespace common\models;

use yii\base\Model;
use Yii;

/**
 * Common is the model behind the login form.
 */
class Common extends Model {

    static function phone_number($number) {
// Allow only Digits, remove all other characters.
        $number = preg_replace("/[^\d]/", "", $number);

// get number length.
        $length = strlen($number);

// if number = 10
        if ($length == 10) {
            $number = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $number);
        }

        return $number;
    }

    static function dod_format($number) {
// Allow only Digits, remove all other characters.
        $number = preg_replace("/[^\d]/", "", $number);

// get number length.
        $length = strlen($number);

// if number = 10
        if ($length == 11) {
            $number = preg_replace("/^1?(\d{9})(\d{2})$/", "$1-$2", $number);
        }

        return $number;
    }

    static function ss_format($number) {
// Allow only Digits, remove all other characters.
        $number = preg_replace("/[^\d]/", "", $number);

// get number length.
        $length = strlen($number);

// if number = 10
        if ($length == 9) {
            $number = preg_replace("/^1?(\d{3})(\d{2})(\d{4})$/", "$1-$2-$3", $number);
        }

        return $number;
    }

    static function makeDate($date) {
        $date = str_replace("/", "-", $date);
        $new_date = explode("-", $date);
        return $new_date[2] . "-" . $new_date[0] . "-" . $new_date[1];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($param) {
        $message = Yii::$app->mailer->compose('your_view', ['param' => $param]);
        $message->attach('/path/to/source/file.pdf');

// create attachment on-the-fly
// $message->attachContent('Attachment content', ['fileName' => 'attach.txt', 'contentType' => 'text/plain']);

        $message->setTo($param['to_email'])
                ->setFrom([$param['from_name'] => $param['from']])
                ->setSubject($param['subject'])
// ->setTextBody($param['body'])
                ->send();
    }

    public static function createSlug($string) {

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', strtolower($string));

        return $slug;
    }

    public static function getUrl($fileName, $fileExt, $folderName) {
        return \Yii::$app->homeUrl . "/" . $folderName . "/" . $fileName . "." . $fileExt;
    }

}
