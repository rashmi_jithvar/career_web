<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Template;

/**
 * TemplateSearch represents the model behind the search form of `common\models\Template`.
 */
class TemplateSearch extends Template {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'status', 'created_by'], 'integer'],
            [['name', 'created_on', 'createdUser'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Template::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['createdBy']);

        // grid filtering conditions
        $query->andFilterWhere([
            '`tbl_template`.id' => $this->id,
            '`tbl_template`.status' => $this->status,
            '`tbl_template`.created_by' => $this->created_by,
//            '`tbl_template`.created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', '`tbl_template`.name', $this->name])
            ->andFilterWhere(['like', '`tbl_user`.username', $this->createdUser])
            ->andFilterWhere(['like', '`tbl_template`.created_on', $this->created_on]);

        return $dataProvider;
    }

}
