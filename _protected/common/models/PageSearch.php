<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Page;

/**
 * PageSearch represents the model behind the search form of `common\models\Page`.
 */
class PageSearch extends Page {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id', 'template_id', 'status', 'created_by'], 'integer'],
            [['name', 'content', 'image', 'slug', 'created_on', 'createdUser', 'templateM'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Page::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['template', 'createdBy']);

        // grid filtering conditions
        $query->andFilterWhere([
            '`tbl_page`.id' => $this->id,
            '`tbl_page`.template_id' => $this->template_id,
            '`tbl_page`.status' => $this->status,
            '`tbl_page`.created_by' => $this->created_by,
//            'created_on' => $this->created_on,
        ]);

        $query->andFilterWhere(['like', '`tbl_page`.name', $this->name])
                ->andFilterWhere(['like', '`tbl_page`.content', $this->content])
                ->andFilterWhere(['like', '`tbl_page`.image', $this->image])
                ->andFilterWhere(['like', '`tbl_user`.username', $this->createdUser])
                ->andFilterWhere(['like', '`tbl_template`.name', $this->templateM])
                ->andFilterWhere(['like', '`tbl_page`.slug', $this->slug]);

        return $dataProvider;
    }

}
