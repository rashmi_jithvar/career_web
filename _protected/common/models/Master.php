<?php

namespace common\models;

use Yii;
use yii\helpers\Arrayhelper;

/**
 * This is the model class for table "{{%master}}".
 *
 * @property int $id
 * @property string $name
 * @property string $abbr
 * @property int $parent_id
 * @property int $status
 */
class Master extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%master}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'abbr', 'status'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['name', 'abbr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'abbr' => Yii::t('app', 'Abbr'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    // function to bind master
    public function bindMaster($abbr)
    {
        $master = Master::find()
                        ->where(['abbr' => $abbr])
                        ->andWhere(['status' => self::STATUS_ACTIVE])
                        ->all();

        return Arrayhelper::map($master, 'id', 'name');
    }
}
