<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_master".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $abbr
 * @property int $parent_id
 * @property int $status
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'parent_id', 'status'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 150],
            [['code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'abbr' => 'Abbr',
            'parent_id' => 'Parent ID',
            'status' => 'Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}
