<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Member;

/**
 * MemberSearch represents the model behind the search form of `common\models\Member`.
 */
class MemberSearch extends Member
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name_type', 'city', 'state', 'qualification', 'created_by', 'updated_by'], 'integer'],
            [['sponser_id', 'upline_id', 'left_right_leg', 'applicant_name', 'gender', 'father_spouse_name', 'dob', 'address', 'mobile', 'email', 'pan_no', 'aadhaar_no', 'bank_name', 'account_no', 'IFSC_code', 'account_holder_name', 'date', 'nomine_name', 'upload_photo', 'id_proof_photo', 'pan_card_photo', 'passbook_photo', 'password', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Member::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'name_type' => $this->name_type,
            'dob' => $this->dob,
            'city' => $this->city,
            'state' => $this->state,
            'date' => $this->date,
            'qualification' => $this->qualification,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'updated_by' => $this->updated_by,
            'updated_on' => $this->updated_on,
        ]);

        $query->andFilterWhere(['like', 'sponser_id', $this->sponser_id])
            ->andFilterWhere(['like', 'upline_id', $this->upline_id])
            ->andFilterWhere(['like', 'left_right_leg', $this->left_right_leg])
            ->andFilterWhere(['like', 'applicant_name', $this->applicant_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'father_spouse_name', $this->father_spouse_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'pan_no', $this->pan_no])
            ->andFilterWhere(['like', 'aadhaar_no', $this->aadhaar_no])
            ->andFilterWhere(['like', 'bank_name', $this->bank_name])
            ->andFilterWhere(['like', 'account_no', $this->account_no])
            ->andFilterWhere(['like', 'IFSC_code', $this->IFSC_code])
            ->andFilterWhere(['like', 'account_holder_name', $this->account_holder_name])
            ->andFilterWhere(['like', 'nomine_name', $this->nomine_name])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
