<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_user".
 *
 * @property int $id
 * @property string $old_password
 * @property string $password
 * @property string $confirm_password
 * @property int $updated_at
 *
 * @property Organisation[] $organisations
 */
class ChangePassword extends User {

    public $old_password;
    public $password;
    public $confirm_password;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['old_password', 'password', 'confirm_password'], 'required'],
            [['old_password', 'password', 'confirm_password'], 'string', 'max' => 30, 'min' => '6'],
            ['updated_at', 'integer'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'old_password' => Yii::t('app', 'Old Password'),
            'password' => Yii::t('app', 'Password'),
            'confirm_password' => Yii::t('app', 'Confirm Password'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Check Old Password Method
     * @return boolean
     */
    public function checkOldPassword() {
        return $this->validatePassword($this->old_password);
    }

    /**
     * Change Password Method
     * @return string|boolean
     */
    public function changePassword() {
        if ($this->checkOldPassword()) {
            $this->setPassword($this->password);

            $this->save();

            if ($this->errors) {
                return "Update Password Error:" . json_encode($this->errors);
            } else {
                return true;
            }
        } else {
            return "Old Password is not matched.";
        }
    }

    /**
     * Find Self Method 
     * @param string|array $condition
     * @param integer $offset
     * @param integer $limit
     * @return self
     */
    public static function findSelf($condition, $offset = 0, $limit = 1000) {
        return self::find()
                        ->where($condition)
                        ->offset($offset)
                        ->limit($limit);
    }

    /**
     * Find Self One Method 
     * @param string|array $condition
     * @param integer $offset
     * @param integer $limit
     * @return self
     */
    public static function findSelfOne($condition) {
        return self::find()
                        ->where($condition)
                        ->one();
    }

    /**
     * Find Self All Method 
     * @param string|array $condition
     * @param integer $offset
     * @param integer $limit
     * @return self
     */
    public static function findSelfAll($condition = [], $offset = 0, $limit = 1000) {
        return self::find()
                        ->where($condition)
                        ->offset($offset)
                        ->limit($limit)
                        ->all();
    }

}
