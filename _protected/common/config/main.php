<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
             
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '565288713970212',
                    'clientSecret' => '9157285e9cd86e63f961464221453a16',
                ],
                // etc.
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User', // User must implement the IdentityInterface
            'enableAutoLogin' => true,
        // 'authTimeout' => 1200,
        ],
        'session' => [
            'class' => 'yii\web\Session'
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '-',
        // 'dateFormat' => 'PHP:MM-DD-YYYY',
        // 'datetimeFormat' => 'mm-dd-yyyy h:i:s'
        // 'dateTime' => 'mm-dd-yyyy',
        ],
    // 'urlManager' => [
    //     // 'suffix' => '.jtvr',
    //     'enablePrettyUrl' => true,
    //     'showScriptName' => false,
    //     'rules' => [
    //     ],
    // ],
    ],
];
