<?php
// /common/components/MyComponent.php

namespace app\common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class MyComponent extends Component
{
    public function welcome()
    {
        echo "Hello.. Welcome to MyComponent";
    }

}