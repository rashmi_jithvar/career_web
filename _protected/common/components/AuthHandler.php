<?php
 
namespace common\components;
use common\models\Auth;
use common\models\User;
use frontend\models\SignupForm;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use  yii\web\Session;
use common\models\Survey;
use common\models\Question;


class AuthHandler
{
    private $client;
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
         $email = ArrayHelper::getValue($attributes, 'email');
         $id = ArrayHelper::getValue($attributes, 'id');
         $name = ArrayHelper::getValue($attributes, 'name');
         $gender = ArrayHelper::getValue($attributes, 'gender');
         $birthday = ArrayHelper::getValue($attributes, 'birthday');
         //$location = ArrayHelper::getValue($attributes, 'location');
         //print_r($location);
         $education = ArrayHelper::getValue($attributes, 'education');
         $meeting_for = ArrayHelper::getValue($attributes, 'meeting_for');
         $interested_in = ArrayHelper::getValue($attributes, 'interested_in');
         $inspirational_people = ArrayHelper::getValue($attributes, 'inspirational_people');
         $political = ArrayHelper::getValue($attributes, 'political');
         $sports = ArrayHelper::getValue($attributes, 'sports');
         $website = ArrayHelper::getValue($attributes, 'website');
         $about = ArrayHelper::getValue($attributes, 'about');
         $address = ArrayHelper::getValue($attributes, 'address');
         $nickname = ArrayHelper::getValue($attributes, 'login');
        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();
        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var User $user */
                $user = $auth->user;
                $this->updateUserInfo($user);
                Yii::$app->user->login($user, 30 * 24 * 60);


            } else { // signup
                if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                    ]);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    $user = new User([
                        'username' => $email,
                        'email' => $email,
                        'password' => $password,
                        'status' => User::STATUS_ACTIVE,
                        //'gender'  => $gender,
                        //'birthday'=> $birthday,
                        //'location'=> $location,
                        //'education'=> $education,
                        //'meeting_for'=> $meeting_for,
                       // 'interested_in'=> $interested_in,
                       // 'inspirational_people'=> $inspirational_people,
                       // 'political'=> $political,
                       // 'sports'=> $sports,
                       // 'website'=> $website,
                      //  'about'=> $about,
                      //  'address'=> $address,

                    ]);
                    // $user->generateAuthKey();
                    // $user->generatePasswordResetToken();
                    $transaction = User::getDb()->beginTransaction();
                    //$create= date("Y-m-d H:i:s");
                    //$update= date("Y-m-d H:i:s");
                    //$model->created_at =  $create;
                    //$model->updated_at =  $update;
                    // $user->save();
                    $signup = new SignupForm();
                    $signup->username = $email;
                    $signup->email    = $email;
                    $signup->password = $password;
                    $success = $signup->signup();

                    if ($success) {
                        $auth = new Auth([
                            'user_id' => $success->id,
                            'source' => $this->client->getId(),
                            'source_id' => (string)$id,
                        ]);
                      
                        if ($auth->save()) {
                            $transaction->commit();

                            Yii::$app->user->login($user, 3600);
           
               
                        } else {
                            Yii::$app->getSession()->setFlash('error', [
                                Yii::t('app', 'Unable to save {client} account: {errors}', [
                                    'client' => $this->client->getTitle(),
                                    'errors' => json_encode($auth->getErrors()),
                                ]),
                            ]);
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Unable to save user: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($user->getErrors()),
                            ]),
                        ]);
                    }
                }
            }
    }
}
    /**
     * @param User $user
     */
    private function updateUserInfo(User $user)
    {
        $attributes = $this->client->getUserAttributes();
        $username = ArrayHelper::getValue($attributes, 'login');
        if ($user->username === null && $username) {
            $user->username = $username;
            $user->save();
        }
    }
}