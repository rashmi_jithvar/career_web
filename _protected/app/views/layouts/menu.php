<?php
use yii\helpers\Url;
$session = Yii::$app->session;

?>
    <div id="leftsidebar" class="sidebar">
        <div class="sidebar-scroll">
            <nav id="leftsidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="heading">Main</li>
                    <li class="active"><a href="<?= Url::to(['/']);?>"><i class="icon-home"></i><span>Dashboard</span></a></li>
                    <?php
                    if(Yii::$app->user->identity->role_id == 1)
                    {
                    ?>
                    <li class="heading">Masters</li>
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Masters</span></a>
                        <ul>
                        <li><a href="<?= Url::to(['site/under-construction']); ?>">Add Master</a></li>
                            <!-- <li><a href="<?= Url::to(['insurance-plan/']); ?>">Insurance Plan</a></li> -->
                            <!-- <li><a href="<?= Url::to(['billing-code/']); ?>">Billing Code</a></li> -->
                            <!-- <li><a href="<?= Url::to(['category/']); ?>">Category</a></li> -->
                            <!-- <li><a href="<?= Url::to(['product/index']); ?>">Products</a> -->
                            </li>
                            <li><a href="<?= Url::to(['master/name_type']); ?>">Name Type</a></li>
                            <li><a href="<?= Url::to(['country/index']); ?>">Country</a></li>
                            <li><a href="<?= Url::to(['state/index']); ?>">State</a></li>
                            <li><a href="<?= Url::to(['city/index']); ?>">City</a></li>
                            <li><a href="<?= Url::to(['area/index']); ?>">Area</a></li>                            
                    <?php
                    }
                    ?>
                        </ul>
                    </li>


                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-users"></i><span>User Management</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['roles/']); ?>">Roles</a></li>
                            <li><a href="<?= Url::to(['permission-group/']); ?>">Permission Group</a></li>
                            <li><a href="<?= Url::to(['permission/']); ?>">Permission</a></li>
                            <li><a href="<?= Url::to(['role-permission/create']); ?>">Role Permission</a></li>
                            <li><a href="<?= Url::to(['user/']); ?>">User</a></li>
                            <li><a href="<?= Url::to(['user/create']); ?>">Add User</a></li>
                        </ul>
                    </li>

                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Plan</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['/plan']); ?>">View Plan Created</a></li>
                        </ul>
                    </li>                    
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-users"></i><span>Members</span></a>

                        <ul>
                            <li><a href="<?= Url::to(['member/create']); ?>">Add Member</a></li>
                            <li><a href="<?= Url::to(['/member']); ?>">Member List</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Update Member</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Change Password</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">View Member Profile</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">View Member Team</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Generate Member Pin</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Member Pin Report</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Print Welcome Letter</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Send Notification Message</a></li>
                        </ul>
                    </li>
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Pin Management</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Generate Member Pin</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Used Pin Report</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Cancel Pin</a></li>
                        </ul>
                    </li>
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Payout</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Generate Payout</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">View Generated Payout</a></li>
                        </ul>
                    </li>                    
                     <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Report</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Membership Report</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Due Payment Report</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Paid Payment Report</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Total Business Report</a></li>
                        </ul>
                    </li>
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>  Announcements</span></a>
                        <ul>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">Post Announcement</a></li>
                            <li><a href="<?= Url::to(['site/under-construction']); ?>">View and Manage Announcements</a></li>
                        </ul>
                    </li>                             
                    <!-- <li class="heading">UI Elements</li>
                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-diamond"></i><span>Component</span></a>
                        <ul>
                            <li><a href="ui-card.html">Card Layout</a></li>
                            <li><a href="ui-typography.html">Typography</a></li>
                            <li><a href="ui-tabs.html">Tabs</a></li>
                            <li><a href="ui-buttons.html">Buttons</a></li>
                        </ul>
                    </li> -->
                </ul>
            </nav>
        </div>
    </div>
