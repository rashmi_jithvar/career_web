<?php
use app\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\web\HttpException;

use common\models\Permission;
use common\models\RolePermission;
if(Yii::$app->user->isGuest)
{
    Yii::$app->user->logout();
    return Yii::$app->response->redirect(['site/login']);
}
$role = Yii::$app->user->identity->role_id;
$per_array = null;
$permission = RolePermission::find()
                            ->where(['role_id' => $role])
                            ->all();
$per_array[] = '/';
$per_array[] = 'site/index';
foreach ($permission as $pKey => $perm) {
    $getPerm = Permission::findOne($perm->permission_id);
    $per_array[] = $getPerm->controller_name."/".$getPerm->action_name;
    // $per_array['action'] = $getPerm->action_name;
}
$session = Yii::$app->session;
$session['url'] = $per_array;

$contoller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

if(!in_array($contoller."/".$action, $session['url']))
{
    // throw new HttpException(401, "You are not authorized to access this page");
}


// var_dump($per_array);
// exit();
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        .has-error{
            color: #da0808 !important;
        }

        .theme-blue .page-loader-wrapper {
            background: rgb(62,172,255, 0.4) !important;
        }
    </style>
</head>
<body class="">
    <?php $this->beginBody() ?>

<!-- Page Loader -->

<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">
    <?php
        include 'top-menu.php';
        include 'menu.php';
    ?>

    <div id="main-content">
        <div class="container-fluid">


            <div class="block-header" style="margin-bottom: 0 !important; margin-top: 0 !important;">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <div class="page-title text-capitalize m-b-0"><?= $this->title; ?></div>
                    </div>
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <?php
                            echo Breadcrumbs::widget([
                                'homeLink' => [ 
                                            'label' => Yii::t('yii', 'Dashboard'),
                                            'url' => Yii::$app->homeUrl,
                                            'template' => "<li><i class='icon-home'></i>&nbsp;{link}&nbsp; / &nbsp; </li>\n",
                                         ],
                                'itemTemplate' => "<li>{link}&nbsp; / &nbsp; </li>\n", // template for all links
                                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                'options' => ['class' => 'breadcrumb justify-content-end']
                            ]);
                        ?>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= $content; ?>            
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php
$this->registerJsFile(Yii::getAlias('@web').'/themes/baby/light/assets/bundles/libscripts.bundle.js');
?>

    <?php $this->endBody() ?>
</body>
</html>
<?php
$registerJs = '
setInterval(function()
{
    $(document).ready(function(){
        $.post("'. Url::to(['automate/shipstation']) .'");
    });
}, 100000);
';
// $this->registerJs($registerJs);
?>

<?php $this->endPage() ?>

<?php $this->registerJsFile('js/company-index.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 
