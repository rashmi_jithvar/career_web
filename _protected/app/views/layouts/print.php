<?php
use app\assets\AppAsset;
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        .has-error{
            color: #da0808 !important;
        }
    </style>
</head>
<body class="theme-blue">
    <?php $this->beginBody() ?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?= $content; ?>            
                </div>
            </div>
    
<?php
$this->registerJsFile(Yii::getAlias('@web').'/themes/baby/light/assets/bundles/libscripts.bundle.js');
?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php $this->registerJsFile('js/company-index.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?> 
