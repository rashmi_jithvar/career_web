<?php
use yii\helpers\Url;
?>
            <div class="sidebar-container">
                <div class="sidemenu-container navbar-collapse collapse fixed-menu">
                    <div id="remove-scroll">
                        <ul class="sidemenu  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <li class="sidebar-user-panel">
                                <div class="user-panel">
                                    <div class="pull-left image">
                                        <img src="<?= Yii::getAlias('@web'); ?>/themes/default/assets/img/dp.jpg" class="img-circle user-img-circle" alt="User Image" />
                                    </div>
                                    <div class="pull-left info">
                                        <p> Kiran Patel</p>
                                        <a href="#"><i class="fa fa-circle user-online"></i><span class="txtOnline"> Online</span></a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item start active">
                                <a href="<?= Url::to(['site/index']); ?>" class="nav-link nav-toggle">
                                    <i class="material-icons">dashboard</i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-wrench"></i>
                                    <span class="title">Masters</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="<?= Url::to(['brand/']); ?>" class="nav-link ">
                                            <span class="title">Brand</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?= Url::to(['product-type/']); ?>" class="nav-link ">
                                            <span class="title">Product Type</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="<?= Url::to(['item-category/']); ?>" class="nav-link nav-toggle"> <i class="material-icons">menu</i>
                                    <span class="title">Manage Item Category</span> 
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= Url::to(['item/']); ?>" class="nav-link nav-toggle"> <i class="material-icons">menu</i>
                                    <span class="title">Manage Item</span> 
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
