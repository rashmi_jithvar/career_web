<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Organisation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php
    $form = ActiveForm::begin([
                'id' => $model->formName(),
               // 'enableAjaxValidation' => true,
                //'validationUrl' => ($model->id > 0) ? Url::to(['user/validation-change-password', 'id' => $model->id]) : Url::to(['user/validation-change-password']),
    ]);
    ?>

    <div class="row">
        <div class="col-12 offset-3 col-sm-6 col-lg-6 col-xl-6 col-md-6">
            <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12">
                <?= $form->field($model, 'old_password')->passwordInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'Enter Your Profile Email Here...']) ?>
            </div>
            <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12">
                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'Enter Your Profile First Name Here...']) ?>
            </div>
            <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12">
                <?= $form->field($model, 'confirm_password')->passwordInput(['maxlength' => true, 'autofocus' => true, 'placeholder' => 'Enter Your Profile Last Name Here...']) ?>

            </div>

            <div class="col-12 col-sm-12 col-lg-12 col-xl-12 col-md-12" style="">
                <div class="form-group pull-right">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success SubmitBtn']); ?>
                </div>
            </div>

        </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    var modal_formName = '<?= $model->formName(); ?>';
    var modal_Update = '<?= ($model->id > 0) ? "true" : "false"; ?>';
<?php Yii::$app->view->beginBlock('select2'); ?>
    $(function () {
        $(".select2").select2();
    });
<?php Yii::$app->view->endBlock(); ?>
</script>
<?php
Yii::$app->view->registerJsFile(Url::home() . "js/change-password.js", ['depends' => \yii\web\JqueryAsset::class]);
Yii::$app->view->registerJs(Yii::$app->view->blocks['select2'], yii\web\View::POS_END);
