<?php
use common\rbac\models\AuthItem;
use common\models\Roles;
// use nenad\passwordStrength\PasswordInput;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $role common\rbac\models\Role; */
?>
<div class="user-form">

    <?php $form = ActiveForm::begin(['id' => 'form-user', 'options' => ['autocomplete'=>"off"]]); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($user, 'first_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($user, 'last_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($user, 'username')->textInput(['maxlength' => true]) ?>
            </div>            
            <!--<div class="col-md-4">
                <?= $form->field($user, 'employee_id')->textInput(['maxlength' => true]) ?>
            </div>-->
        </div>

        <div class="row">

            <div class="col-md-4">
                <?= $form->field($user, 'email') ?>
            </div>
            <div class="col-md-4">
                <?php 
                    $getRoles = Roles::find()->where(['status' => Roles::STATUS_ACTIVE])->all();
                    $roles = ArrayHelper::map($getRoles, 'id', 'name');
                    $role->item_name = $user->role_id;
                /*foreach (AuthItem::getRoles() as $item_name): ?>
                    <?php $roles[$item_name->name] = $item_name->description ?>
                <?php endforeach*/ ?>
                <?= $form->field($role, 'item_name')->dropDownList($roles, ['prompt' => 'Please Select', 'class' => 'select-search']) ?>
            </div>
            <div class="col-lg-4">
                <?= $form->field($user, 'status')->dropDownList($user->statusList) ?>
            </div>            
        </div>
        

        <div class="row">
            <div class="col-md-4">
                <?php if ($user->scenario === 'create'): ?>
                    <?= $form->field($user, 'password')->passwordInput(); ?>
                <?php else: ?>
                    <?= $form->field($user, 'password')->passwordInput(['placeholder' => Yii::t('app', 'New Password ( if you want to change it )')]) 
                    ?>       
                <?php endif ?>
            </div>
            <div class="col-md-4">
                <?php if ($user->scenario === 'create'): ?>
                    <?= $form->field($user, 'c_password')->passwordInput(); ?>
                <?php else: ?>
                    <?= $form->field($user, 'password')->passwordInput(['placeholder' => Yii::t('app', 'Confirm Password ( if you want to change it )')]) 
                    ?>       
                <?php endif ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') 
                        : Yii::t('app', 'Update'), ['class' => $user->isNewRecord 
                        ? 'btn btn-success' : 'btn btn-primary']) ?>

                    <?= Html::a(Yii::t('app', 'Cancel'), ['user/index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
 
</div>
