<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

<div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <div class="card card-topline-aqua">
                                    <div class="card-body no-padding height-9">
                                        <div class="row">
                                            <div class="profile-userpic">
                                                <i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 600%;"></i>
 </div>
                                        </div>
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"><?= $model ? $model->username:''?></div>
                                            <div class="profile-usertitle-job"> <?= $model->roles ? $model->roles->name:''?> </div>

                                        </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Name</b> <a class="pull-right"><?= $model ? $model->first_name:''?>&nbsp;<?= $model ? $model->last_name:''?></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Username</b> <a class="pull-right"><?= $model ? $model->username:''?></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Email</b> <a class="pull-right"><?= $model ? $model->email:''?></a>
                                            </li>
                                        </ul>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        <div class="profile-userbuttons">
                                           
                                            <!--<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-pink" data-upgraded=",MaterialButton,MaterialRipple" href="<?= Url::to(['user/change-password']); ?>">Change Password<span class="mdl-button__ripple-container"><span class="mdl-ripple"></span></span></button>-->
                                        </div>
                                        <!-- END SIDEBAR BUTTONS -->
                                    </div>
                                </div>


                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->

                        </div>

</div>
