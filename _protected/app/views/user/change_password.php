<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-body col-md-12" id="bar-parent">
    	<?php if (Yii::$app->session->hasFlash('success')): ?>
		<?= Yii::$app->session->getFlash('success') ?>
		<?php endif; ?>
        <?=
        $this->render('_change_password', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
