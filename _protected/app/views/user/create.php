<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $role common\rbac\models\Role */

$this->title = Yii::t('app', 'Create User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-body col-md-12" id="bar-parent">
        <?= $this->render('_form', [
            'user' => $user,
            'role' => $role,
        ]) ?>
    </div>
</div>

