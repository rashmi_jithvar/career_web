<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t('app', 'Login');
?>
<div class="site-login">
    <div class="">

        <p><?php //echo Yii::t('app', 'Please fill out the following fields to login:') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?php //-- use email or username field depending on model scenario --// ?>
        <?php if ($model->scenario === 'lwe'): ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email Address'])->label(false); ?>        
        <?php else: ?>
            <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username'])->label(false); ?>
        <?php endif ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false); ?>
        <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'fancy-checkbox element-left', 'label' => 'Remember Me', 'labelOptions' => ['style' => 'display: -webkit-box; padding: 0;']]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Login'), ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
                                    <div class="bottom">

                                        <span class="helper-text m-b-10"><i class="fa fa-lock"></i><a href="<?= Url::to(['/site/request-password-reset']); ?>">Forgot password?</a></span>
                                        <span>Don't have an account? <a href="<?= Url::to(['/site']); ?>">Register</a></span>
                                    </div>

    </div>
  
</div>
