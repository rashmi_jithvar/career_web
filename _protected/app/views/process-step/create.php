<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProcessStep */

$this->title = Yii::t('app', 'Create Process Step');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Process Steps'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-head">
    	<header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
