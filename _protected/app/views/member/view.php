<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Member */

$this->title = "Profile";
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card card-box">
    <div class="card-head">

    <!-- <h1><?//= Html::encode($this->title) ?></h1> -->
<!-- 
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p> -->
</div>
<div class="card-body">
    <!---<?/*= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'id',
            'sponser_id',
            'upline_id',
            'left_right_leg',
            'applicant_name',
            'gender',
            'name_type',
            'father_spouse_name',
            'dob:date',
            'address:ntext',
            'city',
            'state',
            'mobile',
            'email:email',
            'pan_no',
            'aadhaar_no',
            'bank_name',
            'account_no',
            'IFSC_code',
            'account_holder_name',
            'date:date',
            'nomine_name',
            'qualification',
            'password',
            'created_by',
            'created_on:date',
            //'updated_by',
            //'updated_on',
        ],
    ])*/ ?>--->
<div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="user-view">

<div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PROFILE SIDEBAR -->
                            <div class="profile-sidebar">
                                <div class="card card-topline-aqua">
                                    <div class="card-body no-padding height-9">
                                        <div class="row">
                                            <div class="profile-userpic">
                                                    <?php  if($model->image ? $model->image->upload_photo : ''){?>
                                                    <img src="<?= \Yii::getAlias('@web') . "/uploads/".$model->image->upload_photo;?>"> 
                                                    <?php }else {?>
                                                    <i class="fa fa-user-circle-o" aria-hidden="true" style="font-size: 600%;"></i>
                                                    <?php } ?>
                                            </div>
                                        </div>
                                        <div class="profile-usertitle">
                                            <div class="profile-usertitle-name"><?= $model ? $model->applicant_name:''?></div>
                                            <div class="profile-usertitle-job"> <?= $model ? $model->sponser_id:''?></div>
                                        </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Left Sponser</b> <a class="pull-right"><?= $left ? $left->applicant_name.'/'.$left->unique_id:''?></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Right Sponser</b> <a class="pull-right"><?= $right ? $right->applicant_name.'/'.$right->unique_id:''?></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Left Business</b> <a class="pull-right"></a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Right Business</b> <a class="pull-right"></a>
                                            </li>                                            
                                        </ul>
                                        <!-- END SIDEBAR USER TITLE -->
                                        <!-- SIDEBAR BUTTONS -->
                                        <div class="profile-userbuttons">
                                            <p>
                                                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                                    'class' => 'btn btn-danger',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this item?',
                                                        'method' => 'post',
                                                    ],
                                                ]) ?>
                                            </p>
                                        </div>
                                        <!-- END SIDEBAR BUTTONS -->
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-head card-topline-aqua">
                                        <header>About Me</header>
                                    </div>
                                   
                                        
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Gender </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->gender:''?></div>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Date Of Birth</b>
                                                <div class="profile-desc-item pull-right"><?= date("d-m-Y ",strtotime($model ? $model->dob:'' ))
                                                                        ?></div>
                                            </li>
                                             <li class="list-group-item">
                                                <b>Qualification </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->qualification:''?></div>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Aadhaar No.</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->aadhaar_no:''?></div>
                                            </li>
                                            <li class="list-group-item">
                                                <b>PAN No.</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->pan_no:''?></div>
                                            </li>                                            
                                        </ul>
                                    
                                </div>
                                <div class="card">
                                    <div class="card-head card-topline-aqua">
                                        <header>Address</header>
                                    </div>
                                    
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Contact No. </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->mobile:''?></div>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Email</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->email:''?></div>
                                            </li>
                                             <li class="list-group-item">
                                                <b>Address </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->address:''?>&nbsp;&nbsp;,<?= $model->statename ? $model->statename->name:''?>&nbsp;&nbsp;,<?= $model->cityname ? $model->cityname->name:''?></div>
                                            </li>                                           
                                        </ul>
                                    
                                </div>
                                <div class="card">
                                    <div class="card-head card-topline-aqua">
                                        <header>Bank Details</header>
                                    </div>
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Bank Name</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->bank_name:''?></div>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Account No.</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->account_no:''?></div>
                                            </li>
                                             <li class="list-group-item">
                                                <b>IFSC Code </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->IFSC_code:''?></div>
                                            </li> 
                                             <li class="list-group-item">
                                                <b>Account Holder Name</b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->account_holder_name:''?></div>
                                            </li>
                                             <li class="list-group-item">
                                                <b>Nominee Name </b>
                                                <div class="profile-desc-item pull-right"><?= $model ? $model->nomine_name:''?></div>
                                            </li>
                                             <li class="list-group-item">
                                                <b>Date</b>
                                                <div class="profile-desc-item pull-right"><?= date("d-m-Y ",strtotime($model ? $model->date:'' ))
                                                                         ?></div>
                                            </li>                                                                                                                                                                              
                                        </ul>
                                </div>
                            </div>
                            <!-- END BEGIN PROFILE SIDEBAR -->
                            <!-- BEGIN PROFILE CONTENT -->
                            <div class="profile-content">
                                <div class="row">
                                     <div class="card">
                                         <div class="card-topline-aqua">
                                             <header></header>
                                         </div>
                                            <div class="white-box">
                                                <!-- Nav tabs -->
                                                <div class="p-rl-20">
                                                    <ul class="nav customtab nav-tabs" role="tablist">
                                                        <li class="nav-item active"><a href="#tab1" class="nav-link active show" data-toggle="tab" aria-expanded="true">Left Business</a></li>
                                                        <li class="nav-item"><a href="#tab2" class="nav-link" data-toggle="tab" aria-expanded="false">Right Business</a></li>
                                                    </ul>
                                                </div>
                                                <!-- Tab panes -->
                                                <div class="tab-content">

                                                    </div>
                                                    <div class="tab-pane" id="tab2">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="full-width p-rl-20">
                                                                    <div class="panel">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="full-width p-rl-20">
                                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         </div>
                                     </div>
                                </div>
                                <!-- END PROFILE CONTENT -->
                            </div>
                        </div>

</div>
            
                </div>
            </div>
</div>
</div>