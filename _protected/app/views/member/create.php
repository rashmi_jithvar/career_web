<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Member */

$this->title = 'Create Member';
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-head">
    	<!-- <h3><?= Html::encode($this->title) ?></h3> -->
    </div>	
<div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
        'get_member_ddl' =>$get_member_ddl,
        'get_plan_ddl' =>$get_plan_ddl,
        'image'=>$image,
        'get_state_ddl' =>$get_state_ddl,
        
    ]) ?>
</div>
</div>
