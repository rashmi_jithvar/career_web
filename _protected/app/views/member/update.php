<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Member */

$this->title = 'Update Member: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card card-box">
    <div class="card-head">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="card-body">
    <?= $this->render('_update-form', [
        'model' => $model,
        'get_member_ddl' =>$get_member_ddl,
        'get_plan_ddl' =>$get_plan_ddl,
        'image'=>$image,  
        'get_state_ddl'=>$get_state_ddl,
        'get_city_ddl'=>$get_city_ddl,
    ]) ?>

</div>
</div>
