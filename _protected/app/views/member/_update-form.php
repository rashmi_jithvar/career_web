<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use common\models\Master;
/* @var $this yii\web\View */
/* @var $model common\models\Member */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container">
<div class="member-form">


    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <div class="row">    

            <div class="col-md-6">
                  <h5> General Information</h5>
                <?= $form->field($model, 'sponser_id')->dropDownList($get_member_ddl, ['class' => 'select-search', 'prompt' => 'Please Select']) ?>

                <?= $form->field($model, 'upline_id')->textInput(['maxlength' => true,'placeholder' => 'Upline', 'readonly' => true]) ?>
    
                <?= $form->field($model, 'left_right_leg')->dropDownList([ 'left' => 'Left', 'right' => 'Right'], ['prompt' => 'Please Select']) ?>
          
                <?= $form->field($model, 'applicant_name')->textInput(['maxlength' => true]) ?>
    
                    <?= $form->field($model, 'gender')->dropDownList([ 'male' => 'Male', 'female' => 'Female'], ['prompt' => 'Please Select']) ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'name_type')->dropDownList([ 'S/O' => 'S/O', 'B/O' => 'B/O','W/O' => 'W/O', 'F/O' => 'F/O','D/O' => 'D/O', 'FRM' => 'FRM','COY' => 'COY'], ['prompt' => 'Please Select']) ?>
                    </div>    
                    <div class="col-md-8">
                        <?= $form->field($model, 'father_spouse_name')->textInput(['maxlength' => true]) ?>
                    </div>
                </div> 
            <?= DatePicker::widget([
                        'name' => 'dob', 
                        'value' => date('d-m-Y'),
                        'options' => ['placeholder' => 'Select Date Of Birth ...'],
                        'pluginOptions' => [
                            'format' => 'dd-m-yyyy',
                            'todayHighlight' => true
                        ]
                    ]);?>                   
               
      
                <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'state')->dropDownList($get_state_ddl,
                    [
                        'prompt' => Yii::t('app','Select'),
                        'onchange'=>'
                        $.get( "'.Yii::$app->urlManager->createUrl('member/city-dropdown?id=').'"+$(this).val(), function( data ) {
                        $( "select.city" ).html( data );
                        })'
                    ]); ?>
                <?= $form->field($model, 'city', ['inputOptions'=>['class'=>'city form-control']])->dropDownList($get_city_ddl,['prompt' => 'Select']) ?>
                <?= $form->field($model, 'mobile')->textInput(['maxlength'=>10]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
  
                <?= $form->field($model, 'pan_no')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'aadhaar_no')->textInput(['maxlength' => true]) ?>
            </div>              


    <div class="col-md-6">
        <h5> Other Information</h5>
            <?= $form->field($model, 'plan_id')->dropDownList($get_plan_ddl, ['class' => 'select-search', 'prompt' => 'Please Select']) ?>

            <?= $form->field($model, 'plan_amount')->textInput(['maxlength' => true]) ?>
        <h5> Bank Information</h5>
            <?= $form->field($model, 'bank_name')->textInput(['maxlength' => true]) ?>
 
            <?= $form->field($model, 'account_no')->textInput(['maxlength' => true]) ?>
       
            <?= $form->field($model, 'IFSC_code')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'account_holder_name')->textInput(['maxlength' => true]) ?>
            <?= DatePicker::widget([
                        'name' => 'date', 
                        'value' => date('d-m-Y'),
                        'options' => ['placeholder' => 'Select Date ...'],
                        'pluginOptions' => [
                            'format' => 'dd-m-yyyy',
                            'todayHighlight' => true
                        ]
                    ]);?>
            

   
            <?= $form->field($model, 'nomine_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'qualification')->textInput() ?>

            <?= $form->field($image, 'upload_photo')->fileInput(['maxlength' => true]) ?>
                <?php  if($image->upload_photo !=''){?>
                    <img src="<?= \Yii::getAlias('@web') . "/uploads/".$image->upload_photo;?>" width="30%;"> 
                <?php }?>   

            <?= $form->field($image, 'id_proof_photo')->fileInput(['maxlength' => true]) ?>
                <?php  if($image->id_proof_photo !=''){?>
                    <img src="<?= \Yii::getAlias('@web') . "/uploads/".$image->id_proof_photo;?>" width="30%;"> 
                <?php }?>   

            <?= $form->field($image, 'pan_card_photo')->fileInput(['maxlength' => true]) ?>
                <?php  if($image->pan_card_photo != ''){?>
                    <img src="<?= \Yii::getAlias('@web') . "/uploads/".$image->pan_card_photo;?>" width="30%;"> 
                <?php }?> 

            <?= $form->field($image, 'passbook_photo')->fileInput(['maxlength' => true]) ?>
                <?php  if($image->passbook_photo != ''){?>
                    <img src="<?= \Yii::getAlias('@web') . "/uploads/".$image->passbook_photo;?>" width="30%;"> 
                <?php }?> 

                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    </div>

   
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>

    </div>
</div>

    <?php ActiveForm::end(); ?>

</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
        $(document).ready( function() {
            $sponser_id =  $('#member-sponser_id').val();
                $.ajax({
                                type : 'post',
                                url : "<?= Url::to(['member/city-dropdown']);?>",
                                //dataType:"json",
                                data:{ 'sponser_id':$sponser_id},
                                success:function(data){ 
                                    alert(data);
                                    $('.city').html(data);
                                }
                });             
            $( "#member-sponser_id" ).change(function() {
                $sponser_id = 0;
                if(('#member-sponser_id')!=''){
                    $sponser_id  = $('#member-sponser_id').val();
                }
                        $.ajax({
                                type : 'post',
                                url : "<?= Url::to(['member/get-upline']);?>",
                                //dataType:"json",
                                data:{ 'sponser_id':$sponser_id},
                                success:function(data){ 
                                    $('#member-upline_id').val(data);
                                }
                        }); 

                        $.ajax({
                                type : 'post',
                                url : "<?= Url::to(['member/get-leg']);?>",
                                //dataType:"json",
                                data:{ 'sponser_id':$sponser_id},
                                success:function(data){ 
                                    $('#member-left_right_leg').html(data);
                                }
                        });                         
                    })
            // $( "#member-state" ).change(function() {

            //     $state_id = 0;
            //     if(('#member-state')!=''){
            //         $state_id  = $('#member-state').val();
            //     }
            //             $.ajax({
            //                     type : 'post',
            //                     url : "<?= Url::to(['member/get-state']);?>",
            //                     //dataType:"json",
            //                     data:{ 'state_id':$state_id},
            //                     success:function(data){ 
            //                         $('#member-city').html(data);
            //                     }
            //             });                        
            //         })            
        });
</script>