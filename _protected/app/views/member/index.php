<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-head">

    <!---<h1><?//= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="card-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'unique_id',
            'upline_id',
            //'left_right_leg',
            'applicant_name',
            //'gender',
            //'name_type',
            //'father_spouse_name',
            //'dob',
            //'address:ntext',
            //'city',
            //'state',
            //'mobile',
            //'email:email',
            //'pan_no',
            //'aadhaar_no',
            //'bank_name',
            //'account_no',
            //'IFSC_code',
            //'account_holder_name',
            //'date',
            //'nomine_name',
            //'qualification',
            //'password',
            //'created_by',
            //'created_on',
            //'updated_by',
            //'updated_on',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
