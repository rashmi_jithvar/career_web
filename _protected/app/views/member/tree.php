
<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Member;
use yii\helpers\Url;
$this->title = 'Tree';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-head">
</div>
	<table class="table" align="center" border="0" style="text-align:center">
			<tr height="150">
					<td colspan="4"><a href="<?= Url::to(['member/view','id'=>$model->id]);?>" ><i class="fa fa-user fa-4x" style="color:#1430B1"></i><p><?= $model ? $model->applicant_name:'' ?></p></a></td>
					
			</tr>
			<tr height="150">
			<?php

				$left = New Member();
				$firstLeft = $left->getFindLeft($model->id);
				$firstRight = $left->getFindRight($model->id);

				//$secondLeft = $sLeft->getFindLeft($firstLeft->id);
				//$getfirstteamleft = Member::find()->where(['sponser_id'=>$model->id])->andWhere(['left_right_leg' => 'left'])->one();
				//$getfirstteamright = Member::find()->where(['sponser_id'=>$model->id])->andWhere(['left_right_leg' => 'right'])->one();
				//$getteam =$team->getFindteam($model->id);
				 //Member::getFindteam();
			?>
			<?php 
			if(!empty($firstLeft)){
			?>
			<td colspan="2"><a href="<?= Url::to(['member/view','id'=>$firstLeft->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $firstLeft->applicant_name;  ?></p></a></td>
			<?php 
			}
			else{
			?>
			<td colspan="2"><i class="fa fa-user fa-4x" style="color:#361515"></i><p><?php  ?></p></td>
			<?php
			}
			?>
			<?php 
			if(!empty($firstRight)){
			?>
			<td colspan="2"><a href="<?= Url::to(['member/view','id'=>$firstRight->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $firstRight->applicant_name;  ?></p></a></td>
			<?php 
			}
			else{
			?>
			<td colspan="2"><i class="fa fa-user fa-4x" style="color:#361515"></i><p><?php  ?></p></td>
			<?php
			}
		
			?>
			</tr>
			<tr height="150">
			<?php
				if($firstLeft->id !=false){
					$second = New Member();
					$secondLeft = $second->getFindLeft($firstLeft->id);
					$secondRight = $second->getFindRight($firstLeft->id);				
				}		
	

			if($firstRight->id !=false){
				$third = New Member();
					$thirdLeft = $third->getFindLeft($firstRight->id);
					$thirdRight = $third->getFindRight($firstRight->id);					
				}
							
				// $thirdteamleft = New Member();
				// $third_left = $getfirstteamleft['id'];
				// $thirdleftteam = Member::find()->where(['sponser_id'=>$third_left])->andWhere(['left_right_leg'=>'left'])->one();

				// $thirdrightteam = Member::find()->where(['sponser_id'=>$third_left])->andWhere(['left_right_leg'=>'right'])->one();

				// $getteamrightthird = Member::find()->where(['sponser_id'=>$getfirstteamright['id']])->andWhere(['left_right_leg'=>'right'])->one();
				// $getteamleftthird = Member::find()->where(['sponser_id'=>$getfirstteamright['id']])->andWhere(['left_right_leg'=>'left'])->one();

			?>
			<?php 
	
			if(isset($secondLeft) && $secondLeft !=false ){
			?>
			<td><a href="<?= Url::to(['member/view','id'=>$secondLeft->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $secondLeft->applicant_name;  ?></p></a></td>
			<?php 
			}
	
			else{
			?>
			<td><i class="fa fa-user fa-4x" style="color:#361515"></i></td>
			<?php
			}
			?>
			<?php 
						
			if(isset($secondRight) && $secondRight!=false){
			?>
			<td><a href="<?= Url::to(['member/view','id'=>$secondRight->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $secondRight->applicant_name;  ?></p></a></td>
			<?php 
			}
			
			else{
			?>
			<td><i class="fa fa-user fa-4x" style="color:#361515"></i></td>
			<?php
			}
			?>
			<?php 


			if($thirdLeft !=false && isset($thirdLeft)){
			?>
			<td><a href="<?= Url::to(['member/view','id'=>$thirdLeft->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $thirdLeft->applicant_name;  ?></p></a></td>
			<?php 
			}
		
			else{
			?>
			<td><i class="fa fa-user fa-4x" style="color:#361515"></i></td>
			<?php
			}
			?>
			<?php 
		
			if($thirdRight!=false && isset($thirdRight)){
			?>
			<td><a href="<?= Url::to(['member/view','id'=>$thirdRight->id]);?>"><i class="fa fa-user fa-4x" style="color:#D520BE"></i><p><?= $thirdRight->applicant_name;  ?></p></a></td>
			<?php 
			}
		
			else{
			?>
			<td><i class="fa fa-user fa-4x" style="color:#361515"></i></td>
			<?php
			}
			?>
			</tr>			
	</table>
</div>
