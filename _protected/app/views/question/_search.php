<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SearchQuestion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'q_id') ?>

    <?= $form->field($model, 'question') ?>

    <?= $form->field($model, 'opt_a') ?>

    <?= $form->field($model, 'opt_b') ?>

    <?= $form->field($model, 'opt_c') ?>

    <?php // echo $form->field($model, 'opt_d') ?>

    <?php // echo $form->field($model, 'opt_e') ?>

    <?php // echo $form->field($model, 'answer') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_id') ?>

    <?php // echo $form->field($model, 'update_on') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'update_ip') ?>

    <?php // echo $form->field($model, 'view') ?>

    <?php // echo $form->field($model, 'user_like') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
