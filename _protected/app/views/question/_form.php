<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Question */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="question-form">
<div class="container">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    
      <?= $form->field($model, 'question')->textarea(['rows' => 6]) ?>
   <div class="row">
    <div class="col-md-3">
      <?= $form->field($model, 'opt_a')->textInput(['maxlength' => true]) ?>
    </div>  
    <div class="col-md-3">
     <?= $form->field($model, 'opt_b')->textInput(['maxlength' => true]) ?>
    </div>
     <div class="col-md-3"> 
       <?= $form->field($model, 'opt_c')->textInput(['maxlength' => true]) ?>
   </div>
    <div class="col-md-3"> 
      <?= $form->field($model, 'opt_d')->textInput(['maxlength' => true]) ?>
  </div>
</div>
<div class="row">
    <div class="col-md-3">
     <?= $form->field($model, 'opt_e')->textInput(['maxlength' => true]) ?>
   </div>
   <div class="col-md-3">
    <?= $form->field($model, 'answer')->textInput(['maxlength' => true]) ?>
  </div>
 <div class="col-md-3">
   <?= $form->field($model1, 'image')->fileInput() ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'is_active')->dropDownList([$model::STATUS_ACTIVE => 'Active', $model::STATUS_PENDING => 'Pending', $model::STATUS_INACTIVE => 'Inactive', $model::STATUS_DELETE => "Delete"], ['prompt' => 'Select An Option', 'class' => 'select-search']) ?>
</div>
</div>
<div class="row">

</div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
