<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SearchQuestion */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Questions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-header">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Question', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
  </div >
    <div class="card-body " id="bar-parent">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'q_id',
            'question:ntext',
            'opt_a',
            'opt_b',
            'opt_c',
            //'opt_d',
            //'opt_e',
            //'answer',
            //'created_by',
            //'created_on',
            //'created_id',
            //'update_on',
            //'update_by',
            //'update_ip',
            //'view',
            //'user_like',
            //'slug',
            //'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
