<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PermissionGroup */

$this->title = Yii::t('app', 'Create Permission Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Permission Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box col-md-6">
    <div class="card-head">
    	<header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
