<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Openings */

$this->title = 'Create Openings';
$this->params['breadcrumbs'][] = ['label' => 'Openings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-head">

    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="card-body">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>