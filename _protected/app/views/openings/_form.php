<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Template;
use common\models\Page;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model common\models\Openings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="openings-form">
<div class="container">
    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-3"> 
    <?= $form->field($model, 'job_code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3"> 
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-md-3">
     <?= $form->field($model, 'min_experience')->textInput() ?> 
    </div>
    <div class="col-md-3"> 
    <?= $form->field($model, 'designation')->textInput() ?>
   </div>
</div>
<div class="row">
   <div class="col-md-3"> 
    <?= $form->field($model, 'min_experience')->textInput() ?>
  </div>
  <div class="col-md-3"> 
    <?= $form->field($model, 'max_experience')->textInput() ?>
 </div>
 <div class="col-md-3"> 
    <?= $form->field($model, 'min_salary')->textInput(['maxlength' => true]) ?>
 </div>
 <div class="col-md-3"> 
     <?= $form->field($model, 'max_salary')->textInput(['maxlength' => true]) ?>
 </div>
</div>
<div class="row">
    <div class="col-md-3"> 
    <?= $form->field($model, 'salary_frequence')->textInput() ?>
    </div>
    <div class="col-md-3"> 
    <?= $form->field($model, 'location')->textInput() ?>
    </div>
    <div class="col-md-3"> 
    <?= $form->field($model, 'walkin')->textInput() ?>
    </div>
    <div class="col-md-3"> 
    <?= $form->field($model, 'walkin_from')->textInput()?>
    </div>
</div>
<div class="row">
    <div class="col-md-3"> 
    <?= $form->field($model, 'walkin_to')->textInput() ?>
   </div>
   <div class="col-md-3"> 
    <?= $form->field($model, 'openings')->textInput() ?>
   </div>
   <div class="col-md-3">
   <?= DatePicker::widget([
    'model' => $model,
    'attribute' => 'opening_date',
    'template' => '{addon}{input}',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'dd-M-yyyy'
        ]
    ]);?>
    <?= $form->field($model, 'opening_date')->textInput() ?>
</div>
</div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'class' => 'form-control summernote']) ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
