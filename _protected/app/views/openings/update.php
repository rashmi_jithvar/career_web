<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Openings */

$this->title = 'Update Openings: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Openings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="openings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
