<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OpeningsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Openings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-header">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Openings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="card-body " id="bar-parent">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'job_code',
            'title',
            'description:ntext',
            'designation',
            //'min_experience',
            //'max_experience',
            //'min_salary',
            //'max_salary',
            //'salary_frequence',
            //'location',
            //'walkin',
            //'walkin_from',
            //'walkin_to',
            //'openings',
            //'opening_date',
            //'created_by',
            //'created_on',
            //'created_ip',
            //'updated_on',
            //'updated_by',
            //'updated_ip',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
