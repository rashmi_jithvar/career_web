<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Openings */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Openings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="openings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'job_code',
            'title',
            'description:ntext',
            'designation',
            'min_experience',
            'max_experience',
            'min_salary',
            'max_salary',
            'salary_frequence',
            'location',
            'walkin',
            'walkin_from',
            'walkin_to',
            'openings',
            'opening_date',
            'created_by',
            'created_on',
            'created_ip',
            'updated_on',
            'updated_by',
            'updated_ip',
        ],
    ]) ?>

</div>
