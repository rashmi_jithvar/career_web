<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\OpeningsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="openings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'job_code') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'designation') ?>

    <?php // echo $form->field($model, 'min_experience') ?>

    <?php // echo $form->field($model, 'max_experience') ?>

    <?php // echo $form->field($model, 'min_salary') ?>

    <?php // echo $form->field($model, 'max_salary') ?>

    <?php // echo $form->field($model, 'salary_frequence') ?>

    <?php // echo $form->field($model, 'location') ?>

    <?php // echo $form->field($model, 'walkin') ?>

    <?php // echo $form->field($model, 'walkin_from') ?>

    <?php // echo $form->field($model, 'walkin_to') ?>

    <?php // echo $form->field($model, 'openings') ?>

    <?php // echo $form->field($model, 'opening_date') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'created_ip') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_ip') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
