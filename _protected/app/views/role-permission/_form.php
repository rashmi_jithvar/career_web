<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RolePermission */
/* @var $form yii\widgets\ActiveForm */
$model->role_id = Yii::$app->request->get("role");
?>

<div class="role-permission-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'role_id')->dropDownList($map_roles, ['class' => 'select-search', 'prompt' => 'Please Select', 'onchange' => "loadRole(this.value)"]) ?>
        </div>
    </div>

    <?php
    $perm_array = null;
    if($permission)
    {
        foreach ($permission as $perKey => $perm_val) {
            $perm_array[] = $perm_val->permission_id;
        }
    }

    foreach ($permGroup as $pKey => $group) {
        if($group->permission)
        {
        ?>
        <div class="row">
            <div class="col-md-12 m-b-20">
                <h5><u><?= $group->name; ?></u></h5>
                <div class="row">
                <?php
                    foreach($group->permission as $pKey => $permission) 
                    {
                        $data_checked = '';
                        if($perm_array)
                        {
                            if(in_array($permission->id, $perm_array))
                            {
                                $data_checked = "checked";
                            }
                        }
                    ?>
                        <div class="col-md-2">
                            <?= $form->field($model, 'permission_id[]')->checkbox(['label' => $permission->name, 'value' => $permission->id, 'data-checked' => $data_checked])->label(false) ?>
                            <?php $permission->name; ?>
                        </div>
                        <?php
                    }
                ?>
                </div>
            </div>
        </div>
        <hr />
        <?php
        }
    }
    ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    function loadRole(value)
    {
        window.location = "<?= Url::to(['role-permission/create', "role" => '']); ?>"+value;
    }
</script>

<?php
$loadJs = '
    $(document).ready(function(){
        $checked = $(this).find("input").each(function(){
            $is_checked = $(this).attr("data-checked");
            if($is_checked == "checked")
            {
            // console.log($is_checked);
                $(this).prop("checked", true);
            }
        });
    });
';

$this->registerJs($loadJs);
?>