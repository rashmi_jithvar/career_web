<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RolePermission */

$this->title = Yii::t('app', 'Create Role Permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Role Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-head">
    	<header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	        'map_roles' => $map_roles,
            'permGroup' => $permGroup,
            'permission' => $permission,
	    ]) ?>
	</div>
</div>
