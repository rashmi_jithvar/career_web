<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\PermissionGroup;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PermissionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;
?>

<style type="text/css">


</style>

<div class="card card-box">
    <div class="card-body " id="bar-parent">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Permission'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'pager' => [
        //     'options'=>['container' => 'div', 'class'=>'pagination dataTables_paginate paging_simple_numbers'],   // set clas name used in ui list of pagination
        //     // 'prevPageLabel' => 'Previous',   // Set the label for the “previous” page button
        //     // 'nextPageLabel' => 'Next',   // Set the label for the “next” page button
        //     'firstPageLabel'=>'First',   // Set the label for the “first” page button
        //     'lastPageLabel'=>'Last',    // Set the label for the “last” page button
        //     'nextPageCssClass'=>'paginate_button page-item',    // Set CSS class for the “next” page button
        //     'prevPageCssClass'=>'paginate_button page-item',    // Set CSS class for the “previous” page button
        //     'firstPageCssClass'=>'paginate_button page-item',    // Set CSS class for the “first” page button
        //     'lastPageCssClass'=>'paginate_button page-item',    // Set CSS class for the “last” page button
        //     'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed 
        //     ],
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'group_id', 'value' => function($data){
                return $data->group ? $data->group->name : '-';
            },
            'filter' => ArrayHelper::map(PermissionGroup::find()->all(), 'id', 'name'),
            'filterInputOptions' => ['class' => 'form-control', 'prompt' => 'All']
            ],
            'name',
            'controller_name',
            'action_name',
            // 'created_by',
            //'created_on',
            //'updated_by',
            //'updated_on',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
