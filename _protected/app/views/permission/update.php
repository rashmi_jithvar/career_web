<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Permission */

$this->title = Yii::t('app', 'Update Permission: {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-box col-md-6">
    <div class="card-head">
    	<header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
            'group' => $group,
	    ]) ?>
	</div>
</div>
