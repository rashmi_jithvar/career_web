<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Template;
use common\models\Page;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">
<div class="container">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-md-3">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($model, 'tel')->textInput(['maxlength' => true]) ?>
   </div>
   <div class="col-md-3">
    <?= $form->field($model, 'flag_icon')->fileInput() ?>
   </div>
</div>
<div class="row">
    
    <div class="col-md-3">
      <?= $form->field($model, 'language')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-3">
     <?= $form->field($model, 'status')->dropDownList([$model::STATUS_ACTIVE => 'Active', $model::STATUS_PENDING => 'Pending', $model::STATUS_INACTIVE => 'Inactive', $model::STATUS_DELETE => "Delete"], ['prompt' => 'Select An Option', 'class' => 'select-search']) ?>
    </div>   
   
</div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
<?php
$js = ' 
        $(document).ready(function() { 
            
        });
    ';

$this->registerJs($js, yii\web\View::POS_END);
