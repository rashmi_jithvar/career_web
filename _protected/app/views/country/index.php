<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Country');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-header">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Country', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
<div class="card-body " id="bar-parent">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'c_id',
            'name',
            'code',
            'tel',
            'flag_icon',
            //'flag_path',
            //'language',
            //'created_on',
            //'created_by',
            //'created_ip',
            //'update_on',
            //'update_by',
            //'update_ip',
            //'status',
            //'is_delete',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
