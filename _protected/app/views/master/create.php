<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Master */

$this->title = Yii::t('app', 'Create Master');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Masters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
