<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Templates');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-header">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

        <p>
            <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Add'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
        
    </div>
    <div class="card-body " id="bar-parent">

        <?php Pjax::begin(); ?>

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//                'id',
                'name',
//                'status',
                [
                    'attribute' => 'status',
                    'filter' => array($searchModel::STATUS_ACTIVE => "Active", $searchModel::STATUS_INACTIVE => "Inactive", $searchModel::STATUS_PENDING => "Pending", $searchModel::STATUS_DELETE => "Delete"),
                    'value' => function($model) {
                        return $model->getStatus();
                    },
                ],
//                'created_by',
                [
                    "attribute" => "createdUser",
                    'value' => "createdBy.username",
                ],
                'created_on',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
