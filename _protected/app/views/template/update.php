<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Template */

$this->title = Yii::t('app', 'Update Template: {name}', [
            'name' => $model->name,
        ]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="card card-box">
    <div class="card-body " id="bar-parent">

        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="card-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
