<?php

use yii\helpers\Html; 

/* @var $this yii\web\View */
/* @var $model common\models\Template */

$this->title = Yii::t('app', 'Create Template');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Templates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-head">

        <header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">


        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>

</div>