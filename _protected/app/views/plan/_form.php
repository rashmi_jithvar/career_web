<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Plan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plan-form">

    <?php $form = ActiveForm::begin(); ?>
<div class="container">
    <div class="row">
<div class="col-md-6">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
</div>
</div>
    <div class="row">
<div class="col-md-6">
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
</div>
</div>
    <div class="row">
<div class="col-md-6">
    <?= $form->field($model, 'discount_type')->textInput(['maxlength' => true]) ?>
</div>
</div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
</div>
    <?php ActiveForm::end(); ?>

</div>
