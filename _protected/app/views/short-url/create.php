<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ShortUrl */

$this->title = Yii::t('app', 'Create Short Url');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Short Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
