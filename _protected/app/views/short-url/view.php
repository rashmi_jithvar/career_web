<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShortUrl */

$this->title = 'Result';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Short Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="short-url-view">

    <div class="card-head text-right">
        <a href="<?= Url::to(['short-url/excel', 'id' => $id]); ?>" target="_blank" class="btn btn-success">Export to Excel</a>
    </div>
    <br />
    <table class="table table-dark table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th>Original URL</th>
                <th>Short URL</th>
                <th>Clicks</th>
                <th>Tags</th>
                <th>Created By</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($model as $mKey => $url) {
            ?>
            <tr>
                <td><?= $mKey + 1; ?></td>
                <td><?= $url->original_url; ?></td>
                <td><a href="<?= Url::base(true); ?>/../<?= $url->short_url; ?>" target="_blank"><?= substr(Url::base(true), 0, -3); ?><?= $url->short_url; ?></a></td>
                <td><?= $url ? $url->clicks : ''; ?></td>
                <td><?= $url ? $url->tags : ''; ?></td>
                <td><?= $url ? $url->created->username : ''; ?></td>
            </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
 </div>
