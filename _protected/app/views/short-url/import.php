<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\ShortUrl */

$this->title = Yii::t('app', 'Create Short Url by Import');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Short Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box">
    <div class="card-body " id="bar-parent">
	    <?php $form = ActiveForm::begin(); ?>

	    <div class="row">
	        <div class="col-md-12">
	            <?= $form->field($model, 'csvFile')->fileInput(['rows' => 6]) ?>
	        </div>
	    </div>

	    <div class="row">
	        <div class="col-md-12">
	            <div class="form-group">
	                <?= Html::submitButton(Yii::t('app', 'Import CSV'), ['class' => 'btn btn-success']) ?>
	            </div>
	        </div>
	    </div>

	    <?php ActiveForm::end(); ?>
	</div>
</div>
