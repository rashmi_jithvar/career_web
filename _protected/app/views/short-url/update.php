<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ShortUrl */

$this->title = Yii::t('app', 'Update Short Url: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Short Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="card card-box">
    <div class="card-head">
    	<header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>
	</div>
</div>
