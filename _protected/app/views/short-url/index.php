<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ShortUrlSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Short Urls');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-body " id="bar-parent">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Short Url'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Import CSV & Choose File'), ['import'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'batch_id',
            // 'short_url:url',
            'tags',
            ['attribute' => 'created_by', 'value' =>  function($data){
                return $data->created->username;
            }],
            ['attribute' => 'created_on', 'value' => function($data){
                return date('d-m-Y h:i:s', strtotime($data->created_on));
            }],
            'created_ip',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view1}',
                                    'contentOptions' => ['style' => 'width:15%;'],
                'buttons' => [

                    'view1' => function ($url,$model,$key){
                        // if($uRole != 'client')
                        {
                            $url = Url::to(['short-url/view', 'id' => $model->batch_id]);
                            return Html::a('<span class="icon icon-eye" data-popup="tooltip" data-original-title="Security Deposit" data-placement="top"></span>', $url);
                        }
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
