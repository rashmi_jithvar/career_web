<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShortUrl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="short-url-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'original_url')->textarea(['rows' => 6]); ?>
        </div>
        
        <div class="col-md-12">
            <?= $form->field($model, 'tags')->textInput(['data-role' => 'tagsinput']); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Create short URL'), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
