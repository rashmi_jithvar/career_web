<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Roles;

/* @var $this yii\web\View */
/* @var $model common\models\ProcessStepMapping */
/* @var $form yii\widgets\ActiveForm */
$getRoles = Roles::find()->where(['status' => Roles::STATUS_ACTIVE])->all();
$roles = ArrayHelper::map($getRoles, 'id', 'name');
?>

<div class="process-step-mapping-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'from_step')->dropDownList($process, ['class' => 'select-search', 'prompt' => 'Please Select']); ?>

    <?= $form->field($model, 'to_step[]')->dropDownList($process, ['class' => 'select-search', 'multiple' => true]); ?>

    <?= $form->field($model, 'display_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'to_role[]')->dropDownList($roles, ['class' => 'select-search', 'multiple' => true]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
