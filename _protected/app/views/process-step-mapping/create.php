<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProcessStepMapping */

$this->title = Yii::t('app', 'Create Process Step Mapping');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Process Step Mappings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card card-box col-sm-6">
    <div class="card-body " id="bar-parent">
	    <?= $this->render('_form', [
	        'model' => $model,
            'process' => $process,
	    ]) ?>
	</div>
</div>
