<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Area */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="area-form">
<div class="container">
    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-3">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
  </div>
   <div class="col-md-3">
    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
</div>
 <div class="col-md-3">
    <?= $form->field($model, 'parent_id')->textInput() ?>
</div>
 <div class="col-md-3">
    <?= $form->field($model, 'status')->textInput() ?>
</div>
</div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
