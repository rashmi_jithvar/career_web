<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-header">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

        <p>
            <?= Html::a(Yii::t('app', '<i class="fa fa-plus"></i> Add'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    </div >
    <div class="card-body " id="bar-parent">


        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'id',
                'name',
//                'template_id',
                [
                    "attribute" => "templateM",
                    "value" => function($model) {
                        return $model->template->name;
                    },
                ],
//            'content:ntext',
//            'image',
//                'status',
                [
                    'attribute' => 'status',
                    'filter' => array($searchModel::STATUS_ACTIVE => "Active", $searchModel::STATUS_INACTIVE => "Inactive", $searchModel::STATUS_PENDING => "Pending", $searchModel::STATUS_DELETE => "Delete"),
                    'value' => function($model) {
                        return $model->getStatus();
                    },
                ],
                'slug',
//                'created_by', 
                [
                    "attribute" => "createdUser",
                    "value" => function ($model) {
                        return $model->createdBy->username;
                    },
                ],
                'created_on',
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update}{delete}'],
            ],
        ]);
        ?>
        <?php Pjax::end(); ?>
    </div>
</div>
