<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::t('app', 'Create Page');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-box">
    <div class="card-head">

        <header><?= Html::encode($this->title) ?></header>
    </div>
    <div class="card-body">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>

</div>