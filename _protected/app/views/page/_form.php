<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Template;
use common\models\Page;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
<div class="container">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="row">
    <div class="col-md-3">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-md-3">
    <?php $dataCategory=ArrayHelper::map(Page::find()->all(), 'id', 'name');
    echo $form->field($model, 'sub_name')->dropDownList($dataCategory, 
             ['prompt'=>'-Choose a Service-'
            ]); ?>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'template_id')->dropDownList(ArrayHelper::map(Template::find()->where(['status' => Template::STATUS_ACTIVE])->all(), "id", "name"), ['prompt' => 'Select a Template']) ?>
</div>
<div class="col-md-3">
        <?= $form->field($model, 'og_type')->textInput() ?>
</div>   
</div>
<div class="row">
    
    <div class="col-md-3">
         <?= $form->field($model, 'og_url')->textInput() ?>
    </div>
    <div class="col-md-3">
         <?= $form->field($model, 'og_site')->textInput() ?>
    </div>
    <div class="col-md-3">
         <?= $form->field($model, 'og_file')->fileInput(['accept' => 'image/*']) ?>
    </div>
    <div class="col-md-3">
         <?= $form->field($model, 'image_file')->fileInput(['accept' => 'image/*']) ?>
    </div>
</div>
 <?= $form->field($model, 'content')->textarea(['rows' => 6, 'class' => 'form-control summernote']) ?>
    <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'meta_desc')->textarea(['rows' => 6]) ?>
    
    <?= $form->field($model, 'og_description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'status')->dropDownList([$model::STATUS_ACTIVE => 'Active', $model::STATUS_PENDING => 'Pending', $model::STATUS_INACTIVE => 'Inactive', $model::STATUS_DELETE => "Delete"], ['prompt' => 'Select An Option', 'class' => 'select-search']) ?>
    <?php //= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'created_by')->textInput() ?>

    <?php //= $form->field($model, 'created_on')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
<?php
$js = ' 
        $(document).ready(function() { 
            
        });
    ';

$this->registerJs($js, yii\web\View::POS_END);
