<?php

/**
 * -----------------------------------------------------------------------------
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * -----------------------------------------------------------------------------
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

// set @themes alias so we do not have to update baseUrl every time we change themes
Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 *
 * Customized by Nenad Živković
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@themes';
    public $css = [
        'assets/vendor/bootstrap/css/bootstrap.min.css',
        'assets/vendor/animate-css/animate.min.css',
        'assets/vendor/font-awesome/css/font-awesome.min.css',
        'assets/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css',
        'assets/vendor/chartist/css/chartist.min.css',
        'assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css',
        'light/assets/css/main.css',
        'assets/vendor/select2/css/components.min.css',
        // 'assets/vendor/select2/css/styles.css',
        'light/assets/css/color_skins.css',
        'assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css',
        'assets/vendor/jquery-datatable/dataTables.bootstrap4.min.css',
        'assets/vendor/jquery-datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css',
        'assets/vendor/jquery-datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css',
        'assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        'assets/vendor/summernote/dist/summernote.css',
        'assets/vendor/sweetalert/sweetalert.css',
    ];
    public $js = [
        // 'assets/plugins/jquery/jquery.min.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'light/assets/bundles/libscripts.bundle.js',
        'light/assets/bundles/vendorscripts.bundle.js',
        'light/assets/bundles/chartist.bundle.js',
        'light/assets/bundles/knob.bundle.js',
        'light/assets/bundles/flotscripts.bundle.js',
        'assets/vendor/flot-charts/jquery.flot.selection.js',
        'light/assets/bundles/mainscripts.bundle.js',
        'light/assets/bundles/morrisscripts.bundle.js',
        'assets/vendor/select2/js/interactions.min.js',
        'assets/vendor/select2/js/select2.min.js',
        'assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js',
        'assets/vendor/select2/js/form_select2.js',
        'light/assets/js/index.js',
        'assets/vendor/ckeditor/ckeditor.js',
        'assets/vendor/summernote/dist/summernote.js',
        'assets/vendor/multi-select/js/jquery.multi-select.js',
        'assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js',
        'light/assets/bundles/datatablescripts.bundle.js',
        'assets/vendor/jquery-datatable/buttons/dataTables.buttons.min.js',
        'assets/vendor/jquery-datatable/buttons/buttons.colVis.min.js',
        'assets/vendor/jquery-datatable/buttons/buttons.html5.min.js',
        'assets/vendor/jquery-datatable/buttons/buttons.print.min.js',
        'light/assets/js/pages/tables/jquery-datatable.js',
        'assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'assets/vendor/sweetalert/sweetalert.min.js',
        '../../js/common.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
