<?php

namespace app\controllers;

use Yii;
use common\models\Page;
use common\models\PageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\models\Common;
use yii\base\Exception;
use yii\db\Expression;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Page();

        $model->scenario = Page::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post())) {


            $transaction = Yii::$app->db->beginTransaction();

            try {

                $model->slug = Common::createSlug($model->name);

                $model->image_file = UploadedFile::getInstance($model, "image_file");
                if ($model->image_file) {

                    $model->image_file->saveAs(\Yii::getAlias('@webroot') . "/../uploads/page/" . $model->slug . '-' . time() . '.' . $model->image_file->extension, false);

                    //save the path in the db column
                    $model->image = "/uploads/page/" . $model->slug . '-' . time() . '.' . $model->image_file->extension;
                }

                $model->og_file = UploadedFile::getInstance($model, "og_file");
                if ($model->og_file) {

                    $model->og_file->saveAs(\Yii::getAlias('@webroot') . "/../uploads/page/" . $model->slug . '-og-' . time() . '.' . $model->og_file->extension, false);

                    //save the path in the db column
                    $model->og_image = "/uploads/page/" . $model->slug . '-og-' . time() . '.' . $model->og_file->extension;
                }

                $model->created_by = Yii::$app->user->id;
                $model->created_on = new Expression("now()");
                $model->save();

                if ($model->errors) {
                    throw new \Exception("Error Saving Page :" . json_encode($model->errors), 1);
                }

                $transaction->commit();
                return $this->redirect(['index']);
            } catch (Exception $ex) {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request " . $e, 1);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $transaction = Yii::$app->db->beginTransaction();

            try {

                $model->slug = Common::createSlug($model->name);

                $model->image_file = UploadedFile::getInstance($model, "image_file");
                if ($model->image_file) {

                    $model->image_file->saveAs(\Yii::getAlias('@webroot') . "/../uploads/page/" . $model->slug . '-' . time() . '.' . $model->image_file->extension, false);


                    if ($model->image != "" && file_exists(\Yii::getAlias('@webroot') . "/../" . $model->image))
                        unlink(\Yii::getAlias('@webroot') . "/../" . $model->image);

                    //save the path in the db column
                    $model->image = "/uploads/page/" . $model->slug . '-' . time() . '.' . $model->image_file->extension;
                }

                $model->og_file = UploadedFile::getInstance($model, "og_file");
                if ($model->og_file) {

                    $model->og_file->saveAs(\Yii::getAlias('@webroot') . "/../uploads/page/" . $model->slug . '-og-' . time() . '.' . $model->og_file->extension, false);

                    if ($model->og_image != "" && file_exists(\Yii::getAlias('@webroot') . "/../" . $model->og_image))
                        unlink(\Yii::getAlias('@webroot') . "/../" . $model->og_image);

                    //save the path in the db column
                    $model->og_image = "/uploads/page/" . $model->slug . '-og-' . time() . '.' . $model->og_file->extension;
                }

                $model->created_by = Yii::$app->user->id;
                $model->created_on = new Expression("now()");
                $model->save();

                if ($model->errors) {
                    throw new \Exception("Error Saving Page :" . json_encode($model->errors), 1);
                }

                $transaction->commit();
                return $this->redirect(['index']);
            } catch (Exception $ex) {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request " . $e, 1);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->image != "" && file_exists(\Yii::getAlias('@webroot') . "/../" . $model->image))
            unlink(\Yii::getAlias('@webroot') . "/../" . $model->image);
        if ($model->og_image != "" && file_exists(\Yii::getAlias('@webroot') . "/../" . $model->og_image))
            unlink(\Yii::getAlias('@webroot') . "/../" . $model->og_image);

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
