<?php
namespace app\controllers;

use common\models\User;
use common\models\UserSearch;
use common\rbac\models\Role;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Controller;
use common\models\ChangePassword;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * Lists all User models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     *
     * @param  integer $id The user id.
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Displays a single User model.
     *
     * @param  integer $id The user id.
     * @return string
     *
     * @throws NotFoundHttpException
     */
    public function actionProfile()
    {
        $user_id = Yii::$app->user->id;
        $user =  User::find()->where(['id' => $user_id])
                        ->with('roles')
                        ->one();
                     
        return $this->render('profile', [
            'model' => $user,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $user = new User(['scenario' => 'create']);
        $role = new Role();

        if ($user->load(Yii::$app->request->post()) && 
            $role->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$user, $role]))
        {
            $user->setPassword($user->password);
            $user->generateAuthKey();

            $user->role_id = $role->item_name;
            
            if ($user->save()) 
            {
                $role->user_id = $user->getId();
                $role->save(); 
            }  

            return $this->redirect('index');      
        } 
        else 
        {
            return $this->render('create', [
                'user' => $user,
                'role' => $role,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param  integer $id The user id.
     * @return string|\yii\web\Response
     *
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        // get role
        $role = Role::findOne(['user_id' => $id]);

        // get user details
        $user = $this->findModel($id);

        // only The Creator can update everyone`s roles
        // admin will not be able to update role of theCreator
        if (!Yii::$app->user->can('theCreator')) 
        {
            if ($role->item_name === 'theCreator') 
            {
                return $this->goHome();
            }
        }

        // load user data with role and validate them
        if(Yii::$app->request->post())
        // if ($user->load(Yii::$app->request->post()) && 
            // $role->load(Yii::$app->request->post()) && Model::validateMultiple([$user, $role])) 
        {
            $user->load(Yii::$app->request->post());
            // var_dump($user);
            // echo $user->first_name;
            // exit();
            $role->load(Yii::$app->request->post());
            $user->role_id = $role->item_name;
            // only if user entered new password we want to hash and save it
            if ($user->password) 
            {
                $user->setPassword($user->password);
            }

            // if admin is activating user manually we want to remove account activation token
            if ($user->status == User::STATUS_ACTIVE && $user->account_activation_token != null) 
            {
                $user->removeAccountActivationToken();
            }            

            $user->save(false);
            // $role->save(false); 
            // exit();
            return $this->redirect(['view', 'id' => $user->id]);
        }
        else 
        {
            return $this->render('update', [
                'user' => $user,
                'role' => $role,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param  integer $id The user id.
     * @return \yii\web\Response
     *
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        // delete this user's role from auth_assignment table
        if ($role = Role::find()->where(['user_id'=>$id])->one()) 
        {
            $role->delete();
        }

        return $this->redirect(['index']);
    }
/* change Password */
    public function actionChangePassword() {

        $model = ChangePassword::findSelfOne(['id' => \Yii::$app->user->id]);
        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();

            try {

                $result = $model->changePassword();
                if (is_bool($result) && $result) {

                    $transaction->commit();
                        Yii::$app->session->setFlash('success', "<div class='alert alert-success alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    <strong>Password Update Successfully.!</strong></div>");
                        return Yii::$app->getResponse()->redirect(['user/change-password']);
                    \Yii::$app->end();
                } else {
                    $transaction->rollBack();
                        Yii::$app->session->setFlash('success', "<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    <strong>Old Password is not matched.</strong></div>");
                        return Yii::$app->getResponse()->redirect(['user/change-password']);
                    \Yii::$app->end();
                }
            } catch (Exception $exc) {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request " . $exc, 1);
            }
        }

        return $this->render('change_password', [
                    'model' => $model,
        ]);
    }    

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param  integer $id The user id.
     * @return User The loaded model.
     *
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) 
        {
            return $model;
        } 
        else 
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
