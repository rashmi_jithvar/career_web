<?php

namespace app\controllers;

use Yii;
use common\models\Question;
use common\models\SearchQuestion;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Image;
use common\models\user;
use yii\web\UploadedFile;
use common\models\common;
/**
 * QuestionController implements the CRUD actions for Question model.
 */


class QuestionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Question models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchQuestion();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Question model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Question model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Question();
        $model2 = new User();
        $model1= new Image();

        if ($model->load(Yii::$app->request->post()) && $model1->load(Yii::$app->request->post())  ) {
            $model->slug = Common::createSlug($model->question);
           $model->created_by =  Yii::$app->user->identity->id;
           $model->created_on =  date('Y-m-d H:i:s');
            $model->save();
 /*           $id = Yii::$app->db->getLastInsertID();
            if (Yii::$app->request->isPost) {
            $file = UploadedFile::getInstances($model1, 'image');
           if (!empty($file))
                $model1->image = $file;
                $model1->ques_id=$id;
                $model1->save();
            if($model1->save())
            {
            if (!empty($file))
              //$file->saveAs( Yii::getAlias('@root') .'/images/service_banner/' . $file);   
            //return $this->redirect(['view', 'id' => $model->id]);
       // }

    }
}*/
            return $this->redirect(['view', 'id' => $model->q_id]);
} 
        return $this->render('create', [
            'model' => $model,'model1' => $model1,
        ]);
    }

    /**
     * Updates an existing Question model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->q_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Question model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Question model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Question the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Question::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
