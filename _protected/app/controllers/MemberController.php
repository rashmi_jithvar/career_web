<?php

namespace app\controllers;

use Yii;
use common\models\Member;
use common\models\MemberImage;
use common\models\MemberSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Plan;
use common\models\City;
use common\models\Master;
use common\models\State;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\db\Expression;
use common\models\Commission;
use yii\helpers\ArrayHelper;
/**
 * MemberController implements the CRUD actions for Member model.
 */
class MemberController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Member models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MemberSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Member model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = Member::find($id)
                        ->with('plan','image','statename','cityname')
                        ->where(['id'=>$id])
                        ->one();
        $team = New Member();   
        $left = $team->getFindLeft($id); 
        $right = $team->getFindRight($id);   
        if(empty($left)){
            $left =0;
        } 
        if(empty($right)){
            $right = 0;
        }         
                       // print_r($model->statename->name);exit;
        //print_r($model);exit;
        return $this->render('view', [
            'model' => $model,
            'left' => $left,
            'right' =>$right,

        ]);
    }

    /**
     * Creates a new Member model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Member();
        $image = new MemberImage();
        $get_member_ddl = $model->getMember();
        $plan = new Plan();
        $get_plan_ddl = $plan->getPlan();
        $state = new State();
        $get_state_ddl = $state->getState();

        if ($model->load(Yii::$app->request->post()) && $image->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                   // print_r(Yii::$app->request->post());exit;
                $model->dob= date('Y-m-d',strtotime(Yii::$app->request->post('dob')));
                $model->date= date('Y-m-d',strtotime(Yii::$app->request->post('date')));
                
            if ($image->upload_photo = UploadedFile::getInstance($image,'upload_photo')) {
                $image->upload_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .time() . '.' . $image->upload_photo);
                //save the path in DB..
                $image->upload_photo = $image->upload_photo;
                $image->save();
            }        

            if ($image->id_proof_photo = UploadedFile::getInstance($image,'id_proof_photo')) {
                $image->id_proof_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .time() . '.' . $image->id_proof_photo);
                //save the path in DB..
                $image->id_proof_photo = $image->id_proof_photo;
                $image->save();
            } 

            if ($image->pan_card_photo = UploadedFile::getInstance($image,'pan_card_photo')) {
                $image->pan_card_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .time() . '.' . $image->pan_card_photo);
                //save the path in DB..
                $image->pan_card_photo = $image->pan_card_photo;
                $image->save();
            } 
            if ($image->passbook_photo = UploadedFile::getInstance($image,'passbook_photo')) {
                $image->passbook_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .time() . '.' . $image->passbook_photo);
                //save the path in DB..
                $image->passbook_photo = $image->passbook_photo;
                $image->save();
            }
            $model->created_by =Yii::$app->user->id;
            $model->created_on =  new Expression("now()");
            $model->save();
            //$model->status = '1';
           // $model->save()e;
            $image->member_id = $model->id;
            $image->save();
        /* genarate unique no.*/
            $randomletter = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5);             
            $model->unique_id = $randomletter.$model->id;
            $model->save();  
        /* update leg count */
            //$ret = explode('/', $model->upline_id);
            //$upline = $ret[0];
         
            $check_sponser = Member::find()
                                    ->where(['id' => $model->sponser_id])
                                    ->One();   
                                    //print_r($check_sponser);exit;       
            if(!empty($check_sponser)){
                if($check_sponser->leg_count ==0){
                    $check_sponser->leg_count=1;
                }
                else{
                    $check_sponser->leg_count=2;
                }
                $check_sponser->save();
                //print_r($check_sponser->errors);exit;
            }
            /* get Direct Commission */
            $to_sponser = $check_sponser->id;
            $from_sponser = $model->id;
            $plan_amount = $model->plan_amount;
            $plan_id = $model->plan_id;
            $commission = new Commission();
            $data = $commission->getDirectCommission($to_sponser,$from_sponser,$plan_amount,$plan_id);
                                 
            $transaction->commit();
            return $this->redirect(['/member']);
            
          // else {
          //    throw new \Exception("Error Saving Invoice :" . json_encode($model->errors), 1);
          //   }
        }
             catch (Exception $exc) {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request " . $exc, 1);
            } 
        }           
        return $this->render('create', [
            'model' => $model,
            'get_member_ddl' => $get_member_ddl,
            'get_plan_ddl' =>$get_plan_ddl,
            'image'=>$image,
            'get_state_ddl' =>$get_state_ddl,
        ]);
    }

    /**
     * Updates an existing Member model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $image = MemberImage::find()
                                    ->where(['member_id' => $id])
                                    ->One();  

                            //print_r($image);exit;
        $get_member_ddl = $model->getMember();
        $plan = new Plan();
        $get_plan_ddl = $plan->getPlan();
        $state = new State();
        $get_state_ddl = $state->getState();   
        $city = new City();

        $get_city_ddl = $city->getCity($model->state);
                     
        if ($model->load(Yii::$app->request->post()) &&  $image->load(Yii::$app->request->post())) {
            

                $model->dob= date('Y-m-d',strtotime(Yii::$app->request->post('dob')));
                $model->date= date('Y-m-d',strtotime(Yii::$app->request->post('date')));
                
            if ($image->upload_photo = UploadedFile::getInstance($image,'upload_photo')) {
                $image->upload_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/" .$image->upload_photo);
                //save the path in DB..
               $image->upload_photo = $image->upload_photo;
                $image->save();
            }        

            if ($image->id_proof_photo = UploadedFile::getInstance($image,'id_proof_photo')) {
                $image->id_proof_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .$image->id_proof_photo);
                //save the path in DB..
                $image->id_proof_photo = $image->id_proof_photo;
                $image->save();
            } 

            if ($image->pan_card_photo = UploadedFile::getInstance($image,'pan_card_photo')) {
                $image->pan_card_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .$image->pan_card_photo);
                //save the path in DB..
                $image->pan_card_photo = $image->pan_card_photo;
                $image->save();
            } 
            if ($image->passbook_photo = UploadedFile::getInstance($image,'passbook_photo')) {
                $image->passbook_photo->saveAs(\Yii::getAlias('@webroot') . "/uploads/"  .$image->passbook_photo);
                //save the path in DB..
                $image->passbook_photo = $image->passbook_photo;
                $image->save();
            }
            $model->updated_by =Yii::$app->user->id;
            $model->updated_on =  new Expression("now()");
            $model->save();
            //$model->status = '1';
           // $model->save()e;
            $image->member_id = $id;
            $image->save();   
            //print_r($image->errors);exit;
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'get_member_ddl' => $get_member_ddl,
            'get_plan_ddl' =>$get_plan_ddl,
            'image'=>$image,
            'get_state_ddl'=>$get_state_ddl,
            'get_city_ddl'=>$get_city_ddl,
        ]);
    }

    /**
     * Deletes an existing Member model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Member model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Member the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Member::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /* get Upline */
    public function actionGetUpline(){
       $sponser_id = Yii::$app->request->post('sponser_id');
       $findLegCount = Member::find()
                        ->where(['<', 'leg_count', '2'])  
                        ->where(['id'=>$sponser_id])  
                        ->One();

         //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;           
        if($findLegCount){
          echo $findLegCount->unique_id."/".$findLegCount->applicant_name;
        }  
        else{
            echo False;
        }              
        //$dataOptions = array(''=>'Select Upline');
        //$data=CHtml::listData($data,'id','concat(sponser_id,"/",applicant_name) as name');

        // foreach ($findLegCount as $upline) {
        //     $dataOptions[$upline->id]= $upline->sponser_id.'/'.$upline->applicant_name;
        // }
        //return json_encode($dataOptions);
        //return $dataOptions;                        
        //return print_r($findLegCount);exit;
    }  

    /* get Leg */
    public function actionGetLeg(){
       $sponser_id = Yii::$app->request->post('sponser_id');
       $findLegCount = Member::find()
                        ->where(['<', 'leg_count', '2'])  
                        ->where(['id'=>$sponser_id])  
                        ->One();
        $data = "<option value=>Select</option>";                
        if($findLegCount->leg_count =='0'){
                $data .= '<option value="left">Left</option>';
                $data .= '<option value="right">Right</option>';
                return $data;
        }
        else if($findLegCount->leg_count=='1')  {
            $get_leg = $findLegCount->left_right_leg;
            if($get_leg =='right'){
                $data .= '<option value="left">Left</option>';
                return $data;
            }
            else{
                $data .= '<option value="right">Right</option>';
                return $data;
              
            }
        }
        else{
                return False;
        }     

    }         


    /* get Leg */
    public function actionGetState(){
       $state_id = Yii::$app->request->post('state_id');
       $findState = Master::find()
                        ->where(['abbr', 'City'])  
                        ->where(['parent_id'=>$state_id])  
                        ->All();
                       // print_r($findState);exit;
                     
        if($findState){
            $data = "<option value=>Select</option>";   
            foreach ($findState as $key => $value) {
                $data .= '<option value="">'.$value->name.'</option>';
            }

           
        }

        else{
                $data = "<option value=>No Data Found</option>";    ;
        } 
         return $data;    

    }  


    /* get Leg */
    public function actionCityDropdown($id){
        $countPosts = Master::find()
            ->where(['parent_id' => $id])
            ->count();

        $posts =  Master::find()
            ->where(['parent_id' => $id])
            ->where(['abbr'=> 'City'])  
            ->orderBy('name ASC')
            ->all();
        echo "<option value=''>Select</option>";
        if($countPosts>0){
            foreach($posts as $post){
                echo "<option value='".$post->id."'>".Yii::t('app',$post->name)."</option>";
            }
        } 

    }  

    /**
     * Deletes an existing Member model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionFindTeam($id)
    {
        $team = Member::find()->where(['id'=>$id])->one();
        if(!empty($team)){
            $team_left = Member::find()->where(['sponser_id'=>$team->id])->where(['left_right_leg'=>'left'])->one();
            $team_right = Member::find()->where(['sponser_id'=>$team->id])->where(['left_right_leg'=>'right'])->one();

            if(empty($team_left)){
                    $team_left = 0;
            }
            if(empty($team_right)){
                $team_right = 0;
            }
            return $team = array('left' =>$team_left ,'right'=>$team_right );
        }

    }

    public function actionTree(){
        $model = Member::findOne('1');
        return $this->render('tree', [
            'model' => $model,
        ]);
    }

}
