<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Roles;
use common\models\Permission;
use common\models\RolePermission;
use common\models\PermissionGroup;
use common\models\RolePermissionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * RolePermissionController implements the CRUD actions for RolePermission model.
 */
class RolePermissionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RolePermission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RolePermissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RolePermission model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RolePermission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RolePermission();

        $getRole = Roles::find()
                        ->where(['status' => Roles::STATUS_ACTIVE])
                        ->all();

        $map_roles   =   ArrayHelper::map($getRole, 'id', 'name');

        $permGroup = PermissionGroup::find()
                                    ->where(['status' => PermissionGroup::STATUS_ACTIVE])
                                    ->all();

        $role = Yii::$app->request->get("role");
        $permission = null;
        if($role)
        {
            $permission = RolePermission::find()->select("permission_id")->where(['role_id' => $role])->all();
        }

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            RolePermission::deleteAll(['role_id' => $role]);
            foreach (array_filter($model->permission_id) as $pK => $perm) {
                echo $perm;
                $new_model = new RolePermission();
                $new_model->permission_id   =   $perm;
                $new_model->role_id         =   $model->role_id;
                $new_model->status          =   $new_model::STATUS_ACTIVE;
                $new_model->created_by      =   Yii::$app->user->id;
                $new_model->save();
            }
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->refresh();
        }

        return $this->render('create', [
            'model' => $model,
            'map_roles' => $map_roles,
            'permGroup' => $permGroup,
            'permission' => $permission,
        ]);
    }

    /**
     * Updates an existing RolePermission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RolePermission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RolePermission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RolePermission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RolePermission::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
