<?php

namespace app\controllers;

use Yii;
use common\models\ShortUrl;
use common\models\ShortUrlSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;

/**
 * ShortUrlController implements the CRUD actions for ShortUrl model.
 */
class ShortUrlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShortUrl models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShortUrlSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShortUrl model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => ShortUrl::find()
                                ->where(['batch_id' => $id])
                                ->all(),
            'id' => $id,
        ]);
    }

    /**
     * Creates a new ShortUrl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShortUrl();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {

            $original_url = explode("\n", $model->original_url);
            $time = time();


            $transaction = Yii::$app->db->beginTransaction();

            try{
                foreach ($original_url as $oKey => $url) {
                    if($url)
                    {
                        $newModel = new ShortUrl();
                        $newModel->original_url     =   $url;
                        $newModel->batch_id         =   $time;
                        $newModel->short_url        =   $newModel->generateRandomString();
                        $newModel->created_by       =   Yii::$app->user->id;
                        $newModel->created_ip       =   Yii::$app->request->userIP;
                        $newModel->tags             =   $model->tags;
                        $newModel->save();
                        if($newModel->errors && $newModel->errors['short_url'])
                        {
                            if($newModel->errors['short_url'][0] == 'Already Exist')
                            {
                                $newModel->short_url        =   $newModel->generateRandomString();
                                $newModel->save();
                            }
                        }
                    }
                }
                $transaction->commit();
            }
            catch(\Exception $e)
            {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request ". $e, 1);
            }

            return $this->redirect(['view', 'id' => $time]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShortUrl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShortUrl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShortUrl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShortUrl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShortUrl::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    // function for excel export
    public function actionExcel($id)
    {
        $model =  ShortUrl::find()
                            ->where(['batch_id' => $id])
                            ->all();

        $html = $this->renderPartial('view', [
            'model' => $model,
            'id' => $id,
        ]);

        $file="short_url-". $id .".xls";
        header("Content-type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=$file");
        echo $html;
    }


    /**
     * Creates a new ShortUrl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionImport()
    {
        $model = new ShortUrl();
        $model->scenario = 'import';

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $original_url = explode("\n", $model->original_url);
            $time = time();

            $transaction = Yii::$app->db->beginTransaction();

            try{

                $file = UploadedFile::getInstance($model, 'csvFile');
                $handle = fopen($file->tempName, "r");

                while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
                {
                    if($filesop[0])
                    {
                        $newModel = new ShortUrl();
                        $newModel->original_url     =   $filesop[0];
                        $newModel->batch_id         =   $time;
                        $newModel->short_url        =   $newModel->generateRandomString();
                        $newModel->created_by       =   Yii::$app->user->id;
                        $newModel->created_ip       =   Yii::$app->request->userIP;
                        $newModel->save();
                        if($newModel->errors && $newModel->errors['short_url'])
                        {
                            if($newModel->errors['short_url'][0] == 'Already Exist')
                            {
                                $newModel->short_url        =   $newModel->generateRandomString();
                                $newModel->save();
                            }
                        }
                    }
                }
                $transaction->commit();
            }
            catch(\Exception $e)
            {
                $transaction->rollBack();
                throw new \Exception("Error Processing Request ". $e, 1);
            }

            return $this->redirect(['view', 'id' => $time]);
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }
}
