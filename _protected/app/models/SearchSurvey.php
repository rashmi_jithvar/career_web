<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Survey;

/**
 * SearchSurvey represents the model behind the search form of `common\models\Survey`.
 */
class SearchSurvey extends Survey
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['s_id', 'question_id', 'user_id', 'created_by', 'created_ip', 'update_id', 'update_by', 'correct_answer', 'status'], 'integer'],
            [['selected_ans', 'user_name', 'created_on', 'update_on', 'user_email'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Survey::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            's_id' => $this->s_id,
            'question_id' => $this->question_id,
            'user_id' => $this->user_id,
            'created_by' => $this->created_by,
            'created_on' => $this->created_on,
            'created_ip' => $this->created_ip,
            'update_id' => $this->update_id,
            'update_on' => $this->update_on,
            'update_by' => $this->update_by,
            'correct_answer' => $this->correct_answer,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'selected_ans', $this->selected_ans])
            ->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_email', $this->user_email]);

        return $dataProvider;
    }
}
