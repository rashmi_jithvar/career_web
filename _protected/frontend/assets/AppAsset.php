<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

// set @themes alias so we do not have to update baseUrl every time we change themes
Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@themes';

    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'css/fonts/font-awesome/css/font-awesome.css',
        'vendor/owl-carousel/owl.carousel.css',
        'vendor/owl-carousel/owl.theme.css',
        'vendor/flexslider/flexslider.css',
        'vendor/chosen/chosen.css',
        'css/theme-animate.css',
        'css/theme-elements.css',
        'css/theme-blog.css',
        'css/theme-map.css',
        'css/theme.css',
        'style-switcher/css/style-switcher.css',
        'css/colors/red/style.html',
        'css/theme-responsive.css',
        'http://fonts.googleapis.com/css?family=Rochester',
        'http://fonts.googleapis.com/css?family=Raleway:400,700'
    ];
    public $js = [
        'vendor/jquery.min.js',
        'bootstrap/js/bootstrap.min.js',
        'vendor/owl-carousel/owl.carousel.js',
        'vendor/flexslider/jquery.flexslider-min.js',
        'vendor/chosen/chosen.jquery.min.js',
        'http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true',
        'vendor/gmap/gmap3.infobox.min.js',
        'js/theme.plugins.js',
        'js/theme.js',
        //'style-switcher/js/switcher.js',
    ];
   public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
