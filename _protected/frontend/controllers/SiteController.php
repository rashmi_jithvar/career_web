<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\Survey;
use app\controllers\ShortUrl;
use frontend\models\ContactForm;
use common\components\AuthHandler;
use yii\web\NotFoundHttpException;
use yii\db\Expression;
use yii\base\Exception;
use common\models\Question;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }

    public function onAuthSuccess($client)
    {
        return (new AuthHandler($client))->handle();
        

    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
              'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
      
       
       //$model = new Survey();
       
         // $model->question_id = $_POST['question'];
         // $model->selected_ans =$_POST['option'];
          //$model->user_id = Yii::$app->user->id;
         // $model->created_by = Yii::$app->user->id;
         // $model->correct_answer = $row['answer'];
         // $model->user_email = Yii::$app->user->email;
         //$model->save();
       // echo "saved";
      //  return $this->goHome();
       //    
        // var_dump($_POST);
        // exit(); 
      //$answer = Survey::find()
               // ->select(['question_id'])
                //->where(['user_id' => Yii::$app->user->id]);    
     // $question = Question::find()
                //  ->where(['not in', 'q_id', $answer])
                  //->all();
       return $this->render('index');

}


//      

    public function actionInvite()
    {
        return $this->render('invite');
    }
   public function actionSdk()
    {
        return $this->render('login-call');
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
     

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
           $session = Yii::$app->session;
           if($session['answer'] !=""){
                $session['question'];
                     $session['answer'];
                        $question =Question::find()->where(['q_id'=>$session['question']])->all();
                            foreach($question as $row){
                                $answer= $row['answer'];
                            }
                              $model = new Survey();
                              $model->question_id = $session['question'];
                              $model->selected_ans =$session['answer'];
                              $model->user_id = Yii::$app->user->id;
                              $model->created_by = Yii::$app->user->id;
                              $model->correct_answer = $row['answer'];
                              $model->save();
                              unset($session['question']);
                              unset($session['answer']);
                              Yii::$app->session->setFlash('msg', '
                                 <div class="alert alert-success alert-dismissable">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <strong>Thank You For Participate.!!</strong> .</div>'
                              );
                           
                            $answer = Survey::find()
                                ->select(['question_id'])
                                ->where(['user_id' => Yii::$app->user->id]);    
                            $question = Question::find()
                                ->where(['not in', 'q_id', $answer])
                                ->all();
                            return $this->render('thanku',['question'=>$question]);

                }
                else{
                           return $this->goBack();
                }

            
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTest()
    {
        return $this->render('test');
    }


    public function actionProfile()
    {
        return $this->render('user_profile');
    }

     public function actionThanku()
    {

        return $this->render('thanku');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {

        $model = new SignupForm();
        // $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
          
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    $session = Yii::$app->session;
                if($session['answer'] !=""){
                        $question =Question::find()->where(['q_id'=>$session['question']])->all();
                            foreach($question as $row){
                                $answer= $row['answer'];
                            }
                              $model = new Survey();
                              $model->question_id = $session['question'];
                              $model->selected_ans =$session['answer'];
                              $model->user_id = Yii::$app->user->id;
                              $model->created_by = Yii::$app->user->id;
                              $model->correct_answer = $row['answer'];
                              $model->save();
                              unset($session['question']);
                              unset($session['answer']);
                            $answer = Survey::find()
                                ->select(['question_id'])
                                ->where(['user_id' => Yii::$app->user->id]);    
                            $question = Question::find()
                                ->where(['not in', 'q_id', $answer])
                                ->all();
                            Yii::$app->session->setFlash('msg', '
                                 <div class="alert alert-success alert-dismissable">
                                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                 <strong>Thank You For Participate.!!</strong> .</div>'
                              );    
                            return $this->render('thanku',['question'=>$question]);

                }
                else{
                        return $this->goHome(); 
                  }   
                }
                   
            }
        }
    }
       

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

   /* public function actionSlug($slug) {
        $model = ShortUrl::find()
                ->where(['short_url' => $slug])
                ->One();

        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $model->clicks = $model->clicks + 1;
        $model->save();

        return $this->redirect($model->original_url);
    }*/

    //
        public function actionSubmit()
    {
        $request = Yii::$app->request;
        $question = $request->post('question');
        $question =Question::find()->where(['q_id'=>$question])->all();
        foreach($question as $row){
            $answer= $row['answer'];
        }
       $model = new Survey();
       /// if ($model->load(Yii::$app->request->post())) {
          $model->question_id = $_POST['question'];
          $model->selected_ans =$_POST['option'];
          $model->user_id = Yii::$app->user->id;
          $model->created_by = Yii::$app->user->id;
          $model->correct_answer = $row['answer'];
          $model->save();
       // echo "saved";
        $answer = Survey::find()
                        ->select(['question_id'])
                        ->where(['user_id' => Yii::$app->user->id]);
        $question = Question::find()
                            ->where(['q_id' => $answer])
                            ->all();
        return $this->render('thanku',[
            'question' => $answer,
        ]);
       // $model->user_email = Yii::$app->user->email;        

    //}
   }

    public function actionSlug($slug) {
        $model = Question::find()
                ->where(['slug' => $slug])
                ->One();
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $model->view = $model->view + 1;
        $model->save();
        return $this->render('question',[
            'question' => $model,
        ]);

        // return $this->redirect($model->original_url);
    }

    public function actionQuestion()
    {
            $session = Yii::$app->session;
            $session['question'] = Yii::$app->request->post('question');
            $session['answer'] = Yii::$app->request->post('answer');
    }
}
