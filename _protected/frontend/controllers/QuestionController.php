<?php

namespace frontend\controllers;

use Yii;
use common\models\Post;
use common\models\PostSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use yii\base\Exception;
use common\models\Common;
use common\models\PostCategoryMapping;
use common\models\Category;
use common\models\CategorySearch;
use common\models\FileUpload;

/**
 * CategoryController implements the CRUD actions for Post model.
 */
class QuestionController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSlug($slug) {

        $question = Question::find()->where(['slug' => $slug]);
        print_r($question);exit;
        if ($question->count() <= 0) {
            throw new Exception("Question is not Found", 1);
        }

        $question = $question->one();

        return $this->render('index', [
                    'question' => $question,
        ]);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
