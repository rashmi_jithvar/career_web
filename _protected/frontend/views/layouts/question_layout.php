<?php use yii\helpers\Html;
use common\models\Question;

use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <h2></h2>
    </div>
</div><!-- end row -->

<div class="container">
    
<?php 
$answer = Survey::find()
                ->select(['question_id'])
                ->where(['user_id' => Yii::$app->user->id]);    
      $question = Question::find()
                  ->where(['not in', 'q_id', $answer])
                  ->all();
                  $i=0;
    foreach($question as $row){ $i++;?>
    <div class="col-md-4 col-sm-4 add_bottom_30 box">
        <p><img src="themes/jithvar/img/icon-1.png" alt="Icon"></p>
        <h3>Question no.<?php echo $i;?></h3>
        <p>
            <?php echo  $row->question;?>
        </p>

        <a href="<?= Url::to(['/' . $row->slug])?>" title="read more" class="button_medium_2">Read more</a>
    </div>
 <?php } ?>   
</div><!-- end row -->
