<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<!-- Start Survey container -->
<br>
<div class="container" style="background-color:white;width:80%;">
<div class="row">
    <div class="col-md-12"><br>
        <h3>Profile<span></span></h3>
    </div>
</div><!-- end row -->

<div class="row" style="margin:20px;">

  
    
    <div class="col-md-3">
        <div class="thumbnail">
            <div class="project-item-image-container"><img src="img/user.png" alt=""/></div>
            <div class="caption">
               
            </div>
        </div>
    </div><!-- team  item -->
   <div class="col-md-6">
   </div>
      <div class="col-md-6 pull-right">
        <h4>Account Details</h4>
        <p><b>Username :</b></p>
        <p><b>Email :</b></p>   

        <h4>Personal Details</h4>
        <p><b>Username :</b></p>
        <p><b>Gender :</b></p>
         <p><b>DOB :</b></p>
    </div>
    
    
    
</div><!-- end row -->
</section><!-- end section main container -->
  </div>     
<footer>
    <section class="container">
    <div class="row">
        <div class="col-md-4">
            <h3>About us</h3>
            <p>Lorem ipsum dolor sit amet, duo iudico accusamus ne, at vix sumo alia. Usu etiam probatus ne, eu magna ullum iusto his. Nec ea case eirmod dissentiunt. Etiam denique has cu, nam quando accusamus dissentiunt ne. Ea sit malorum scribentur.</p>
        </div>
        
        <div class="col-md-4" id="contact">
            <h3>Contact info</h3>
            <p>Nec ea case eirmod dissentiunt. Etiam denique has cu, nam quando accusamus dissentiunt ne. </p> 
                <ul>
                        <li><i class="icon-home"></i> 1110 Bates Avenue Los Angeles, CA 90029, US</li>
                        <li><i class="icon-phone"></i> Telephone: 41.22.319.36.10 </li>
                        <li><i class="icon-envelope"></i> Email: <a href="#">info@annova.com </a></li>
                        <li><i class="icon-skype"></i> Skype name: Annova</li>
                    </ul>    
        </div>
        
        <div class="col-md-4">
            <h3>Latest tweet</h3>
            <div class="latest-tweets" data-number="10" data-username="ansonika" data-mode="fade" data-pager="false" data-nextselector=".tweets-next" data-prevselector=".tweets-prev" data-adaptiveheight="true"></div>
              <div class="tweet-control">
                <div class="tweets-prev"></div>
                <div class="tweets-next"></div>
              </div><!-- End .tweet-control -->
        </div>
        
    </div><!-- end row -->
    </section>
    
    <section id="footer_2">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
                <ul id="footer-nav">
                    <li>CopyrightÂ© Ansonika </li>
                    <li><a href="#">Terms of Use</a></li>
                    <li><a href="#">Privacy</a></li>
                </ul>              
        </div>
            <div class="col-md-6" style="text-align:center">
                <ul class="social-bookmarks clearfix">
                    <li class="facebook"><a href="#">facebook</a></li>
                    <li class="googleplus"><a href="#">googleplus</a></li>
                    <li class="twitter"><a href="#">twitter</a></li>
                    <li class="delicious"><a href="#">delicious</a></li>
                    <li class="paypal"><a href="#">paypal</a></li>
                </ul>
            </div>
        </div>
        </div>
    </section>

</footer> 
 
 <div id="toTop">Back to Top</div>  

<!-- Modal About -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">About us</h4>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus.</p>
<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal About -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="termsLabel">Terms and conditions</h4>
      </div>
      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus.</p>
<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus.</p>
<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum 
sanctus, pro ne quod dicunt sensibus.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->