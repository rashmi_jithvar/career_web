<?php use yii\helpers\Html;
use common\models\Question;
use common\models\Survey;
use yii\helpers\Url;

$this->title =  $question->slug;
?>


<div class="container">
             <div class="row">
                 <div class="col-md-12 main-title">
                 <h1>Satisfaction survey</h1>
                <p>Help us to improve our service and customer satisfaction.</p>
                </div>
            </div>
</div>

 </section>   

<section class="container-fuild" id="main">
    <div class="row">
    <div class="col-md-2" style="border:2px solid;"></div>
<div  id="survey_container" class="col-md-8" style="border:2px solid;">

<?php  

            // foreach($question as $row){
$row = $question;
{
          ?>
<?= Html::beginForm(['site/submit']); ?>
        <div id="middle-wizard">
            <div class="step">
                <div class="row">
                    <h3 class="col-md-10"><?php echo $row->question;?></h3>
                    <div class="">
                        <input name="question" id="questionId" type="hidden" class="required check_radio questionId" value="<?php echo $row->q_id;?>">
                    <ul class="data-list  clearfix">
                        <div class="row">
                        <div class="col-md-12">
                            <li><input name="option" id="optId" type="radio" class="required check_radio" value="A"><label class="label_gender"> <?php echo $row->opt_a;?></label></li>
                        </div>
                        <div class="col-md-12">
                            <li><input name="option" id="optId" type="radio" class="required check_radio" value="B"><label class="label_gender"> <?php echo $row->opt_b;?></label></li>
                        </div>
                         </div>  
                         <div class="row"> 
                             <div class="col-md-12">
                            <?php if($row->opt_c !=''){?>
                                <li><input name="option"  id="optId" type="radio" class="required check_radio" value="C"><label class="label_gender"> <?php echo $row->opt_c;?></label></li>
                            <?php } ?>
                            </div>
                             <div class="col-md-12">
                             <?php if($row->opt_d !=''){?>
                            <li><input name="option" id="optId" type="radio" class="required check_radio" value="D"><label class="label_gender"> <?php echo $row->opt_d;?></label></li>
                            <?php } ?>
                           </div>
                        </div>    
                             <?php if($row->opt_e !=''){?>
                             <li><input name="option" id="optId" type="radio" class="required check_radio" value="E"><label class="label_gender"> <?php echo $row->opt_e;?></label></li>
                         <?php } ?>
                  </ul>
                    </div><!-- end col-md-6 -->
                    

                </div><!-- end row -->
                
     
                
            </div><!-- end step-->
            
        </div><!-- end middle-wizard -->
        
        <div id="bottom-wizard">

    <?php  if(Yii::$app->user->isGuest){ ?>
            <button type="submit"  id="saveBtn" value="0" data-toggle="modal" data-target="#myModal" class="btn btn-primary open-AddBookDialog">Submit </button>
        <?php } else {?>
            <button type="submit" value="1" class="btn btn-primary">Submit </button>
      <?php } ?>
        </div><!-- end bottom-wizard -->
<?= Html::endForm() ?>
<?php } ?>
    
</div><!-- end Survey container -->
<div class="col-md-2" style="border:2px solid;"></div>

</div>

<!---<div class="row">
    <div class="col-md-12">
        <h3>Some screenshots<span>This help us to improve our service and customer satisfaction.</span></h3>
    </div>
</div><!-- end row -->

<!--<div class="row">
    <div class="col-md-12">
    
        <div id="owl-demo">
            <div class="item">
                <a href="themes/jithvar/img/carousel/1.jpg" class="fancybox"><img src="img/carousel/1.jpg" alt="Image"></a>
            </div>
            <div class="item">
                <a href="themes/jithvar/img/carousel/2.jpg" class="fancybox"><img src="img/carousel/2.jpg" alt="Image"></a>
            </div>
            <div class="item">
                <a href="themes/jithvar/img/carousel/3.jpg" class="fancybox"><img src="img/carousel/3.jpg" alt="Image"></a>
            </div>
            <div class="item">
                <a href="themes/jithvar/img/carousel/4.jpg" class="fancybox" ><img src="img/carousel/4.jpg" alt="Image"></a>
            </div>
            <div class="item">
                <a href="themes/jithvar/img/carousel/5.jpg" class="fancybox" ><img src="img/carousel/5.jpg" alt="Image"></a>
            </div>
            <div class="item">
                <a href="themes/jithvar/img/carousel/1.jpg" class="fancybox" ><img src="img/carousel/1.jpg" alt="Image"></a>
            </div>
        </div>
        
    </div>
</div>  

<div class="divider"></div>

<div class="row">
    <div class="col-md-12">
        <h3>About us<span>This help us to improve our service and customer satisfaction.</span></h3>
    </div>
</div>

<div class="row">

    <div class="col-md-6">
        <h4>Our History</h4>
        <p>
             Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
        </p>
        <h4>Our Vision</h4>
        <p>
             Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.
        </p>
    </div>
    
    <div class="col-md-3">
        <div class="thumbnail">
            <div class="project-item-image-container"><img src="themes/jithvar/img/team_1.jpg" alt=""/></div>
            <div class="caption">
                <div class="transit-to-top">
                    <h4 class="p-title">Patricia Doe <small>CEO</small></h4>
                        <div class="widget_nav_menu">
                            <ul class="social-bookmarks team">
                                <li class="facebook"><a href="#">facebook</a></li>
                                <li class="googleplus"><a href="#">googleplus</a></li>
                                <li class="twitter"><a href="#">twitter</a></li>
                                <li class="linkedin"><a href="#">linkedin</a></li>
                            </ul>
                            <div class="phone-info"><i class="icon-phone-sign"></i> + 4 (123) 456-7890</div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
    <div class="col-md-3">
        <div class="thumbnail">
            <div class="project-item-image-container">
                <img src="themes/jithvar/img/team_2.jpg" alt=""/>
            </div>
            <div class="caption">
                <div class="transit-to-top">
                    <h4 class="p-title">Megan Fox <small>MANAGER</small></h4>
                        <div class="widget_nav_menu">
                            <ul class="social-bookmarks team">
                                <li class="facebook"><a href="#">facebook</a></li>
                                <li class="googleplus"><a href="#">googleplus</a></li>
                                <li class="twitter"><a href="#">twitter</a></li>
                                <li class="linkedin"><a href="#">linkedin</a></li>
                            </ul>
                            <div class="phone-info">
                                <i class="icon-phone-sign"></i> + 4 (123) 456-7890
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div><!-- end row -->

<?php
$session = Yii::$app->session;
$registerJs = '
 $(document).ready(function(){  
     $(".open-AddBookDialog").click(function(){
        $question =$("#questionId").val();

        $answer = $("#optId:checked").val();
        if($("#optId:checked").val()){

        }
        else{
            alert("Please Select the Option");
        }
        $.ajax({
            url: "'. Url::to(['site/question']) .'",
            type: "POST",
            data: {question:$question,answer:$answer,},
            dataType:"json",
            success: function (data) {
                alert("reached");
            },
            error: function(jqXHR, errMsg) { 
                // handle error
                //alert(errMsg);

            }
        });
      
        $("#question").val($question);
        $("#option").val($answer);
        $("#question_a").val($question);
        $("#option_a").val($answer);
        $("#myModal").modal("show"); 
       
    });
    });
';

$this->registerJs($registerJs);
?>