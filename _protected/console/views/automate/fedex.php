<?php
use yii\helpers\Html;
use common\models\Common;
/* @var $this yii\web\View */
/* @var $user common\models\User */
$customer = $model->order->customer;
// $this->registerCssFile("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
?>
<style type="text/css">
	.table-bordered {
	    border: 1px solid #dee2e6;
	}
	.table {
	    width: 100%;
	    margin-bottom: 1rem;
	    background-color: transparent;
	}

	table {
    	border-collapse: collapse;
	}
	*, ::after, ::before {
	    box-sizing: border-box;
	}
	table {
	    display: table;
	    border-collapse: separate;
	    border-spacing: 2px;
	    border-color: grey;
	}

	thead {
	    display: table-header-group;
	    vertical-align: middle;
	    border-color: inherit;
	}

	tr {
	    display: table-row;
	    vertical-align: inherit;
	    border-color: inherit;
	}

	.table-bordered thead td, .table-bordered thead th {
	    border-bottom-width: 2px;
	}
	.table thead th {
	    vertical-align: bottom;
	    border-bottom: 2px solid #dee2e6;
	}
	.table-bordered td, .table-bordered th {
	    border: 1px solid #dee2e6;
	}
	.table td, .table th {
	    padding: .75rem;
	    vertical-align: top;
	    border-top: 1px solid #dee2e6;
	}
	th {
	    text-align: inherit;
	}

	*, ::after, ::before {
	    box-sizing: border-box;
	}
	tbody {
	    display: table-row-group;
	    vertical-align: middle;
	    border-color: inherit;
	}

	.table-bordered td, .table-bordered th {
	    border: 1px solid #dee2e6;
	}
	.table td, .table th {
	    padding: .75rem;
	    vertical-align: top;
	    border-top: 1px solid #dee2e6;
	}
	*, ::after, ::before {
	    box-sizing: border-box;
	}
	td, th {
	    display: table-cell;
	    vertical-align: inherit;
	}
</style>
<?php
$order = $model->order;
if($model->carrier_code == 'stamps_com')
{
    $via = ' USPS';
}
else if($model->carrier_code == 'fedex')
{
    $via   =   ' Fedex';
}
else if($model->carrier_code == 'ups')
{
    $via   =   ' UPS';
}

// $via = $model->carrier_code == 'stamps_com' ? ' USPS' : ' FedEx';

if($model->confirmation == 'direct_signature')
{
	$confirmation = 'Direct Signature';
}
else if($model->confirmation == 'delivery')
{
	$confirmation = 'No Signature';
}
else if($model->confirmation == 'signature')
{
	$confirmation = 'Signature';
}
else if($model->confirmation == 'signature')
{
	$confirmation = 'Signature';
}


if($customer->ship_via == 10)
{
	$ship_via = 'Any';
}
if($customer->ship_via == 12)
{
	$ship_via = 'USPS';
}
else if($customer->ship_via == 11)
{
	$ship_via = 'Fedex';
}
else if($customer->ship_via == 15)
{
	$ship_via = 'UPS';
}

if($customer->signature == 13)
{
	$sign = " signature ";
}
else if($customer->signature == 14)
{
	$sign = " email";
}
?>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colspan="2">
					<?php
					if($email_type == 1)
					{
					?>
						Shipping label was created with ship via <?= $via; ?> however <?= $customer->first_name ." ". $customer->last_name; ?> has requested for <?= $ship_via; ?> for all of her Orders. Please check into this matter.
					<?php
					}
					else if($email_type == 2)
					{
					?>
						Shipping label was created with <?= $confirmation; ?> confirmation however <?= $customer->first_name ." ". $customer->last_name; ?> has requested for <?= $sign; ?> confirmation for all of her Orders. Please check into this matter.
					<?php
					}
					else if($email_type == 3)
					{
					?>
						Shipping label was created with ship via <?= $via; ?> and <?= $confirmation; ?> confirmation however <?= $customer->first_name ." ". $customer->last_name; ?> has requested for <?= $ship_via; ?> and <?= $sign; ?> confirmation for all of her Orders. Please check into this matter.
					<?php
					}
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2"><strong>Customer Information</strong></td>
			</tr>
			<tr>
				<td>Customer</td>
				<td><?= $customer->first_name ." ". $customer->last_name ; ?></td>
			</tr>
			<tr>
				<td>Phone Number</td>
				<td><?= Common::phone_number($order->phone_number); ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?= $order->email; ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><?= $order->address ? $order->address : ''; ?> <?= $order->address1 ? ", ".$order->address1 : ''; ?> <?= $order->city ? ", ". $order->city : ''; ?> <?= $order->state ? ", ".$order->state : ''; ?> <?= $order->zip_code ? ", ".$order->zip_code : ''; ?> <?= $order->country ? ", ". $order->country : ''; ?></td>
			</tr>

			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
				<td colspan="2"><strong>Tracking info</strong></td>
			</tr>
			<tr>
				<td>Tracking Number#</td>
				<td><?= $model->tracking_number ; ?></td>
			</tr>
			<tr>
				<td>Ship Date</td>
				<td><?= $model->ship_date ? date('m-d-Y', strtotime($model->ship_date)) : ''; ?></td>
			</tr>
			<tr>
				<td>Ship Via</td>
				<td><?= $via; ?></td>
			</tr>
			<tr>
				<td>Delivery Date</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>

			<tr>
				<td colspan="2"><strong>Order Information</strong></td>
			</tr>
			<tr>
				<td>Order Number</td>
				<td><?= $order->order_number ; ?></td>
			</tr>
			<tr>
				<td>Order Date</td>
				<td><?= $order->order_date ? date('m-d-Y', strtotime($order->order_date)) : ''; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?= $order->email; ?></td>
			</tr>
			<tr>
				<td>Address</td>
				<td><?= $order->address ? $order->address : ''; ?> <?= $order->address1 ? ", ".$order->address1 : ''; ?> <?= $order->city ? ", ". $order->city : ''; ?> <?= $order->state ? ", ".$order->state : ''; ?> <?= $order->zip_code ? ", ".$order->zip_code : ''; ?> <?= $order->country ? ", ". $order->country : ''; ?></td>
			</tr>
		</tbody>
	</table>
