<?php

namespace console\controllers;

use Yii;
use yii\helpers\Url;
use yii\console\Controller;
use yii\db\Expression;

// Models included
use common\models\User;
use common\models\Order;
use common\models\Message;
use common\models\Shopify;
use common\models\ShipStation;

/**
 * Test controller
 */
class AutomateController extends Controller {

    public function actionIndex() {
        echo "cron service runnning";
    }

    public function actionSendMail() {
        $getMessage = Message::find()
                                ->where(['sent_flag' => 0])
                                ->all();

        foreach ($getMessage as $mKey => $message)
        {
            if($message->message_type)
            {
                $param['to_email'] = $message->sent_to;
                if($message->sent_cc)
                {
                    $param['cc_email'] = explode(", ", $message->sent_cc);
                }
                else{
                    $param['cc_email'] = '';
                }
                
                if($message->sent_bcc)
                {
                    $param['bcc_email'] = [$message->sent_bcc];
                }
                else{
                    $param['bcc_email'] = '';
                }
                
                $param['subject']  = $message->subject;
                $param['from_email'] = $message->sent_from;

                $email = Yii::$app->mailer->compose('email', ['message' => $message->message_content]);

                if($message->message_attachment)
                {
                    $email->attach(Yii::getAlias("@app")."/../../".$message->message_attachment);
                }


                $sentMeail =    $email
                                ->setTo($param['to_email'])
                                ->setCc($param['cc_email'])
                                ->setFrom([$param['from_email'] => $param['from_email']])
                                ->setSubject($param['subject'])
                                ->send();
                if($sentMeail)
                {
                    $message->sent_flag     = 1;
                    $message->sent_attempts = $message->sent_attempts + 1;
                }
                else
                {
                    $message->sent_attempts = $message->sent_attempts + 1;
                }
                $message->save();
            }
        }
    }

    public function actionShipstation()
    {
        $getOrder = Order::find()
                            ->where(['ship_flag' => 0])
                            ->all();

        $db = Yii::$app->db;
        // $transaction = $db->beginTransaction();
        try{
            foreach ($getOrder as $oKey => $order) {
                if($order->order_key)
                {
                    $shopify    =   Shopify::getOrder($order->order_key);
                    if($shopify)
                    {
                        $response   =   json_decode($shopify);
                        $order_s    =   $response->order;

                        
                        $ship_model = new ShipStation();
                        $shipment = $ship_model->saveShipstation($order->order_number, $order->id);

                        $getShip    =   ShipStation::find()
                                                    ->where(['order_id' => $order->id])
                                                    ->all();

                        if($getShip)
                        {
                            foreach ($getShip as $key => $ship)
                            {
                                $customer_ship_via = $order->customer->ship_via;
                                $message = new Message();
                                $email_type = 0;
                                $send_email = 0;

                                if($ship->carrier_code == 'fedex')
                                {
                                    $ship_via_d = Order::FEDEX;
                                }
                                else{
                                    $ship_via_d = Order::USPS;
                                }

                                // if($customer_ship_via != Order::FEDEX && $customer_ship_via != Order::ANY)
                                if($customer_ship_via != $ship_via_d && $customer_ship_via != Order::ANY)
                                {
                                    if($order->customer->signature == 13 && (($ship->confirmation == 'delivery' || $ship->confirmation == '')))
                                    {
                                        $email_type = 3;
                                        $message['subject']         =   "Alert: Order#". $order->order_number ." issue with Ship Via & Signature";
                                    }
                                    else if($order->customer->signature == 14 && (($ship->confirmation != 'delivery' || $ship->confirmation != '')))
                                    {
                                        $email_type = 3;
                                        $message['subject']         =   "Alert: Order#". $order->order_number ." issue with Ship Via & Signature";
                                    }
                                    else
                                    {
                                        $message['subject']         =   "Alert: Order#". $order->order_number ." ship via doesn't match";
                                        $email_type = 1;
                                    }
                                    $send_email = 1;
                                }
                                else
                                {
                                    if($order->customer->signature == 13 && (($ship->confirmation == 'delivery' || $ship->confirmation == '')))
                                    {
                                        $email_type = 2;
                                        $message['subject']         =   "Alert: Order#". $order->order_number ." Signature via doesn't match";
                                    }
                                    else if($order->customer->signature == 14 && (($ship->confirmation != 'delivery' || $ship->confirmation != '')))
                                    {
                                        $email_type = 2;
                                        $message['subject']         =   "Alert: Order#". $order->order_number ." Signature via doesn't match";
                                    }
                                    $send_email = 0;
                                }

                                if($send_email == 1)
                                {
                                    // , 'check_sign' => $check_sign
                                    

                                    $msgTemplate = $this->renderPartial('fedex', ['model' => $ship, 'email_type' => $email_type]);

                                    $message['sent_from']       =   'papersync@babypavilion.com';
                                    $message['sent_to']         =   'info@babypavilion.com';
                                    $message['sent_cc']         =   'imomin@babypavilion.com, tmithani@babypavilion.com, tracking@babypavilion.com, sjaiswal@babypavilion.com';
                                    $message['sent_bcc']        =   '';
                                    // $message['subject']         =   "Alert: Order#". $order->order_number ." ship via doesn't match";
                                    $message['message_type']    =   'email';
                                    $message['message_content'] =   $msgTemplate;
                                    $message['sent_flag']       =   0;
                                    $message['sent_attempts']   =   0;
                                    $message['sent_on']         =   new Expression ('now()');
                                    $message['created_on']      =   new Expression ('now()');
                                    $message['created_by']      =   1;

                                    $message->saveMessage($message);
                                }
                            }

                            if($order_s->fulfillment_status == 'fulfilled')
                            {
                                $order->ship_flag   =   1;
                                $order->save();
                            }
                        }
                    }
                }
            }
        }
        catch(\Exception $e)
        {
            Yii::error($e);
        }
    }
}
